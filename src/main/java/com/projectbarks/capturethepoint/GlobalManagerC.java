package com.projectbarks.capturethepoint;

import java.util.logging.Level;

import lombok.Getter;

import com.projectbarks.capturethepoint.commands.MapCommands;
import com.projectbarks.capturethepoint.commands.VoteManager;
import com.projectbarks.capturethepoint.commands.VoteManagerC;
import com.projectbarks.capturethepoint.config.Config;
import com.projectbarks.capturethepoint.config.extensions.JSQLBackend;
import com.projectbarks.capturethepoint.config.extensions.WorldParser;
import com.projectbarks.capturethepoint.game.GameManager;
import com.projectbarks.capturethepoint.game.Timer;
import com.projectbarks.capturethepoint.game.TimerC;
import com.projectbarks.capturethepoint.game.bar.BarAPI;
import com.projectbarks.capturethepoint.kits.KitGUI;
import com.projectbarks.capturethepoint.kits.KitManager;
import com.projectbarks.capturethepoint.kits.KitManagerC;
import com.projectbarks.capturethepoint.kits.backend.KitAPI;
import com.projectbarks.capturethepoint.kits.backend.KitAPIImpl;
import com.projectbarks.capturethepoint.messages.MessageManager;
import com.projectbarks.capturethepoint.stats.StatsManager;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class GlobalManagerC implements GlobalManager {

    private static GlobalManagerC globalManager;

    public static void setGameManager(GlobalManagerC gm) {
        if (GlobalManagerC.globalManager != null) {
            throw new UnsupportedOperationException("Cannot redefine singleton Instance");
        }
        GlobalManagerC.globalManager = gm;
    }

    @Getter
    private final CaptureThePoint captureThePoint;
    private Timer timer;
    private StatsManager statsManager;
    private MessageManager msgManager;
    private VoteManagerC voteManager;
    private GameManager gameManager;
    private KitManager kitManager;
    private Config config;
    private MapCommands mapManager;
    private KitGUI listenerInvGUI;
    private WorldParser worldParser;
    private JSQLBackend jsqlBackend;
    private KitAPI kitAPI;
    private BarAPI barAPI;

    public static GlobalManagerC getGlobalManager() {
        return globalManager;
    }

    /**
     * 
     * @param captureThePoint
     */
    public GlobalManagerC(CaptureThePoint captureThePoint) {
        this.captureThePoint = captureThePoint;
    }

    // -----Public Methods-----//
    @Override
    public void enableClass(Class<?> clazz) {
        if (clazz.isAssignableFrom(WorldParser.class)) {
            this.worldParser = new WorldParser(this);
        } else if (clazz.isAssignableFrom(Config.class)) {
            this.config = new Config(this);
        } else if (clazz.isAssignableFrom(MessageManager.class)) {
            this.msgManager = new MessageManager(this);
        } else if (clazz.isAssignableFrom(KitManagerC.class)) {
            this.kitManager = new KitManagerC(this);
        } else if (clazz.isAssignableFrom(VoteManagerC.class)) {
            this.voteManager = new VoteManagerC(this);
        } else if (clazz.isAssignableFrom(GameManager.class)) {
            this.gameManager = new GameManager(this);
        } else if (clazz.isAssignableFrom(TimerC.class)) {
            this.timer = new TimerC(this);
        } else if (clazz.isAssignableFrom(MapCommands.class)) {
            this.mapManager = new MapCommands(this);
        } else if (clazz.isAssignableFrom(KitGUI.class)) {
            this.listenerInvGUI = new KitGUI(this);
        } else if (clazz.isAssignableFrom(StatsManager.class)) {
            this.statsManager = new StatsManager(this);
        } else if (clazz.isAssignableFrom(JSQLBackend.class)) {
            this.jsqlBackend = new JSQLBackend(this);
        } else if (clazz.isAssignableFrom(KitAPIImpl.class)) {
            this.kitAPI = new KitAPIImpl(this);
        } else if (clazz.isAssignableFrom(BarAPI.class)) {
            this.barAPI = new BarAPI(this);
        } else {
            this.captureThePoint.getLogger().log(Level.SEVERE, "{0}" + " is not a valid class to be enabled in the global class manager", clazz.getName());
        }
    }

    @Override
    public MessageManager getMsgManager() {
        return msgManager;
    }

    @Override
    public GameManager getGameManager() {
        return gameManager;
    }

    @Override
    public KitManager getKitManager() {
        return kitManager;
    }

    @Override
    public VoteManager getVoteManager() {
        return voteManager;
    }

    @Override
    public CaptureThePoint getCaptureThePoint() {
        return captureThePoint;
    }

    @Override
    public MapCommands getMapManager() {
        return mapManager;
    }

    @Override
    public Config getConfig() {
        return config;
    }

    @Override
    public WorldParser getWorldParser() {
        return worldParser;
    }

    @Override
    public Timer getTimer() {
        return timer;
    }

    @Override
    public KitGUI getListenerInvGUI() {
        return listenerInvGUI;
    }

    @Override
    public StatsManager getStatsManager() {
        return statsManager;
    }

    @Override
    public JSQLBackend getJsqlBackend() {
        return jsqlBackend;
    }

    @Override
    public KitAPI getKitAPI() {
        return kitAPI;
    }

    @Override
    public BarAPI getBarAPI() {
        return barAPI;
    }
}
