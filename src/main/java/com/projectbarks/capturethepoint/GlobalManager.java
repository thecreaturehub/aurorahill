package com.projectbarks.capturethepoint;

import com.projectbarks.capturethepoint.commands.MapCommands;
import com.projectbarks.capturethepoint.commands.VoteManager;
import com.projectbarks.capturethepoint.config.Config;
import com.projectbarks.capturethepoint.config.extensions.JSQLBackend;
import com.projectbarks.capturethepoint.config.extensions.WorldParser;
import com.projectbarks.capturethepoint.game.GameManager;
import com.projectbarks.capturethepoint.game.Timer;
import com.projectbarks.capturethepoint.game.bar.BarAPI;
import com.projectbarks.capturethepoint.kits.KitGUI;
import com.projectbarks.capturethepoint.kits.KitManager;
import com.projectbarks.capturethepoint.kits.backend.KitAPI;
import com.projectbarks.capturethepoint.messages.MessageManager;
import com.projectbarks.capturethepoint.stats.StatsManager;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public interface GlobalManager {

    /**
     * Gets {@link MessageManager} class. The Message manager handles custom
     * messages by loading the {@link com.projectbarks.Capture-The-Poin.messages.Msg}
     * config and then reading from the messages.yml
     *
     * @return
     */
    public MessageManager getMsgManager();

    /**
     * Gets the {@link GameManager} interface. Can be very dangerous and is used
     * for strict game moderation.
     *
     * @return
     */
    public GameManager getGameManager();

    /**
     * Gets the {@link KitManager} interface. A kit manager that handel's kit
     * selection and application. Along with tokens management.
     *
     * @return
     */
    public KitManager getKitManager();

    /**
     * Gets the {@link VoteManager} interface. Handel's map voting.
     *
     * @return
     */
    public VoteManager getVoteManager();

    /**
     * Gets the {@link  CaptureThePoint} class. Avoid usage.
     *
     * @return
     */
    public CaptureThePoint getCaptureThePoint();

    /**
     * Returns the {@link MapCommands} class.
     *
     * @return
     */
    public MapCommands getMapManager();

    /**
     * Returns the {@link Config} class.
     *
     * @return
     */
    public Config getConfig();

    /**
     * Returns the {@link WorldParser} class.
     *
     * @return
     */
    public WorldParser getWorldParser();

    /**
     * Returns the {@link Timer} interface.
     *
     * @return
     */
    public Timer getTimer();

    /**
     * GUI used for players to select kits. Avoid usage!
     *
     * @return
     */
    public KitGUI getListenerInvGUI();

    /**
     * Gets the class used for managing player statistics. Class also manages
     * saving and loading of, "statistics.yml".
     *
     * @return
     */
    public StatsManager getStatsManager();

    /**
     * Launches a class based off the constructor. A "SEVERE" print will be sent
     * if the class is not found.
     *
     * @param clazz
     */
    public void enableClass(Class<?> clazz);

    /**
     * Gets the JSQLBackend for storing players.
     *
     * @return
     */
    public JSQLBackend getJsqlBackend();

    public KitAPI getKitAPI();
    
    public BarAPI getBarAPI();
}
