package com.projectbarks.capturethepoint.api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.NonNull;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @param <T> object carbon votable wraps.
 * @param <VT> the Object the voter will be made of or will contain an instance
 * of.
 */
public class CarbonVotable<T, VT> implements Comparable<CarbonVotable<T, VT>> {

    private T voteAble;
    private Map<VT, Integer> voters;

    public CarbonVotable(T voteAble) {
        this.voteAble = voteAble;
        this.voters = new HashMap<>();
    }

    public boolean hasVoted(VT voter) {
        return voters.containsKey(voter);
    }

    public void addToVotes(VT voter) {
        int votes = 1;
        if (this.voters.containsKey(voter)) {
            votes += this.voters.get(voter);
        }
        this.voters.put(voter, votes);
    }

    public List<VT> getHighestVoter() {
        List<VT> highestVoters = new ArrayList<>();
        for (Map.Entry<VT, Integer> voter : this.voters.entrySet()) {
            if (highestVoters.size() <= 0) {
                highestVoters.add(voter.getKey());
            } else {
                for (VT currentHighestVoter : highestVoters) {
                    Integer highestVoterPoints = this.voters.get(currentHighestVoter);
                    if (voter.getValue() > highestVoterPoints) {
                        highestVoters.clear();
                        highestVoters.add(voter.getKey());
                    } else if (voter.getValue().equals(highestVoterPoints)) {
                        highestVoters.add(voter.getKey());
                    }
                }
            }
        }

        return Collections.unmodifiableList(highestVoters);
    }

    public Integer getTotalVotes() {
        Integer total = 0;
        for (Integer voteAmount : this.voters.values()) {
            total += voteAmount;
        }
        return total;
    }

    public Map<VT, Integer> getVotesSyncronized() {
        return Collections.synchronizedMap(this.voters);
    }

    @Override
    @NonNull
    public int compareTo(CarbonVotable<T, VT> o) {
        int votes = o.getTotalVotes();
        if (this.getTotalVotes() > votes) {
            return 1;
        } else if (this.getTotalVotes() < votes) {
            return -1;
        } else {
            return 0;
        }
    }
    
    public T getVoteAble() {
        return voteAble;
    }
    
    public Map<VT, Integer> getVoters() {
        return voters;
    }
}
