/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.projectbarks.capturethepoint.api;

import com.projectbarks.capturethepoint.game.scoreboards.GameScoreBoard;
import com.projectbarks.capturethepoint.maps.Beacon;
import com.projectbarks.capturethepoint.maps.GMap;
import java.util.List;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

/**
 *
 * @author Brandon Barker
 */
public interface Game {

    /**
     * Returns the {@link GMap} of the Game.
     *
     * @return
     */
    public GMap getMap();

    /**
     * Get the {@link Participant}'s in the game. It is not reccomended you add
     * to this list.
     *
     * @return a un-muted copy of participants in the game.
     */
    public List<Participant> getParticipants();

    /**
     * Get the current state of the game. This function is not auto moderated
     * and requires physical changes; however, these changes will not modify the
     * game player rather act as an identifier.
     *
     * @return a Enumeration from {@link GameMode}
     */
    public GameMode getMode();

    /**
     * Get the beacon currently set in the game. This is not a auto moderated
     * function and requires setting. Null should not be returned (unless you
     * set it null)
     *
     * @return the current beacon in game.
     */
    public Beacon getCurrentBeacon();

    /**
     * Set the mode/state of the game. This is not a auto moderated function,
     * thus automatically within the object.
     *
     * @param mode cannot be null.
     */
    public void setMode(GameMode mode);

    /**
     * Match the participant with a player.
     *
     * @param player
     * @return null if is not found.
     */
    public Participant getPlayer(OfflinePlayer player);

    /**
     * Complex method that will fire event {@link WinnerChangeEvent}. Returns a
     * new participant if the old and new highest point users are not the same
     * and If there is not a tie.
     *
     * @return
     */
    public Participant getWinner();

    public List<Participant> getWinners();

    public void pointCompassToBeacon();

    public int getBeaconPoints(OfflinePlayer p);

    public int addBeaconPoints(OfflinePlayer p, int val);
    
    public void changeBeacon();

    public GameScoreBoard getScoreboard();
}
