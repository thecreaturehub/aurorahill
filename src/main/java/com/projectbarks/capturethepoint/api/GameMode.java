package com.projectbarks.capturethepoint.api;

import lombok.Getter;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public enum GameMode {

    /**
     * The first mode equal to One
     */
    CREATED(0),
    /**
     * The second mode equal to Two
     */
    STARTED(1),
    /**
     * The third and final mode equal to Three
     */
    ENDED(2);
    private @Getter
    final int level;

    GameMode(Integer mode) {
        this.level = mode;
    }

    /**
     *
     * @param id
     * @return the Matched mode of an Integer id It will return null if it is
     * not between 0 and 2
     */
    public static GameMode matchMode(Integer id) {
        switch (id) {
            case 0:
                return CREATED;
            case 1:
                return STARTED;
            case 2:
                return ENDED;
            default:
                return null;
        }
    }

    /**
     *
     * @return The Event mode that is higher in the ladder. This will return
     * null if there is nothing above.
     */
    public GameMode nextMode() {
        return matchMode(this.level + 1);
    }

    /**
     *
     * @return The Event mode that is lower in the ladder. This will return null
     * if there is nothing below.
     */
    public GameMode previousMode() {
        return matchMode(this.level - 1);
    }

    /**
     * Gets the name of the enumeration using a simple toString function.
     *
     * @return
     */
    public String getName() {
        return this.toString();
    }
}
