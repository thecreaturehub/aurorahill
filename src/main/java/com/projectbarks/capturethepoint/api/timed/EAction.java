package com.projectbarks.capturethepoint.api.timed;

import java.util.Locale;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public enum EAction {

   @Deprecated
    BEACON_CHANGE,
    LOBBY_MSG,
    LOBBY_MSG_TIME,
    BROADCAST_GAME_TIME,
    START_GAME,
    END_GAME,
    RESTART_GAME,
    KICK_PLAYERS;

    public String getAction() {
        return toString().toUpperCase(Locale.getDefault());
    }
}