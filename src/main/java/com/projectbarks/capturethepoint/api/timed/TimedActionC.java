package com.projectbarks.capturethepoint.api.timed;

import lombok.Getter;
import lombok.Setter;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class TimedActionC implements TimedAction {

    @Getter
    private final Integer time;
    @Getter
    @Setter
    private boolean fired;
    @Getter
    @Setter
    private EAction type;

    public TimedActionC(Integer time, EAction ea) {
        this.time = time;
        this.fired = false;
        this.type = ea;
    }
    
    public Integer getTime() {
        return time;
    }
    
    public boolean isFired() {
        return fired;
    }
    
    public void setFired(boolean fired) {
        this.fired = fired;
    }
    
    public EAction getType() {
        return type;
    }
    
    public void setType(EAction type) {
        this.type = type;
    }
}
