package com.projectbarks.capturethepoint.api.timed;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public interface TimedAction {

    /**
     * Get the target time in seconds for fire when to fire.
     *
     * @return
     */
    public Integer getTime();

    /**
     * Get if the function has been fired.
     *
     * @return
     */
    public boolean isFired();

    /**
     * Set if the function has been fired.
     *
     * @param fired
     */
    public void setFired(boolean fired);

    /**
     * Get the type of {@link EAction} that the TimedAction is.
     *
     * @return
     */
    public EAction getType();

    /**
     * Set the type of {@link EAction} that the TimedAction is.
     *
     * @param type
     */
    public void setType(EAction type);
}
