package com.projectbarks.capturethepoint.api;

import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.GlobalManagerC;
import com.projectbarks.capturethepoint.kits.backend.Kit;
import com.projectbarks.capturethepoint.messages.Msg;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public abstract class Participant implements Comparable<Participant> {

    /**
     * Gets the amount of tokens the participant has. Default is 0
     *
     * @return
     */
    public abstract Integer getTokens();

    /**
     * Gets the Name of the kit used. This String can be referenced to
     * kitManager to get the kit Object.
     *
     * @return
     */
    public abstract String getKitName();

    /**
     * A boolean if the participant is currently online.
     *
     * @return
     */
    public abstract boolean isOnline();

    /**
     * Returns a interface below the Player. This can be used when a participant
     * disconnects.
     *
     * @return
     */
    public abstract OfflinePlayer getOfflinePlayer();

    /**
     * Returns the player object that inherits functions from OfflinePlayer.
     *
     * @return
     */
    public abstract Player getOnlinePlayer();

    /**
     * Set the amount of tokens a player has.
     *
     * @param tokens
     */
    public abstract void setTokens(Integer tokens);
    
    public abstract Integer getBeaconPoints();

    public static Participant buildParticipant(Player p) {
        GlobalManager gm = GlobalManagerC.getGlobalManager();
        Kit kit = gm.getKitManager().getPlayerKit(p);
        gm.getKitManager().buyKit(p, kit);
        gm.getMsgManager().msg(p, Msg.KIT_BUY, kit.getName(), kit.getCost());
        return new ParticipantC(p, 0, kit.getName());
    }
}
