package com.projectbarks.capturethepoint.api.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.projectbarks.capturethepoint.api.Game;
import com.projectbarks.capturethepoint.api.Participant;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class WinnerChangeEvent extends Event {

    private static final HandlerList handlerList = new HandlerList();
    private final Game game;
    private final Participant oldWinner;
    private final Participant newWinner;

    public WinnerChangeEvent(Game game, Participant OldWinner, Participant NewWinner) {
        this.game = game;
        this.newWinner = NewWinner;
        this.oldWinner = OldWinner;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    public Game getGame() {
        return game;
    }

    public Participant getOldWinner() {
        return oldWinner;
    }

    public Participant getNewWinner() {
        return newWinner;
    }
}