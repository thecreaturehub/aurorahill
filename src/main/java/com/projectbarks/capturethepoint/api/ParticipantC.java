package com.projectbarks.capturethepoint.api;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class ParticipantC extends Participant {

    private final String player;
    private final String kitName;
    private Integer tokens;
    protected Integer beaconPoints;

    public ParticipantC(Player player) {
        this(player, 0);
    }

    public ParticipantC(Player player, Integer token) {
        this(player, token, null);
    }

    public ParticipantC(Player player, Integer token, String kit) {
        this.player = player.getName();
        this.tokens = token;
        this.kitName = kit;
        beaconPoints = 0;
    }

    @Override
    public boolean isOnline() {
        return this.getOfflinePlayer().isOnline();
    }

    @Override
    public OfflinePlayer getOfflinePlayer() {
        return Bukkit.getOfflinePlayer(this.player);
    }

    @Override
    public Player getOnlinePlayer() {
        return Bukkit.getPlayer(this.player);
    }

    @Override
    public int compareTo(Participant o) {
        return this.beaconPoints.compareTo(((ParticipantC) o).beaconPoints);
    }

    @Override
    public Integer getTokens() {
        return tokens;
    }

    @Override
    public String getKitName() {
        return kitName;
    }

    @Override
    public void setTokens(Integer tokens) {
        this.tokens = tokens;
    }

    @Override
    public Integer getBeaconPoints() {
        return beaconPoints;
    }
}
