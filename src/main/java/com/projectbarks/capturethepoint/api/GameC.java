package com.projectbarks.capturethepoint.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.google.common.collect.Lists;
import com.projectbarks.capturethepoint.api.events.WinnerChangeEvent;
import com.projectbarks.capturethepoint.game.scoreboards.GameScoreBoard;
import com.projectbarks.capturethepoint.maps.Beacon;
import com.projectbarks.capturethepoint.maps.GMap;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class GameC implements Game {

    private final GMap map;
    private GameMode mode;
    private List<Participant> participants;
    private Beacon beacon;
    private ParticipantC winner;
    private GameScoreBoard scoreboard;

    public GameC(GMap map) {
        this(map, GameMode.CREATED);
    }

    public GameC(GMap map, GameMode mode) {
        this(map, mode, new ArrayList<Participant>());
    }

    public GameC(GMap map, GameMode mode, Collection<Participant> participants) {
        scoreboard = new GameScoreBoard();
        this.map = map;
        this.mode = mode;
        this.winner = null;
        this.participants = new ArrayList<>();
        this.participants.addAll(participants);

        Random random = new Random();
        this.beacon = map.getBeacons().get(random.nextInt(map.getBeacons().size()));
    }

    @Override
    public List<Participant> getParticipants() {
        return Collections.unmodifiableList(this.participants);
    }

    @Override
    public Beacon getCurrentBeacon() {
        return this.beacon.clone();
    }

    private void setCurrentBeacon(Beacon beacon) {
        this.beacon = beacon.clone();
    }

    @Override
    public Participant getPlayer(OfflinePlayer player) {
        for (Participant p : this.participants) {
            if (p.getOfflinePlayer().getName().equalsIgnoreCase(player.getName())) {
                return p;
            }
        }
        return null;
    }

    private void recalculateWinners() {
        Collections.sort(participants);
        Collections.reverse(participants);
        ParticipantC equaled = (ParticipantC) (participants.size() > 0 ? participants.get(0) : null);
        if (equaled != null && equaled != winner && equaled.beaconPoints > 0) {
            if (winner != null && winner.beaconPoints == equaled.beaconPoints) {
                return;
            }
            Bukkit.getPluginManager().callEvent(new WinnerChangeEvent(this, winner, equaled));
            winner = equaled;
            scoreboard.setWinner(winner.getOfflinePlayer());
        }
    }

    @Override
    public Participant getWinner() {
        return winner;
    }

    @Override
    public List<Participant> getWinners() {
        List<Participant> winners = new ArrayList<>();
        if (winner == null) {
            return winners;
        }

        if (participants.size() > 0) {
            winners.add(winner);
            for (Participant test : participants) {
                if (getBeaconPoints(test.getOfflinePlayer()) != getBeaconPoints(winner.getOfflinePlayer())) {
                    break;
                }
                if (winners.contains(test)) {
                    continue;
                }
                winners.add(test);

            }
        }
        return winners;
    }

    @Override
    public void pointCompassToBeacon() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.isOnline()) {
                player.setCompassTarget(getCurrentBeacon().getLocation());
            }
        }
    }

    @Override
    public int getBeaconPoints(OfflinePlayer p) {
        return (int) ((ParticipantC) this.getPlayer(p)).beaconPoints;
    }

    @Override
    public int addBeaconPoints(OfflinePlayer p, int val) {
        return setBeaconPoints(p, val + this.getBeaconPoints(p));
    }

    private int setBeaconPoints(OfflinePlayer p, int val) {
        this.scoreboard.setScore(this, p, val);
        ParticipantC player = (ParticipantC) this.getPlayer(p);
        player.beaconPoints = val;
        recalculateWinners();
        return player.beaconPoints;
    }

    @Override
    public void changeBeacon() {
        List<Beacon> list = Lists.newArrayList(map.getBeacons());
        Beacon newBeacon;
        do {
            Collections.shuffle(list);
            newBeacon = list.get(0);
        } while (beacon.getLocation() == newBeacon.getLocation());

        beacon.restore();
        newBeacon.applyBeacon(Material.DIAMOND_BLOCK, Material.BEACON);
        setCurrentBeacon(newBeacon);
        this.pointCompassToBeacon();
    }

    @Override
    public GMap getMap() {
        return map;
    }

    @Override
    public GameMode getMode() {
        return mode;
    }

    @Override
    public void setMode(GameMode mode) {
        this.mode = mode;
    }

    @Override
    public GameScoreBoard getScoreboard() {
        return scoreboard;
    }
}
