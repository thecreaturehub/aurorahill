package com.projectbarks.capturethepoint.game.scoreboards;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.projectbarks.capturethepoint.GlobalManagerC;
import com.projectbarks.capturethepoint.game.TimerStatus;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class ScoreboardManager {

    private static CTPScoreboard lobbyScoreboard, blank;
    private static CTPScoreboard currentScoreboard;
    private static TimerStatus status;

    public static void reset() {
        status = null;
        lobbyScoreboard = new LobbyScoreboard();
        blank = new BlankScoreboard();
        currentScoreboard = blank;
    }

    public static void update(TimerStatus status) {
        currentScoreboard.update();

        if (ScoreboardManager.status != status) {
            ScoreboardManager.status = status;
            currentScoreboard = getScoreboard(status);
            for (Player player : Bukkit.getOnlinePlayers()) {
                player.setScoreboard(currentScoreboard.getScoreboard());
            }
        }
    }

    public static CTPScoreboard getScoreboard(TimerStatus status) {
        switch (status) {
        case GAME:
            return GlobalManagerC.getGlobalManager().getTimer().getGame().getScoreboard();
        case LOBBY:
            return lobbyScoreboard;
        default:
            return blank;
        }
    }

    private ScoreboardManager() {
    }

    public static CTPScoreboard getCurrentScoreboard() {
        return currentScoreboard;
    }
}
