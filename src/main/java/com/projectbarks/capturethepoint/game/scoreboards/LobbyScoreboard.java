package com.projectbarks.capturethepoint.game.scoreboards;

import com.projectbarks.capturethepoint.api.CarbonVotable;
import com.projectbarks.capturethepoint.maps.GMap;
import com.projectbarks.capturethepoint.messages.MessageManager;
import com.projectbarks.capturethepoint.messages.Msg;
import org.bukkit.Bukkit;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Team;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class LobbyScoreboard extends CTPScoreboard {

    private Objective onlinePlayers;
    private Team online;

    public LobbyScoreboard() {
        onlinePlayers = scoreboard.registerNewObjective("onlinep", "dummy");
        onlinePlayers.setDisplayName(getGlobalManager().getMsgManager().getMsgNoTitle(Msg.SCOREBOARD_LOBBY));
        onlinePlayers.setDisplaySlot(DisplaySlot.SIDEBAR);
        init();
    }

    @Override
    public void update() {
        online.setSuffix(Bukkit.getOnlinePlayers().length + "");
    }

    private void init() {
        MessageManager mm = getMessageManager();
        this.openData(onlinePlayers);
        this.addData(mm.getMsgNoTitle(Msg.SCOREBOARD_LOBBY_TITLE1), onlinePlayers);
        online = this.addData(mm.getMsgNoTitle(Msg.SCOREBOARD_LOBBY_ONLINE), onlinePlayers);
        this.addData(mm.getMsgNoTitle(Msg.SCOREBOARD_LOBBY_MIN, getGlobalManager().getConfig().getMinPlayers()), onlinePlayers);
        this.addData(mm.getMsgNoTitle(Msg.SCOREBOARD_LOBBY_MAX, Bukkit.getServer().getMaxPlayers()), onlinePlayers);
        this.addBlank(onlinePlayers);
        this.addData(mm.getMsgNoTitle(Msg.SCOREBOARD_LOBBY_TITLE2), onlinePlayers);
        for (CarbonVotable<GMap, String> map : getGlobalManager().getVoteManager().getVotes()) {
            this.addData(mm.getMsgNoTitle(Msg.SCOREBOARD_LOBBY_MAPS, map.getVoteAble().getName()), onlinePlayers);
        }
        this.closeData(onlinePlayers);
    }
}
