package com.projectbarks.capturethepoint.game.scoreboards;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.GlobalManagerC;
import com.projectbarks.capturethepoint.messages.MessageManager;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public abstract class CTPScoreboard {

    private int index = 15;
    private List<Objective> enabled = new ArrayList<>();

    public abstract void update();
    protected Scoreboard scoreboard;

    public CTPScoreboard() {
        scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
    }

    public void openData(Objective o) {
        enabled.add(o);
    }

    public void closeData(Objective o) {
        if (!enabled.contains(o)) {
            return;
        }
        enabled.remove(o);
        index = 15;
    }

    private boolean isOpen(Objective o) {
        return enabled.contains(o);
    }

    ;

    public Team addData(String data, Objective o) {
        if (!isOpen(o)) {
            return null;
        }
        if (!o.getDisplaySlot().equals(DisplaySlot.SIDEBAR)) {
            return null;
        }
        String revised = data.length() > 16 ? data.substring(0, 16) : data;
        revised = ChatColor.translateAlternateColorCodes('&', revised);
        OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(revised);
        Score score = o.getScore(offlinePlayer);
        score.setScore(index);

        index--;
        index = index <= 0 ? 16 : index;
        Team registerNewTeam = scoreboard.registerNewTeam(revised);
        registerNewTeam.addPlayer(offlinePlayer);
        return registerNewTeam;
    }

    public void addBlank(Objective o) {
        StringBuilder space = new StringBuilder(16);
        for (int i = 0; i <= index; i++) {
            space.append(" ");
        }
        this.addData(space.toString(), o);
    }

    public Objective reset(Objective o) {
        String dn = o.getDisplayName();
        DisplaySlot slot = o.getDisplaySlot();
        Objective newObjective = scoreboard.registerNewObjective(o.getName(), o.getCriteria());
        newObjective.setDisplaySlot(slot);
        newObjective.setDisplayName(dn);
        return newObjective;
    }

    public GlobalManager getGlobalManager() {
        return GlobalManagerC.getGlobalManager();
    }

    public MessageManager getMessageManager() {
        return this.getGlobalManager().getMsgManager();
    }

    public Scoreboard getScoreboard() {
        return scoreboard;
    }
}
