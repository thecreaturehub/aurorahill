package com.projectbarks.capturethepoint.game.scoreboards;

import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.api.Game;
import com.projectbarks.capturethepoint.api.Participant;
import com.projectbarks.capturethepoint.messages.Msg;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Team;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class GameScoreBoard extends CTPScoreboard {

    private Objective underName, sidebar, tab;
    private int sidebarCount = 0;
    private Team winner;

    public GameScoreBoard() {
        winner = scoreboard.registerNewTeam("Winner");
        GlobalManager gm = this.getGlobalManager();
        winner.setPrefix(gm.getMsgManager().getMsgNoTitle(Msg.SUFFIX_WINNER, gm.getConfig().getWinnerUnicode()));

        underName = scoreboard.registerNewObjective("namescore", "dummy");
        underName.setDisplaySlot(DisplaySlot.BELOW_NAME);
        underName.setDisplayName(ChatColor.GOLD + "Points");

        initSidebar();

        tab = scoreboard.registerNewObjective("tabscore", "dummy");
        tab.setDisplaySlot(DisplaySlot.PLAYER_LIST);
        tab.setDisplayName("Points");
    }

    @Override
    public void update() {
    }

    public void setScore(Game game, OfflinePlayer p, int score) {
        tab.getScore(p).setScore(score);
        underName.getScore(p).setScore(score);
        sidebar.getScore(p).setScore(score);
        sidebarCount++;
        if (sidebarCount >= 15) {
            sidebar.unregister();
            initSidebar();
            List<Participant> participants = new ArrayList<>(game.getParticipants());
            Collections.sort(participants);
            int toIndex = participants.size() - 15;
            toIndex = Math.max(toIndex, 0);
            for (Participant part : participants.subList(toIndex, participants.size())) {
                sidebar.getScore(part.getOfflinePlayer()).setScore(part.getBeaconPoints());
            }
        }
    }

    public void setWinner(OfflinePlayer p) {
        for (OfflinePlayer r : winner.getPlayers()) {
            winner.removePlayer(r);
        }
        winner.addPlayer(p);
    }

    private void initSidebar() {
        String scoreboardName = "sbs" + UUID.randomUUID().getMostSignificantBits();
        scoreboardName = scoreboardName.substring(0, 16);
        sidebar = scoreboard.registerNewObjective(scoreboardName, "dummy");
        sidebar.setDisplaySlot(DisplaySlot.SIDEBAR);
        sidebar.setDisplayName(ChatColor.GOLD + "Points");
    }
}
