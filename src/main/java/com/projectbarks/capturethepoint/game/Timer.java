package com.projectbarks.capturethepoint.game;

import com.projectbarks.capturethepoint.api.Game;
import org.bukkit.entity.Player;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public interface Timer {

    /**
     * Check if a game is currently taking progress.
     *
     * @return
     */
    public boolean inGame();

    /**
     * Check if the server lobby is currently in progress.
     *
     * @return
     */
    public boolean inLobby();

    /**
     * Checks if a certain player is in a game.
     *
     * @param player
     * @return
     */
    public boolean isInGame(Player player);

    /**
     * Gets the Integer(time) of the timer.
     *
     * @return
     */
    public int getTime();

    /**
     * Gets the Game currently in the timer.
     *
     * @return
     */
    public Game getGame();

    public boolean inGameOver();

    public TimerStatus getStatus();
}
