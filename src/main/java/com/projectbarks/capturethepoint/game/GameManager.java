package com.projectbarks.capturethepoint.game;

import com.projectbarks.capturethepoint.CaptureThePoint;
import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.api.CarbonVotable;
import com.projectbarks.capturethepoint.api.Game;
import com.projectbarks.capturethepoint.api.GameC;
import com.projectbarks.capturethepoint.api.GameMode;
import com.projectbarks.capturethepoint.api.Participant;
import com.projectbarks.capturethepoint.api.timed.EAction;
import com.projectbarks.capturethepoint.api.timed.TimedAction;
import com.projectbarks.capturethepoint.api.timed.TimedActionC;
import com.projectbarks.capturethepoint.commands.VoteManager;
import com.projectbarks.capturethepoint.config.Config;
import com.projectbarks.capturethepoint.maps.GMap;
import com.projectbarks.capturethepoint.messages.MessageManager;
import com.projectbarks.capturethepoint.messages.Msg;
import com.projectbarks.capturethepoint.misc.EntrySet;
import com.projectbarks.capturethepoint.misc.GameUtils;
import com.projectbarks.capturethepoint.stats.PlayerStats;
import com.projectbarks.capturethepoint.stats.StatType;
import com.projectbarks.capturethepoint.stats.StatsManager;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class GameManager {

    private static final Logger LOG = Logger.getLogger(GameManager.class.getName());
    private static GlobalManager gm;
    private static Config config;
    private static MessageManager mm;
    private static VoteManager wm;
    private static boolean init = false;

    public GameManager(GlobalManager gm) {
        if (!init) {
            GameManager.gm = gm;
            config = gm.getConfig();
            mm = gm.getMsgManager();
            wm = gm.getVoteManager();
            init = true;
        }
    }

    public static Game startGame() {
        Player[] onlinePlayers = Bukkit.getServer().getOnlinePlayers();
        if (onlinePlayers.length < config.getMinPlayers()) {
            mm.broadcast(Msg.GAME_FEW_PLAYERS);
            return null;
        }

        GMap map = wm.getHighestMap().getVoteAble();
        for (CarbonVotable<GMap, String> votable : wm.getVotes()) {
            if (votable.getVoteAble() != map) {
                votable.getVoteAble().unloadChunks();
            }
        }

        List<Participant> players = new ArrayList<>();
        map.loadGameChunks();
        for (Player player : onlinePlayers) {
            player.setHealth(20);
            player.setGameMode(org.bukkit.GameMode.SURVIVAL);
            players.add(Participant.buildParticipant(player));
            player.teleport(map.randomSpawnpoint());
        }
        gm.getKitManager().getPlayersKits().clear();
        Game game = new GameC(map, GameMode.STARTED, players);
        mm.broadcast(Msg.GAME_STARTED);
        changeBeacon(game);
        return game;
    }

    public static void endGame(Game game, boolean isDisabling) {
        game.setMode(GameMode.ENDED);
        List<Participant> winners = game.getWinners();
        StatsManager statsManager = gm.getStatsManager();
        statsManager.distributePoints(winners, game.getParticipants(), gm.getConfig().getPercentOfRPDisperse());
        Participant winner = winners.size() > 0 ? winners.get(0) : null;
        if (winners.size() > 1) {
            mm.broadcast(Msg.GAME_WINNERS, winnerBuilder(winners), game.getBeaconPoints(winner.getOfflinePlayer()));
        }
        if (winners.size() == 1) {
            mm.broadcast(Msg.GAME_WINNER, winner.getOfflinePlayer().getName(), game.getBeaconPoints(winner.getOfflinePlayer()));
        } else {
            mm.broadcast(Msg.GAME_NO_WINNER);
        }
        for (Participant p : game.getParticipants()) {
            PlayerStats stats = statsManager.getStats(p.getOfflinePlayer());
            if (p.isOnline()) {
                if (winners.contains(p) && stats != null) {
                    stats.addAmount(StatType.GamesWon, 1);
                }

                p.getOnlinePlayer().teleport(config.getLobby());
                GameUtils.resetPlayer(p.getOnlinePlayer(), GameUtils.PlayerInfo.values());
                p.getOnlinePlayer().setFireTicks(0);
                p.getOnlinePlayer().setCompassTarget(config.getLobby());
                if (winners.size() > 0) {
                    p.setTokens(p.getTokens() + (winners.contains(p) ? config.getWinner() : config.getLooser()));
                }
                mm.msg(p.getOnlinePlayer(), Msg.TOKENS_CHANGED, p.getTokens(), gm.getKitManager().getCurrencyName(p.getTokens()));

            }
            if (stats != null) {
                stats.addAmount(StatType.RageQuits, 1);
                stats.addAmount(StatType.Points, game.getBeaconPoints(p.getOfflinePlayer()));
                stats.addAmount(StatType.GamesPlayed, 1);
                stats.addAmount(StatType.Opponents, game.getParticipants().size());
                statsManager.updatePlayerRanks();
            }

            if (!isDisabling) {
                gm.getKitManager().getEconomy().depositPlayer(p.getOfflinePlayer().getName(), p.getTokens());
            }
        }
        game.getMap().cleanUp();
        game.getMap().rollback();
        wm.reloadVoteMaps();
        mm.broadcast(Msg.GAME_ENDED);
    }

    public static void changeBeacon(Game game) {
        game.changeBeacon();
        Location loc = game.getCurrentBeacon().getLocation();
        mm.broadcast(Msg.GAME_BEACON_CHANGE, loc.getBlockX() + ", " + loc.getBlockY() + ", " + loc.getBlockZ());
    }

    public static void gameMsgTime(TimedAction action, Map<Integer, Set<TimedActionC>> actions) {
        TimedAction gameEnd = GameUtils.findFirstAction(EAction.END_GAME, actions);
        if (gameEnd != null) {
            if (action.getType() == EAction.BROADCAST_GAME_TIME) {
                String[] times = GameUtils.getTime(gameEnd.getTime() - action.getTime());
                mm.broadcast(Msg.GAME_TIME, times[0], times[1]);
            }
        }
    }

    public static void lobbyMsgTime(TimedAction action, Map<Integer, Set<TimedActionC>> actions) {
        TimedAction startedGame = GameUtils.findFirstAction(EAction.START_GAME, actions);
        if (startedGame != null) {
            if (action.getType() == EAction.LOBBY_MSG || action.getType() == EAction.LOBBY_MSG_TIME) {
                String[] times = GameUtils.getTime(startedGame.getTime() - action.getTime());
                mm.broadcast(Msg.LOBBY_TIME, times[0], times[1]);
            }
        }
    }

    public static void lobbyMsg(TimedAction action, Map<Integer, Set<TimedActionC>> actions) {
        CaptureThePoint ctp = gm.getCaptureThePoint();
        lobbyMsgTime(action, actions);
        mm.broadcast(Msg.LOBBY_PLAYER_COUNT, ctp.getServer().getOnlinePlayers().length, ctp.getServer().getMaxPlayers(), config.getMinPlayers());
        mm.broadcast(Msg.LOBBY_VOTE);
        for (int i = 0; i < wm.getVotes().size(); i++) {
            mm.broadcast(Msg.LOBBY_MAP, i + 1, wm.getVotes().get(i).getTotalVotes(), wm.getVotes().get(i).getVoteAble().getName());
        }
    }

    public static void updatePoints(Game game) {
        if (game == null) {
            return;
        }
        EntrySet<Participant, Double> closest = new EntrySet<>();
        Location beacon = game.getCurrentBeacon().getLocation();
        double quickChange;
        for (Participant participant : game.getParticipants()) {
            if (participant == null) {
                continue;
            }
            if (!participant.isOnline()) {
                continue;
            }

            quickChange = distance(participant.getOnlinePlayer().getLocation(), beacon);
            if (closest.getKey() == null && quickChange < 1.5D) {
                closest.put(participant, quickChange);
            } else if (closest.getKey() != null && quickChange < closest.getValue() && quickChange < 1.5D) {
                closest.put(participant, quickChange);
            }
        }
        if (closest.getKey() != null && closest.getKey().isOnline()) {
            Participant p = closest.getKey();
            int bPoints = game.addBeaconPoints(p.getOfflinePlayer(), 1);
            p.getOnlinePlayer().setLevel(bPoints);
            mm.msg(p.getOnlinePlayer(), Msg.GAME_POINTS, bPoints);
        }
    }

    private static double distance(Location l1, Location l2) {
        double x = l1.getX() - l2.getX();
        double y = l1.getY() - l2.getY();
        double z = l1.getZ() - l2.getZ();
        return StrictMath.sqrt(x * x + y * y + z * z);
    }

    public static void restart() {
        CaptureThePoint.getTimerClass().setTime(0);
        try {
            gm.getStatsManager().updatePlayerRanks();
        } catch (Exception e) {
            LOG.log(Level.FINE, "Failed to push ranks");
            e.printStackTrace();
        }
    }

    public static void kickAll() {
        for (Player p : Bukkit.getServer().getOnlinePlayers()) {
            p.kickPlayer(gm.getMsgManager().getMsg(Msg.KICK_SERVER_REBOOTING));
        }
    }

    private static String winnerBuilder(Collection<Participant> participants) {
        StringBuilder nameBuff = new StringBuilder();
        for (Participant participant : participants) {
            nameBuff.append(", ").append(participant.getOfflinePlayer().getName());
        }
        String name = nameBuff.toString().trim();
        name = name.replaceFirst(", ", "");
        return name;
    }
}
