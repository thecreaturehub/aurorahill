package com.projectbarks.capturethepoint.game;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.scheduler.BukkitRunnable;

import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.api.Game;
import com.projectbarks.capturethepoint.api.events.BeaconChangeEvent;
import com.projectbarks.capturethepoint.api.events.GameEndEvent;
import com.projectbarks.capturethepoint.api.events.GameStartEvent;
import com.projectbarks.capturethepoint.api.events.LobbyEndEvent;
import com.projectbarks.capturethepoint.api.timed.EAction;
import com.projectbarks.capturethepoint.api.timed.TimedActionC;
import com.projectbarks.capturethepoint.game.scoreboards.ScoreboardManager;
import com.projectbarks.capturethepoint.maps.Beacon;
import com.projectbarks.capturethepoint.maps.BeaconChangeInterval;
import com.projectbarks.capturethepoint.misc.GameUtils;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class TimerC extends BukkitRunnable implements Timer {

    private final GlobalManager globalManager;
    private int time;
    private Map<Integer, Set<TimedActionC>> superActions;
    private Game game;
    private boolean pause;
    private BeaconChangeInterval bci;

    public TimerC(GlobalManager gm) {
        this.globalManager = gm;
        this.time = 0;
        this.superActions = new HashMap<>();
        this.game = null;
        this.pause = false;
        bci = null;
    }

    @Override
    public void run() {
        if (getSuperActions().isEmpty() || getSuperActions().size() <= 0) {
            schedualActions(globalManager.getConfig().getSchedule());
        }
        if (bci == null) {
            bci = new BeaconChangeInterval(this, 
            globalManager.getConfig().getMaxBeaconChangeInterval(), 
            globalManager.getConfig().getMinBeaconChangeInterval());
            bci.runTaskTimer(globalManager.getCaptureThePoint(), 0L, 20L);
        }

        if (this.getSuperActions().containsKey(this.time)) {
            Set<TimedActionC> amazingActions = this.getSuperActions().get(this.time);
            for (TimedActionC action : amazingActions) {
                try {
                    switch (action.getType()) {
                        case BROADCAST_GAME_TIME:
                            GameManager.gameMsgTime(action, getSuperActions());
                            break;
                        case END_GAME:
                            GameManager.endGame(this.game, false);
                            this.callEvent(new GameEndEvent(game));
                            this.game = null;
                            break;
                        case START_GAME:
                            Game sGame = GameManager.startGame();
                            if (sGame == null) {
                                this.time = 0;
                            } else {
                                this.callEvent(new LobbyEndEvent(globalManager.getConfig().getLobby()));
                                this.game = sGame;
                                this.callEvent(new GameStartEvent(game));
                            }
                            break;
                        case LOBBY_MSG:
                            GameManager.lobbyMsg(action, getSuperActions());
                            break;
                        case LOBBY_MSG_TIME:
                            GameManager.lobbyMsgTime(action, getSuperActions());
                            break;
                        case RESTART_GAME:
                            ScoreboardManager.reset();
                            GameManager.restart();
                            break;
                        case KICK_PLAYERS:
                            GameManager.kickAll();
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    globalManager.getCaptureThePoint().getLogger().log(Level.SEVERE, "Task {0}:{1} failed", new Object[]{action.getType().getAction(), action.getTime()});
                    e.printStackTrace();
                }
            }
        }
        if (!(pause)) {
            time++;
            BarManager.updateBar();
        }

        if (inGame()) {
            GameManager.updatePoints(game);
        }

        ScoreboardManager.update(this.getStatus());
    }

    //----Public Functions----//
    @Override
    public boolean inGame() {
        return GameUtils.findFirstAction(EAction.START_GAME, superActions).getTime() < time && !inGameOver();
    }

    @Override
    public boolean inLobby() {
        return GameUtils.findFirstAction(EAction.START_GAME, superActions).getTime() >= time;
    }

    @Override
    public boolean inGameOver() {
        return GameUtils.findFirstAction(EAction.END_GAME, superActions).getTime() < time;
    }

    @Override
    public TimerStatus getStatus() {
        if (inLobby()) {
            return TimerStatus.LOBBY;
        } else if (inGame()) {
            return TimerStatus.GAME;
        } else if (inGameOver()) {
            return TimerStatus.GAMEOVER;
        } else {
            return TimerStatus.LOBBY;
        }
    }

    @Override
    public boolean isInGame(Player player) {
        if (inGame()) {
            if (game.getPlayer(player) != null) {
                return true;
            }
        }
        return false;
    }

    public void pause(boolean bool) {
        this.pause = bool;
    }

    private void schedualActions(List<TimedActionC> list) {
        for (TimedActionC ta : list) {
            schedualAction(time + ta.getTime(), ta);
        }
    }

    private void schedualAction(int time, TimedActionC action) {
        if (this.getSuperActions().containsKey(time)) {
            this.getSuperActions().get(time).add(action);
        } else {
            Set<TimedActionC> sorenActions = new HashSet<>();
            sorenActions.add(action);
            this.getSuperActions().put(time, sorenActions);
        }
    }

    private void callEvent(Event event) {
        Bukkit.getPluginManager().callEvent(event);
    }

    protected void changeBeacon() {
        Beacon oldBeacon = game.getCurrentBeacon();
        GameManager.changeBeacon(game);
        this.callEvent(new BeaconChangeEvent(game, oldBeacon, game.getCurrentBeacon()));
    }

    @Override
    public int getTime() {
        return time;
    }

    @Override
    public Game getGame() {
        return game;
    }
    
    public void setTime(int time) {
        this.time = time;
    }
    
    public Map<Integer, Set<TimedActionC>> getSuperActions() {
        return superActions;
    }
}