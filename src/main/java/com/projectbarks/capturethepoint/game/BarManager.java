package com.projectbarks.capturethepoint.game;

import com.projectbarks.capturethepoint.CaptureThePoint;
import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.GlobalManagerC;
import com.projectbarks.capturethepoint.api.timed.EAction;
import com.projectbarks.capturethepoint.api.timed.TimedAction;
import com.projectbarks.capturethepoint.game.bar.BarAPI;
import com.projectbarks.capturethepoint.misc.GameUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class BarManager {

    public static void updateBar() {
        GlobalManager globalManager = GlobalManagerC.getGlobalManager();
        EAction mode;
        String title;
        int offset = 0;
        if (globalManager.getTimer().inGame()) {
            mode = EAction.END_GAME;
            offset = GameUtils.findFirstAction(EAction.START_GAME, CaptureThePoint.getTimerClass().getSuperActions()).getTime();
            title = "Game Timer";
        } else if (globalManager.getTimer().inGameOver()) {
            mode = EAction.RESTART_GAME;
            title = "Game Over";
        } else {
            mode = EAction.START_GAME;
            title = "Lobby Timer";
        }
        TimedAction endAction = GameUtils.findFirstAction(mode, CaptureThePoint.getTimerClass().getSuperActions());
        for (Player p : Bukkit.getOnlinePlayers()) {
            float health = (endAction.getTime() - globalManager.getTimer().getTime());

            String[] time = GameUtils.getTime((int) health);
            BarAPI.setMessage(p, ChatColor.BOLD + title + " || " + (Integer.parseInt(time[0]) + 1) + " " + time[1]);

            health /= endAction.getTime() - offset;
            health *= 100;
            health = health >= 2 ? health : 2;
            BarAPI.setHealth(p, health);
        }
    }

    private BarManager() {
    }
}
