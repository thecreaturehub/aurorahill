package com.projectbarks.capturethepoint.maps;

import java.util.UUID;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.material.MaterialData;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class CachedBlockState {
    
    private BlockState localState;
    @Getter
    private int x, y, z;
    @Getter
    private Material type;
    private byte raw;
    private MaterialData data;
    private UUID world;
    
    public CachedBlockState(Block block) {
        this(block.getState());
    }
    
    public CachedBlockState(BlockState state) {
        this.x = state.getX();
        this.y = state.getY();
        this.z = state.getZ();
        this.data = state.getData();
        this.raw = state.getRawData();
        this.type = state.getType();
        this.world = state.getWorld().getUID();
        this.localState = state;
    }
    
    public BlockState toBlockState() {
        localState = Bukkit.getWorld(world).getBlockAt(x, y, z).getState();
        localState.setData(data);
        localState.setRawData(raw);
        localState.setType(type);
        return localState;
    }
}
