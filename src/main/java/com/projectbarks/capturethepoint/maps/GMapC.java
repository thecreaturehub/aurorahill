package com.projectbarks.capturethepoint.maps;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import com.projectbarks.capturethepoint.CaptureThePoint;
import com.projectbarks.capturethepoint.misc.GameUtils;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class GMapC implements GMap {

    private World world;
    private final String name;
    private List<Beacon> beacons;
    private List<CachedLocation> spawnPoints;
    private Logger log = CaptureThePoint.LOG;

    public GMapC(World world, String name, Beacon[] beacons, CachedLocation[] spawnPoints) {
        this.world = world;
        this.name = name;
        this.beacons = new ArrayList<>();
        if (beacons != null) {
            this.beacons.addAll(Arrays.asList(beacons));
        }

        this.spawnPoints = new ArrayList<>();
        if (spawnPoints != null) {
            this.spawnPoints = Arrays.asList(spawnPoints);
        }
        if (world != null) {
            world.setAutoSave(false);
            world.setPVP(true);
            world.setMonsterSpawnLimit(0);
        }
    }

    @Override
    public void setBeacons(Beacon[] beacons) {
        this.beacons.clear();
        if (beacons != null) {
            this.beacons.addAll(Arrays.asList(beacons));
        }
    }

    @Override
    public void setSpawnPoints(CachedLocation[] spawnPoints) {
        this.spawnPoints.clear();
        if (spawnPoints != null) {
            this.spawnPoints.addAll(Arrays.asList(spawnPoints));
        }
    }

    @Override
    public void unloadChunks() {
        for (Chunk chunk : world.getLoadedChunks()) {
            chunk.unload(false);
        }
    }

    @Override
    public void rollback() {
        if (!Bukkit.getServer().unloadWorld(world, false)) {
            log.log(Level.INFO, "Could not unload {0}||{1}", new Object[]{world.getName(), name});
        }
        world = Bukkit.getServer().createWorld(new WorldCreator(world.getName()));
        world.setAutoSave(false);
        world.setPVP(true);
        world.setMonsterSpawnLimit(0);
    }

    @Override
    public void loadGameChunks() {
        List<Chunk> load = new ArrayList<>();
        for (CachedLocation loc : spawnPoints) {
            load.addAll(GameUtils.getChunkBox(loc.toLocation().getChunk()));
        }
        for (Beacon beacon : beacons) {
            Location loc = beacon.getLocation();
            load.addAll(GameUtils.getChunkBox(loc.getChunk()));
        }

        for (Chunk chunk : load) {
            if (!(chunk.isLoaded())) {
                chunk.load(true);
            }
        }
    }

    @Override
    public void addBeacon(Beacon beacon) {
        if (beacon != null) {
            beacons.add(beacon);
        }
    }

    @Override
    public void addSpawnpoint(Location location) {
        if (location != null) {
            spawnPoints.add(new CachedLocation(location));
        }
    }

    @Override
    public Location randomSpawnpoint() {
        Random random = new Random();
        return spawnPoints.get(random.nextInt(spawnPoints.size())).toLocation();
    }

    @Override
    public void cleanUp() {
        for (Beacon beacon : getBeacons()) {
            beacon.restore();
        }
        for (Entity entity : getWorld().getEntities()) {
            if (entity.getType().equals(EntityType.EXPERIENCE_ORB)
                || entity instanceof Arrow || entity instanceof Item
                || entity instanceof LivingEntity && !(entity instanceof Player)) {
                entity.remove();
            }
        }
        unloadChunks();
    }

    public static Beacon buildBeacon(Location loc) {
        try {
            List<Block> blocks = new ArrayList<>();
            Integer y = loc.getBlockY();
            for (int x = loc.getBlockX() - 1; x <= loc.getBlockX() + 1; x++) {
                for (int z = loc.getBlockZ() - 1; z <= loc.getBlockZ() + 1; z++) {
                    blocks.add((new Location(loc.getWorld(), x, y, z)).getBlock());
                }
            }

            Location newLoc = loc.clone();
            newLoc.setY(loc.getY() + 1);
            return new BeaconC(newLoc.getBlock(), blocks, 0);
        } catch (Exception ex) {
            Logger.getLogger(BeaconC.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public World getWorld() {
        return world;
    }

    @Override
    public List<Beacon> getBeacons() {
        return beacons;
    }

    @Override
    public List<CachedLocation> getSpawnPoints() {
        return spawnPoints;
    }

    @Override
    public String getName() {
        return name;
    }
}
