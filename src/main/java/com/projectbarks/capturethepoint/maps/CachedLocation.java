/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.projectbarks.capturethepoint.maps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Entity;

@ToString
@EqualsAndHashCode
@AllArgsConstructor
@SerializableAs("SkyLocationAccurate")
public class CachedLocation implements ConfigurationSerializable {

    private final double x, y, z, yaw, pitch;
    public final String world;

    public CachedLocation() {
        this(0, 0, 0, 0, 0, null);
    }

    public CachedLocation(double x, double y, double z, String world) {
        this(x, y, z, 0, 0, world);
    }

    public CachedLocation(double x, double y, double z, double yaw, double pitch, String world) {
        this.x = x;
        this.y = z;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
        this.world = world;
    }

    public CachedLocation(@NonNull Block block) {
        this(block.getX(), block.getY(), block.getZ(), block.getWorld() == null ? null : block.getWorld().getName());
    }

    public CachedLocation(@NonNull Location location) {
        this(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch(), location.getWorld() == null ? null : location.getWorld().getName());
    }

    public CachedLocation(@NonNull Entity entity) {
        this(entity.getLocation());
    }

    public CachedLocation add(double x, double y, double z) {
        return new CachedLocation(this.x + x, this.y + y, this.z + z, world);
    }

    public CachedLocation add(@NonNull CachedLocation location) {
        return new CachedLocation(this.x + location.x, this.y + location.y, this.z + location.z, world);
    }

    public CachedLocation subtract(@NonNull CachedLocation location) {
        return new CachedLocation(this.x - location.x, this.y - location.y, this.z - location.z, world);
    }

    public CachedLocation changeWorld(String newWorld) {
        return (newWorld == null ? world == null : newWorld.equals(world)) ? this : new CachedLocation(x, y, z, yaw, pitch, newWorld);
    }

    public Location toLocation() {
        World bukkitWorld = null;
        if (world != null) {
            bukkitWorld = Bukkit.getWorld(world);
        }
        if (bukkitWorld == null) {
            Bukkit.getLogger().log(Level.WARNING, "[CachedLocation] World ''{0}'' not found when {1}.toLocation() called", new Object[] { world, this });
        }
        return new Location(bukkitWorld, x, y, z);
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("x", x);
        map.put("y", y);
        map.put("z", z);
        if (yaw != 0) {
            map.put("yaw", yaw);
        }
        if (pitch != 0) {
            map.put("pitch", pitch);
        }
        if (world != null) {
            map.put("world", world);
        }
        return map;
    }

    public static CachedLocation[] fromArray(Location[] locations) {
        CachedLocation[] result = new CachedLocation[locations.length];
        for (int i = 0; i < locations.length; i++) {
            result[i] = new CachedLocation(locations[i]);
        }
        return result;
    }

    public static Location[] toArray(CachedLocation[] locations) {
        Location[] result = new Location[locations.length];
        for (int i = 0; i < locations.length; i++) {
            result[i] = locations[i].toLocation();
        }
        return result;
    }

    public static List<CachedLocation> fromList(List<Location> locations) {
        List<CachedLocation> result = new ArrayList<>(locations.size());
        for (Location location : locations) {
            result.add(new CachedLocation(location));
        }
        return result;
    }

    public static List<Location> toList(List<CachedLocation> locations) {
        List<Location> result = new ArrayList<>(locations.size());
        for (CachedLocation location : locations) {
            result.add(location.toLocation());
        }
        return result;
    }

    public void serialize(ConfigurationSection configurationSection) {
        configurationSection.set("x", x);
        configurationSection.set("y", y);
        configurationSection.set("z", z);
        if (yaw != 0) {
            configurationSection.set("yaw", yaw);
        }
        if (pitch != 0) {
            configurationSection.set("pitch", pitch);
        }
        if (world != null) {
            configurationSection.set("world", world);
        }
    }

    public static CachedLocation deserialize(@NonNull Map<String, Object> map) {
        Object worldO = map.get("world");
        Double x = get(map.get("x"));
        Double y = get(map.get("y"));
        Double z = get(map.get("z"));
        Double yaw = get(map.get("yaw"));
        Double pitch = get(map.get("pitch"));
        if (x == null || y == null || z == null) {
            x = get(map.get("xpos"));
            y = get(map.get("ypos"));
            z = get(map.get("zpos"));
            if (x == null || y == null || z == null) {
                Bukkit.getLogger().log(Level.WARNING, "[CachedLocation] Silently failing deserialization due to x, y or z not existing on map or not being valid doubles.");
                return null;
            }
        }
        String world = worldO == null ? null : worldO instanceof String ? (String) worldO : worldO.toString();
        return new CachedLocation(x, y, z, yaw == null ? 0 : yaw, pitch == null ? 0 : pitch, world);
    }

    public static CachedLocation deserialize(@NonNull ConfigurationSection configurationSection) {
        Object worldO = configurationSection.get("world");
        Double x = get(configurationSection.get("x"));
        Double y = get(configurationSection.get("y"));
        Double z = get(configurationSection.get("z"));
        Double yaw = get(configurationSection.get("yaw"));
        Double pitch = get(configurationSection.get("pitch"));
        if (x == null || y == null || z == null) {
            x = get(configurationSection.get("xpos"));
            y = get(configurationSection.get("ypos"));
            z = get(configurationSection.get("zpos"));
            if (x == null || y == null || z == null) {
                Bukkit.getLogger().log(Level.WARNING, "[CachedLocation] Silently failing deserialization due to x, y or z not existing on map or not being valid doubles.");
                return null;
            }
        }
        String world = worldO == null ? null : worldO instanceof String ? (String) worldO : worldO.toString();
        return new CachedLocation(x, y, z, yaw == null ? 0 : yaw, pitch == null ? 0 : pitch, world);
    }

    private static Double get(Object o) {
        if (o == null) {
            return null;
        }
        if (o instanceof Integer) {
            return Double.valueOf(((Integer) o).doubleValue());
        }
        if (o instanceof Double) {
            return (Double) o;
        }
        return null;
    }
    
    public double getX() {
        return x;
    }
    
    public double getY() {
        return y;
    }
    
    public double getZ() {
        return z;
    }
}
