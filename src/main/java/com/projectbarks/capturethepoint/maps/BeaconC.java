package com.projectbarks.capturethepoint.maps;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class BeaconC implements Beacon, Cloneable {

    private final CachedBlockState beaconBlock;
    private final List<CachedBlockState> baseBlocks;
    private Integer multiplier;

    public BeaconC(Block beacon, Collection<Block> blocks, Integer multiplier) throws Exception {
        this.baseBlocks = new ArrayList<>();
        if (blocks.size() < 9) {
            throw new Exception("Too few too many arguments in blocks collection");
        }

        for (Block block : blocks) {
            this.baseBlocks.add(new CachedBlockState(block));
        }

        this.beaconBlock = new CachedBlockState(beacon);

        final Integer m;
        if (multiplier == null || multiplier <= 0) {
            m = 1;
        } else {
            m = multiplier;
        }

        this.multiplier = m;
    }

    protected BeaconC(CachedBlockState beacon, Collection<CachedBlockState> blocks, Integer multiplier) {
        this.beaconBlock = beacon;
        this.baseBlocks = new ArrayList<>(9);
        this.baseBlocks.addAll(blocks);
        final Integer m;
        if (multiplier == null || multiplier <= 0) {
            m = 1;
        } else {
            m = multiplier;
        }

        this.multiplier = m;
    }

    @Override
    public BeaconC clone() {
        return new BeaconC(beaconBlock, baseBlocks, multiplier);
    }

    @Override
    public void applyBeacon(Material base, Material beacon) {
        for (CachedBlockState block : baseBlocks) {
            BlockState toBlockState = block.toBlockState();
            toBlockState.setType(base);
            toBlockState.update(true);
        }
        BlockState toBlockState = beaconBlock.toBlockState();
        toBlockState.setType(beacon);
        toBlockState.update(true);
    }

    @Override
    public void restore() {
        updateBeacon(true, true);
    }

    public void updateBeacon(Boolean bool, boolean forceRestorel) {
        for (CachedBlockState block : baseBlocks) {
            BlockState toBlockState = block.toBlockState();
            toBlockState.update(bool);
        }

        beaconBlock.toBlockState().update(bool);
    }

    @Override
    public Location getLocation() {
        return this.beaconBlock.toBlockState().getLocation().clone();
    }

    @Override
    public BlockState getBeaconBlock() {
        return this.beaconBlock.toBlockState();
    }

    @Override
    public List<BlockState> getBaseBlocks() {
        List<BlockState> list = new ArrayList<>();
        for (CachedBlockState s : this.baseBlocks) {
            list.add(s.toBlockState());
        }
        return list;
    }

    @Override
    public Integer getMultiplier() {
        return multiplier;
    }

    @Override
    public void setMultiplier(Integer multiplier) {
        this.multiplier = multiplier;
    }
}
