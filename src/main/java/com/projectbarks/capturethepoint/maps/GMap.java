package com.projectbarks.capturethepoint.maps;

import java.util.List;
import org.bukkit.Location;
import org.bukkit.World;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public interface GMap {

    /**
     * Gets the world assigned to this GMap
     *
     * @return
     */
    public World getWorld();

    /**
     * Returns an array of locations that can be used for beacons in the game.
     * Null will never be returned rather a empty array.
     *
     * @return
     */
    public List<Beacon> getBeacons();

    /**
     * Sets the beacons in the GMap. Null will be converted into an empty array
     * when ran through setBeacons.
     *
     * @param beacons can be null
     */
    public void setBeacons(Beacon[] beacons);

    /**
     * Returns an array of locations that can be used for spawnpoints in the
     * game.
     *
     * @return
     */
    public List<CachedLocation> getSpawnPoints();

    /**
     * Sets the beacons in the GMap. Null will be converted into an empty array
     * when ran through setBeacons.
     *
     * @param spawnPoints
     */
    public void setSpawnPoints(CachedLocation[] spawnPoints);

    /**
     * Returns the display name of the Object.
     *
     * @return
     */
    public String getName();

    /**
     * Unloads all chunks in the getWorld(). Helps decrease ram usage.
     */
    public void unloadChunks();

    /**
     * Loads the beacons and spawnpoints chunks from there location. This
     * increases ram usage.
     */
    public void loadGameChunks();

    public void addBeacon(Beacon beacon);

    public void addSpawnpoint(Location location);

    public void cleanUp();

    public Location randomSpawnpoint();

    public void rollback();
}
