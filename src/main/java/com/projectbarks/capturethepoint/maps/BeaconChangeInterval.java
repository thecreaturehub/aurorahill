package com.projectbarks.capturethepoint.maps;

import com.projectbarks.capturethepoint.CaptureThePoint;
import com.projectbarks.capturethepoint.game.TimerC;

public class BeaconChangeInterval {

    private TimerC timer;
    private Integer maxBeaconChangeInterval;
    private Integer minBeaconChangeInterval;

    public BeaconChangeInterval(TimerC timer, Integer maxBeaconChangeInterval, Integer minBeaconChangeInterval) {
        this.timer = timer;
        this.maxBeaconChangeInterval = maxBeaconChangeInterval;
        this.minBeaconChangeInterval = minBeaconChangeInterval;
    }

    public TimerC getTimer() {
        return timer;
    }

    public Integer getMaxBeaconChangeInterval() {
        return maxBeaconChangeInterval;
    }

    public Integer getMinBeaconChangeInterval() {
        return minBeaconChangeInterval;
    }

    public void runTaskTimer(CaptureThePoint captureThePoint, long l, long m) {
        // TODO: what do?
    }
}
