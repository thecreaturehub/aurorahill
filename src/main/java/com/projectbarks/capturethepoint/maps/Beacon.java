package com.projectbarks.capturethepoint.maps;

import java.util.List;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockState;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public interface Beacon {

    /**
     * Clones the {@link BeaconC}
     *
     * @return
     */
    public BeaconC clone();

    /**
     * Gets the block designated to be the beacon.
     *
     * @return
     */
    public BlockState getBeaconBlock();

    /**
     * Gets the blocks designated to be the base blocks.
     *
     * @return
     */
    public List<BlockState> getBaseBlocks();

    /**
     * Gets integer multiplier using in giving players beacon points.
     *
     * @return
     */
    public Integer getMultiplier();

    /**
     * Sets integer multiplier using in giving players beacon points.
     *
     * @param multiplier
     */
    public void setMultiplier(Integer multiplier);

    /**
     * Changes the blocks according to material inputed to the function.
     * Inserting a bad block type (Non block materials) may result in an error.
     *
     * @param base
     * @param beacon
     */
    public void applyBeacon(Material base, Material beacon);
    
    public void restore();

    public Location getLocation();
}
