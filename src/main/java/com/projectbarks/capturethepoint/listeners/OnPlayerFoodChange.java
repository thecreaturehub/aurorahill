package com.projectbarks.capturethepoint.listeners;

import com.projectbarks.capturethepoint.EventCore;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventException;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.plugin.EventExecutor;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class OnPlayerFoodChange implements CTPEventHandler {

    public OnPlayerFoodChange() {
        EventCore.register(FoodLevelChangeEvent.class, EventPriority.MONITOR, this, true);
    }

    public void onPlayerFoodChange(FoodLevelChangeEvent event) {
        if (event.getEntity() instanceof Player) {
            event.setFoodLevel(20);
            ((Player) event.getEntity()).setSaturation(20);
        }
    }

    @Override
    public void execute(Listener ll, Event event) throws EventException {
        this.onPlayerFoodChange((FoodLevelChangeEvent) event);
    }
}
