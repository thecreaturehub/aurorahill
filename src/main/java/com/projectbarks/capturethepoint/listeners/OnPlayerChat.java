package com.projectbarks.capturethepoint.listeners;

import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.event.Event;
import org.bukkit.event.EventException;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.projectbarks.capturethepoint.EventCore;
import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.config.Config;
import com.projectbarks.capturethepoint.kits.KitManager;
import com.projectbarks.capturethepoint.stats.StatType;
import com.projectbarks.capturethepoint.stats.StatsManager;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class OnPlayerChat implements CTPEventHandler {

    private Config config;
    private KitManager km;
    private String format;
    private StatsManager stats;

    public OnPlayerChat(GlobalManager gm) {
        this.config = gm.getConfig();
        this.km = gm.getKitManager();
        this.stats = gm.getStatsManager();
        this.format = null;
        EventCore.register(AsyncPlayerChatEvent.class, EventPriority.LOWEST, this, true);
    }

    public void onPlayerChat(AsyncPlayerChatEvent event) {
        if (format == null) {
            String chatFormat = this.config.getChatFormat();
            chatFormat = chatFormat.replace("(name)", "%s");
            chatFormat = chatFormat.replace("(message)", "%s");
            if (!(StringUtils.countMatches(chatFormat, "%s") >= 2)) {
                chatFormat = "broken[%s]: %s";
            }

            this.format = ChatColor.translateAlternateColorCodes('&', chatFormat);
        }
        if (this.config.isModifyChat()) {
            int money = ((int) km.getEconomy().getBalance(event.getPlayer().getName()));
            String finalFormat = format;
            finalFormat = finalFormat.replace("(money)", money + "");
            int rp = stats.getStats(event.getPlayer()).getStat(StatType.RankPoints);
            finalFormat = finalFormat.replace("(rp)", rp + "");
            event.setFormat(finalFormat);
        }
    }

    @Override
    public void execute(Listener ll, Event event) throws EventException {
        if (event instanceof AsyncPlayerChatEvent) {
            this.onPlayerChat((AsyncPlayerChatEvent) event);
        }
    }
}
