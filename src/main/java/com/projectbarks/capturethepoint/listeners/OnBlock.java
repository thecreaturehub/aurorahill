package com.projectbarks.capturethepoint.listeners;

import com.projectbarks.capturethepoint.EventCore;
import org.bukkit.event.Event;
import org.bukkit.event.EventException;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.plugin.EventExecutor;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class OnBlock implements CTPEventHandler {

    public OnBlock() {
        EventCore.register(BlockPlaceEvent.class, EventPriority.HIGHEST, this, true);
        EventCore.register(BlockBreakEvent.class, EventPriority.HIGHEST, this, true);
        EventCore.register(EntityChangeBlockEvent.class, EventPriority.HIGHEST, this, true);
        EventCore.register(EntityExplodeEvent.class, EventPriority.HIGHEST, this, false);
        EventCore.register(BlockBurnEvent.class, EventPriority.HIGHEST, this, true);
        EventCore.register(BlockFormEvent.class, EventPriority.HIGHEST, this, true);
        EventCore.register(BlockIgniteEvent.class, EventPriority.HIGHEST, this, true);
    }

    public void onBlockPlace(BlockPlaceEvent event) {
        if (!(event.getPlayer().hasPermission("capturethepoint.modifyblock"))) {
            event.setCancelled(true);
        }
    }

    public void onBlockBreak(BlockBreakEvent event) {
        if (!(event.getPlayer().hasPermission("capturethepoint.modifyblock"))) {
            event.setCancelled(true);
        }
    }

    public void onEntityChangeBlock(EntityChangeBlockEvent event) {
        event.setCancelled(true);
    }

    public void onEntityExplode(EntityExplodeEvent event) {
        event.blockList().clear();
        event.setCancelled(true);
    }

    public void onBlockBurn(BlockBurnEvent event) {
        event.setCancelled(true);
    }

    public void onBlockForm(BlockFormEvent event) {
        event.setCancelled(true);
    }

    public void onBlockIgnite(BlockIgniteEvent event) {
        event.setCancelled(true);
    }

    @Override
    public void execute(Listener ll, Event event) throws EventException {
        if (event instanceof BlockPlaceEvent) {
            this.onBlockPlace((BlockPlaceEvent) event);
        } else if (event instanceof BlockBreakEvent) {
            this.onBlockBreak((BlockBreakEvent) event);
        } else if (event instanceof EntityChangeBlockEvent) {
            this.onEntityChangeBlock((EntityChangeBlockEvent) event);
        } else if (event instanceof EntityExplodeEvent) {
            this.onEntityExplode((EntityExplodeEvent) event);
        } else if (event instanceof BlockBurnEvent) {
            this.onBlockBurn((BlockBurnEvent) event);
        } else if (event instanceof BlockFormEvent) {
            this.onBlockForm((BlockFormEvent) event);
        } else if (event instanceof BlockIgniteEvent) {
            this.onBlockIgnite((BlockIgniteEvent) event);
        }
    }
}
