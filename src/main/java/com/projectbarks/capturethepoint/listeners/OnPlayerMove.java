package com.projectbarks.capturethepoint.listeners;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventException;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;

import com.projectbarks.capturethepoint.EventCore;
import com.projectbarks.capturethepoint.GlobalManager;

public class OnPlayerMove implements CTPEventHandler {

    private GlobalManager gm;

    public OnPlayerMove(GlobalManager gm) {
        this.gm = gm;
        EventCore.register(PlayerMoveEvent.class, EventPriority.HIGHEST, this, true);
        EventCore.register(PlayerToggleFlightEvent.class, EventPriority.HIGHEST, this, true);
    }

    @EventHandler
    public void onPlayerFlight(PlayerToggleFlightEvent event) {
        Player player = event.getPlayer();
        if (player.getGameMode() != GameMode.CREATIVE) {
            event.setCancelled(true);
            player.setAllowFlight(false);
            player.setFlying(false);
            player.setVelocity(player.getLocation().getDirection().multiply(.25).setY(1));
        }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        if (gm.getTimer().inLobby()) {
            return;
        }
        Player player = event.getPlayer();
        if (player.getGameMode() != GameMode.CREATIVE && player.getLocation().subtract(0, 1, 0).getBlock().getType() != Material.AIR) {
            player.setAllowFlight(true);
        }
    }

    @Override
    public void execute(Listener l, Event event) throws EventException {
        if (event instanceof PlayerMoveEvent) {
            this.onPlayerMove((PlayerMoveEvent) event);
        }

        else if (event instanceof PlayerToggleFlightEvent) {
            this.onPlayerFlight((PlayerToggleFlightEvent) event);
        }
    }
}
