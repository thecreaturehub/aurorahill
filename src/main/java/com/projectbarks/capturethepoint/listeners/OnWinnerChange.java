package com.projectbarks.capturethepoint.listeners;

import org.bukkit.event.Event;
import org.bukkit.event.EventException;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import com.projectbarks.capturethepoint.EventCore;
import com.projectbarks.capturethepoint.GlobalManagerC;
import com.projectbarks.capturethepoint.api.events.WinnerChangeEvent;
import com.projectbarks.capturethepoint.messages.Msg;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class OnWinnerChange implements CTPEventHandler {

    public OnWinnerChange() {
        EventCore.register(WinnerChangeEvent.class, EventPriority.MONITOR, this, true);
    }

    public void onWinnerChange(WinnerChangeEvent event) {
        if (event.getNewWinner() != null) {
            GlobalManagerC.getGlobalManager().getMsgManager().broadcast(Msg.GAME_WINNER_CHANGED, event.getNewWinner().getOfflinePlayer().getName());
        }
    }

    @Override
    public void execute(Listener ll, Event event) throws EventException {
        if (event instanceof WinnerChangeEvent) {
            this.onWinnerChange((WinnerChangeEvent) event);
        }
    }
}
