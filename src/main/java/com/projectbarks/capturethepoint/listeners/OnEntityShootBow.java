package com.projectbarks.capturethepoint.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventException;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;

import com.projectbarks.capturethepoint.EventCore;
import com.projectbarks.capturethepoint.GlobalManager;

public class OnEntityShootBow implements CTPEventHandler {

    private GlobalManager gm;

    public OnEntityShootBow(GlobalManager gm) {
        this.gm = gm;
        EventCore.register(EntityShootBowEvent.class, EventPriority.LOWEST, this, true);
    }

    public void onEntityShootBow(EntityShootBowEvent event) {
        if (event.getEntity() instanceof Player) {
            if (gm.getTimer().inLobby()) {
                event.setCancelled(true);
            }
        }
    }

    @Override
    public void execute(Listener l, Event event) throws EventException {
        this.onEntityShootBow((EntityShootBowEvent) event);
    }
}
