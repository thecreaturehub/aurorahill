package com.projectbarks.capturethepoint.listeners;

import org.bukkit.Material;
import org.bukkit.event.Event;
import org.bukkit.event.EventException;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;

import com.projectbarks.capturethepoint.EventCore;
import com.projectbarks.capturethepoint.config.Config;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class OnPotionDrink implements CTPEventHandler {

    private Config config;

    public OnPotionDrink(Config config) {
        this.config = config;
        EventCore.register(PlayerInteractEvent.class, EventPriority.NORMAL, this, true);
    }

    private int getPotionEffectBits(ItemStack item) {
        return item.getDurability() & 0x3F;
    }

    public void onPlayerClick(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK && event.hasItem() && config.isQuickPotions()) {
            ItemStack item = event.getItem();
            if (item.getType() == Material.POTION && item.getDurability() != 0) {
                Potion potion = Potion.fromDamage(getPotionEffectBits(item));

                if (potion.isSplash()) {
                    return;
                }

                for (PotionEffect effect : potion.getEffects()) {
                    event.getPlayer().addPotionEffect(effect);
                }

                if (item.getAmount() > 1) {
                    item.setAmount(item.getAmount() - 1);
                } else {
                    event.getPlayer().getInventory().setItem(event.getPlayer().getInventory().getHeldItemSlot(), null);
                }

                event.getPlayer().updateInventory();
                if (!this.config.isRemovePotionBottle()) {
                    event.getPlayer().getInventory().addItem(new ItemStack(Material.GLASS_BOTTLE, 1));
                }

                event.setUseItemInHand(Event.Result.DENY);
                event.setUseInteractedBlock(Event.Result.DENY);
            }
        }
    }

    @Override
    public void execute(Listener ll, Event event) throws EventException {
        this.onPlayerClick((PlayerInteractEvent) event);
    }
}
