package com.projectbarks.capturethepoint.listeners;

import com.projectbarks.capturethepoint.EventCore;
import org.bukkit.event.Event;
import org.bukkit.event.EventException;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 *
 * @author Brandon Barker
 */
public class OnCreatureSpawn implements CTPEventHandler  {
    
    public OnCreatureSpawn() {
        EventCore.register(CreatureSpawnEvent.class, EventPriority.HIGHEST, this, true);
    }
    
    public void onEntitySpawn(CreatureSpawnEvent event) {
        event.setCancelled(true);
    }

    @Override
    public void execute(Listener ll, Event event) throws EventException {
               if (event instanceof CreatureSpawnEvent) {
            this.onEntitySpawn((CreatureSpawnEvent) event);
        }
    }

}
