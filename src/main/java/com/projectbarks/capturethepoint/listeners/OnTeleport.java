package com.projectbarks.capturethepoint.listeners;

import com.projectbarks.capturethepoint.EventCore;
import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.GlobalManagerC;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.event.Event;
import org.bukkit.event.EventException;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.world.ChunkUnloadEvent;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class OnTeleport implements CTPEventHandler {

    private GlobalManager gm;

    public OnTeleport() {
        EventCore.register(PlayerTeleportEvent.class, EventPriority.MONITOR, this, true);
        gm = GlobalManagerC.getGlobalManager();
    }

    public void onPlayerTeleport(PlayerTeleportEvent event) {
        gm.getBarAPI().handleTeleport(event.getPlayer(), event.getTo().clone());
        Chunk to = event.getTo().getChunk();
        if (!to.isLoaded()) {
            to.load(true);
        }
    }

    @Override
    public void execute(Listener ll, Event event) throws EventException {
        this.onPlayerTeleport((PlayerTeleportEvent) event);
    }
}
