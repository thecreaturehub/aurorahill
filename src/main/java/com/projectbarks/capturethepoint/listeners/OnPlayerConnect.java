package com.projectbarks.capturethepoint.listeners;

import com.projectbarks.capturethepoint.CaptureThePoint;
import com.projectbarks.capturethepoint.EventCore;
import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.api.Game;
import com.projectbarks.capturethepoint.api.timed.EAction;
import com.projectbarks.capturethepoint.api.timed.TimedAction;
import com.projectbarks.capturethepoint.game.TimerC;
import com.projectbarks.capturethepoint.game.scoreboards.ScoreboardManager;
import com.projectbarks.capturethepoint.kits.KitManager;
import com.projectbarks.capturethepoint.kits.backend.Kit;
import com.projectbarks.capturethepoint.messages.Msg;
import com.projectbarks.capturethepoint.misc.DelayedTeleport;
import com.projectbarks.capturethepoint.misc.GameUtils;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventException;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class OnPlayerConnect implements CTPEventHandler {

    private GlobalManager gm;
    private TimerC timer;

    public OnPlayerConnect(GlobalManager gm) {
        this.timer = CaptureThePoint.getTimerClass();
        this.gm = gm;
        EventCore.register(PlayerJoinEvent.class, EventPriority.LOW, this, false);
        EventCore.register(PlayerLoginEvent.class, EventPriority.LOW, this, false);
        EventCore.register(PlayerQuitEvent.class, EventPriority.HIGHEST, this, false);
        EventCore.register(PlayerKickEvent.class, EventPriority.HIGHEST, this, false);
    }

    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        String msg = gm.getConfig().isDeathMessages() ? gm.getMsgManager().getMsg(Msg.CONNECT, player.getName()) : null;
        event.setJoinMessage(msg);
        player.setScoreboard(ScoreboardManager.getCurrentScoreboard().getScoreboard());
        Location loc = null;
        GameUtils.resetPlayer(player, GameUtils.PlayerInfo.values());
        if (this.timer.inGame()) {
            if (!(player.isDead())) {
                Game game = this.timer.getGame();
                KitManager km = gm.getKitManager();
                Kit kit = km.getKit(game.getPlayer(player).getKitName());
                km.applyKit(player, kit);
                loc = game.getMap().randomSpawnpoint();
                player.setCompassTarget(game.getCurrentBeacon().getLocation());
            }
        } else {
            player.getInventory().addItem(gm.getConfig().getKitSelector());
            loc = gm.getConfig().getLobby();
            gm.getMsgManager().msg(player, Msg.WELCOME);
        }

        if (!(player.isDead()) && loc != null) {
            if (!player.hasPlayedBefore()) {
                (new DelayedTeleport(player, loc)).runTaskLater(gm.getCaptureThePoint(), 2L);
            } else {
                player.teleport(loc);
            }
        }

        List<Chunk> chunks = GameUtils.getChunkBox(player.getLocation().getChunk());
        for (Chunk chunk : chunks) {
            if (!(chunk.isLoaded())) {
                chunk.load(true);
            }
        }
    }

    public void onPlayerLogin(PlayerLoginEvent event) {
        if (timer.inGame() && !timer.isInGame(event.getPlayer())) {
            event.disallow(PlayerLoginEvent.Result.KICK_OTHER, gm.getMsgManager().getMsg(Msg.KICK_GAME));
        }
    }

    public void onPlayerQuit(PlayerQuitEvent event) {
        gm.getBarAPI().quit(event.getPlayer());
        String msg = gm.getConfig().isDeathMessages() ? gm.getMsgManager().getMsg(Msg.DISCONNECT, event.getPlayer().getName()) : null;
        event.setQuitMessage(msg);
        if (this.timer.inGame()) {
            if (Bukkit.getOnlinePlayers().length <= 1) {
                TimedAction action = GameUtils.findFirstAction(EAction.END_GAME, timer.getSuperActions());
                this.timer.setTime(action.getTime() - 5);
            }
        }
    }

    public void onPlayerKick(PlayerKickEvent event) {
        gm.getBarAPI().quit(event.getPlayer());
        event.setLeaveMessage(gm.getMsgManager().getMsg(Msg.DISCONNECT, event.getPlayer().getName()));
        if (this.timer.inGame()) {
            if (Bukkit.getOnlinePlayers().length <= 1) {
                TimedAction action = GameUtils.findFirstAction(EAction.END_GAME, timer.getSuperActions());
                this.timer.setTime(action.getTime() - 5);
            }
        }
    }

    @Override
    public void execute(Listener ll, Event event) throws EventException {
        if (event instanceof PlayerJoinEvent) {
            this.onPlayerJoin((PlayerJoinEvent) event);
        } else if (event instanceof PlayerLoginEvent) {
            this.onPlayerLogin((PlayerLoginEvent) event);
        } else if (event instanceof PlayerQuitEvent) {
            this.onPlayerQuit((PlayerQuitEvent) event);
        } else if (event instanceof PlayerKickEvent) {
            this.onPlayerKick((PlayerKickEvent) event);
        }
    }
}
