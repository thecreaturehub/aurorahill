package com.projectbarks.capturethepoint.listeners;

import com.projectbarks.capturethepoint.EventCore;
import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.kits.backend.Kit;
import com.projectbarks.capturethepoint.messages.Msg;
import com.projectbarks.capturethepoint.misc.DelayedCompassPointSet;
import com.projectbarks.capturethepoint.misc.GameUtils;
import com.projectbarks.capturethepoint.misc.GameUtils.PlayerInfo;
import com.projectbarks.capturethepoint.stats.StatType;
import org.bukkit.Effect;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventException;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class OnPlayerDeath implements CTPEventHandler {
    
    private GlobalManager gm;
    
    public OnPlayerDeath(GlobalManager gm) {
        this.gm = gm;
        EventCore.register(PlayerDeathEvent.class, EventPriority.LOW, this, false);
        EventCore.register(PlayerRespawnEvent.class, EventPriority.LOW, this, false);
        EventCore.register(EntityDamageEvent.class, EventPriority.LOW, this, false);
    }
    
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();
        event.getDrops().clear();
        event.setKeepLevel(true);
        event.setDroppedExp(0);
        String msg = gm.getConfig().isDeathMessages() ? gm.getMsgManager().getMsg(Msg.DEATH, player.getName()) : null;
        event.setDeathMessage(msg);
        gm.getStatsManager().getStats(player).addAmount(StatType.Deaths, 1);
        Player killer = player.getKiller();
        if (killer != null) {
            gm.getStatsManager().getStats(killer).addAmount(StatType.Kills, 1);
        }
    }
    
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        gm.getBarAPI().handleTeleport(event.getPlayer(), event.getRespawnLocation().clone());
        Player player = event.getPlayer();
        GameUtils.resetPlayer(player, GameUtils.PlayerInfo.values());
        if (!(gm.getTimer().inGame())) {
            player.getInventory().addItem(gm.getConfig().getKitSelector());
            event.setRespawnLocation(gm.getConfig().getLobby());
        } else {
            GameUtils.resetPlayer(player, PlayerInfo.HEALTH, PlayerInfo.HUNGER);
            Kit kit = gm.getKitManager().getKit(gm.getTimer().getGame().getPlayer(player).getKitName());
            gm.getKitManager().applyKit(player, kit);
            event.setRespawnLocation(gm.getTimer().getGame().getMap().randomSpawnpoint());
            DelayedCompassPointSet toBeFiredEvent = new DelayedCompassPointSet(player, gm.getTimer().getGame().getCurrentBeacon().getLocation());
            toBeFiredEvent.scheduleForFire(gm.getCaptureThePoint());
        }
    }

    /**
     *
     * @param event
     */
    public void onEntityDamage(EntityDamageEvent event) {
        Entity entity = event.getEntity();
        if (gm.getTimer().inLobby() || gm.getTimer().inGameOver()) {
            if (entity instanceof LivingEntity) {
                event.setDamage(0);
                event.setCancelled(true);
            }
        } else {
            if (event.getCause() == DamageCause.VOID) {
                if (entity instanceof LivingEntity) {
                    ((LivingEntity) entity).setHealth(0);
                }
            }
            entity.getWorld().playEffect(entity.getLocation(), Effect.STEP_SOUND, 152);
            entity.getWorld().playEffect(entity.getLocation().clone().add(0, 1, 0), Effect.STEP_SOUND, 152);
        }
    }
    
    @Override
    public void execute(Listener ll, Event event) throws EventException {
        if (event instanceof PlayerDeathEvent) {
            this.onPlayerDeath((PlayerDeathEvent) event);
        } else if (event instanceof PlayerRespawnEvent) {
            this.onPlayerRespawn((PlayerRespawnEvent) event);
        } else if (event instanceof EntityDamageEvent) {
            this.onEntityDamage((EntityDamageEvent) event);
        }
    }
}
