package com.projectbarks.capturethepoint.listeners;

import com.projectbarks.capturethepoint.EventCore;
import com.projectbarks.capturethepoint.GlobalManagerC;
import com.projectbarks.capturethepoint.game.Timer;
import org.bukkit.ChatColor;
import org.bukkit.event.Event;
import org.bukkit.event.EventException;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.server.ServerListPingEvent;

/**
 *
 * @author Brandon Barker
 */
public class OnPing implements CTPEventHandler {

    public OnPing() {
        EventCore.register(ServerListPingEvent.class, EventPriority.LOWEST, this, true);
    }

    public void onPing(ServerListPingEvent event) {
        Timer time = GlobalManagerC.getGlobalManager().getTimer();
        String motd;
        if (time.inGame()) {
            motd = ChatColor.GRAY + "In Game";
        } else if (time.inGameOver()) {
            motd = ChatColor.RED + "Restarting";
        } else if (time.inLobby()) {
            motd = ChatColor.GREEN + "Waiting";
        }
        event.setMotd(null);
    }

    @Override
    public void execute(Listener ll, Event event) throws EventException {
        if (event instanceof CreatureSpawnEvent) {
            this.onPing((ServerListPingEvent) event);
        }
    }
}
