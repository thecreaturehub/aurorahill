package com.projectbarks.capturethepoint;

import com.projectbarks.capturethepoint.listeners.CTPEventHandler;
import org.bukkit.event.Event;
import org.bukkit.event.EventPriority;
import org.bukkit.plugin.Plugin;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class EventCore {

    private static Plugin plugin;

    public static void register(Class<? extends Event> bukkitEventClass, CTPEventHandler handler) {
        register(bukkitEventClass, EventPriority.NORMAL, handler);
    }

    public static void register(Class<? extends Event> bukkitEventClass, EventPriority priority, CTPEventHandler handler) {
        register(bukkitEventClass, priority, handler, true);
    }

    public static void register(Class<? extends Event> bukkitEventClass, EventPriority priority, CTPEventHandler handler, boolean ignoresCancelled) {
        if (plugin == null) {
            plugin = GlobalManagerC.getGlobalManager().getCaptureThePoint();
        }
        plugin.getServer().getPluginManager().registerEvent(bukkitEventClass, handler, priority, handler, plugin, ignoresCancelled);
    }

    private EventCore() {
    }
}
