package com.projectbarks.capturethepoint.stats;

import com.hcherndon.jsqlapi.JCaller;
import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.api.Participant;
import com.projectbarks.capturethepoint.config.extensions.JSQLBackend;
import java.util.Collection;
import java.util.HashMap;
import org.bukkit.OfflinePlayer;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class StatsManager {

    private HashMap<String, PlayerStats> loadedStats;
    private JSQLBackend $;

    public StatsManager(GlobalManager gm) {
        this.loadedStats = new HashMap<>();
        $ = gm.getJsqlBackend();
    }

    public void distributePoints(Collection<Participant> winners, Collection<Participant> players, double percentOfDisperse) {
        int dispersePoints = 0;
        for (Participant player : players) {
            boolean toBreak = false;
            for (Participant winner : winners) {
                if (player.getOfflinePlayer().getName().equals(winner.getOfflinePlayer().getName())) {
                    toBreak = true;
                }
            }
            if (toBreak) {
                continue;
            }
            PlayerStats stats = getStats(player.getOfflinePlayer());
            double staticMover = (stats.getStat(StatType.RankPoints) * percentOfDisperse);
            stats.addAmount(StatType.RankPoints, ((int) staticMover) * -1);
            dispersePoints += staticMover;
        }
        for (Participant winner : winners) {
            PlayerStats stats = this.getStats(winner.getOfflinePlayer());
            int pCount = (players.size() - winners.size());
            pCount = pCount <= 0 ? 1 : pCount;
            stats.addAmount(StatType.RankPoints, dispersePoints / pCount);
        }
    }

    public void updatePlayerRanks() {
        this.updateStats();
        this.loadAllStats();
        int level = 1;
        for (PlayerStats stats : this.loadedStats.values()) {
            stats.setAmount(StatType.CachedRank, level);
            level++;
        }
        this.saveLoadedStats();
        this.loadedStats.clear();
    }

    public void updateStats() {
        this.saveLoadedStats();
        this.getLoadedStats().clear();
    }

    public Collection<PlayerStats> getLoadedStats() {
        return loadedStats.values();
    }

    public void saveLoadedStats() {
        for (PlayerStats p : this.getLoadedStats()) {
            $.savePlayer(p);
        }
    }

    public void loadAllStats() {
        $.getAllStats(new JCaller<PlayerStats[]>() {
            @Override
            public void onTaskCompletion(String key, PlayerStats[] object) {
                for (PlayerStats stats : object) {
                    loadedStats.put(key, stats);
                }
            }
        });
    }

    public PlayerStats getStats(OfflinePlayer player) {
        if (loadedStats.containsKey(player.getName())) {
            return loadedStats.get(player.getName());
        }
        PlayerStats nps = new PlayerStats(player);
        $.savePlayer(nps);
        return nps;
    }
}
