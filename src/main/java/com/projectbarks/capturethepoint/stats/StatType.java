package com.projectbarks.capturethepoint.stats;


/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public enum StatType {

    Points(StatType.getDefaultPoints()), Kills(StatType.getDefaultKills()), Deaths(StatType.getDefaultDeaths()), GamesPlayed(StatType.getDefaultGamesPlayed()), GamesWon(StatType.getDefaultGamesWon()), RageQuits(StatType.getDefaultRageQuits()), Opponents(StatType.getDefaultOpponents()), RankPoints(StatType.getDefaultRankPoints()), CachedRank(0);
    private static Integer defaultPoints = 0;
    private static Integer defaultKills = 0;
    private static Integer defaultDeaths = 0;
    private static Integer defaultGamesPlayed = 0;
    private static Integer defaultGamesWon = 0;
    private static Integer defaultRageQuits = 0;
    private static Integer defaultRankPoints = 0;
    private static Integer defaultOpponents = 0;
    private Integer defaultAmount;

    private StatType(Integer defaultAmount) {
        this.defaultAmount = defaultAmount;
    }

    public String getName() {
        return this.toString();
    }

    public static Integer getDefaultPoints() {
        return defaultPoints;
    }

    public static Integer getDefaultKills() {
        return defaultKills;
    }

    public static Integer getDefaultDeaths() {
        return defaultDeaths;
    }

    public static Integer getDefaultGamesPlayed() {
        return defaultGamesPlayed;
    }

    public static Integer getDefaultGamesWon() {
        return defaultGamesWon;
    }

    public static Integer getDefaultRageQuits() {
        return defaultRageQuits;
    }

    public static Integer getDefaultOpponents() {
        return defaultOpponents;
    }

    public static Integer getDefaultRankPoints() {
        return defaultRankPoints;
    }

    public Integer getDefaultAmount() {
        return defaultAmount;
    }
}
