package com.projectbarks.capturethepoint.stats;

import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class PlayerStats implements Comparable<PlayerStats> {

    @Getter
    private Map<StatType, Integer> stats;
    private final String player;

    public PlayerStats(OfflinePlayer player) {
        this(player.getName());
    }

    public PlayerStats(String player) {
        this.stats = new HashMap<>();
        this.player = player;
        for (StatType statType : StatType.values()) {
            this.stats.put(statType, 0);
        }
    }

    public Player getPlayer() {
        return Bukkit.getPlayer(player);
    }

    public OfflinePlayer getOfflinePlayer() {
        return Bukkit.getOfflinePlayer(player);
    }

    public int getStat(StatType statType) {
        return stats.get(statType);
    }

    public void addAmount(StatType statType, int i) {
        setAmount(statType, getStat(statType) + i);
    }

    public void removeAmount(StatType statType, int i) {
        setAmount(statType, getStat(statType) - i);
    }

    public void setAmount(StatType statType, int i) {
        stats.put(statType, i);
    }

    @Override
    public int compareTo(PlayerStats o) {
        if (o instanceof PlayerStats) {
            Integer testingValue2 = o.getStat(StatType.RankPoints);
            Integer testingValue = this.getStat(StatType.RankPoints);
            if (testingValue.equals(testingValue2)) {
                return 0;
            } else if (testingValue > testingValue2) {
                return -1;
            } else if (testingValue < testingValue2) {
                return 1;
            }
        }
        throw new UnsupportedOperationException("Invaild object");
    }
}
