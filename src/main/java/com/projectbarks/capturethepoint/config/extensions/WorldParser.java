package com.projectbarks.capturethepoint.config.extensions;

import com.projectbarks.capturethepoint.CaptureThePoint;
import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.config.AbstractConfig;
import com.projectbarks.capturethepoint.maps.Beacon;
import com.projectbarks.capturethepoint.maps.CachedLocation;
import com.projectbarks.capturethepoint.maps.GMap;
import com.projectbarks.capturethepoint.maps.GMapC;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.configuration.ConfigurationSection;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class WorldParser extends AbstractConfig {

    private CaptureThePoint main;
    @Getter
    private List<GMap> maps;
    
    public WorldParser(GlobalManager gm) {
        super(gm.getCaptureThePoint(), "maps");
        this.maps = new ArrayList<>();
        this.main = gm.getCaptureThePoint();
    }

    /**
     * Loads the config.yml
     */
    @Override
    public void loadConfig() {
        CaptureThePoint.LOG.log(Level.INFO, "Loading Maps");
        try {
            Set<String> worlds = this.config.getKeys(false);
            double percent = 1;
            for (String w : worlds) {
                GMap map = parseMaps(w);
                if (map != null) {
                    maps.add(map);
                    map.unloadChunks();
                    Bukkit.getLogger().log(Level.INFO, "{0} has Loaded. Maps {1}% done loading", new Object[]{map.getName(), (percent / worlds.size()) * 100});
                }
                percent++;
            }
            CaptureThePoint.LOG.log(Level.INFO, "Loaded All Maps");
        } catch (Exception e) {
            CaptureThePoint.LOG.log(Level.SEVERE, "Loading maps has thrown an error! Expect problems");
            e.printStackTrace();
        }
    }

    private GMap parseMaps(String MAPNAME) {
        World world = loadWorld(MAPNAME);
        if (world == null) {
            return null;
        }
        List<Beacon> beacons = new ArrayList<>();
        List<Location> spawnpoints = new ArrayList<>();

        String name = this.config.getString(MAPNAME + ".name", "Error");
        for (String key : this.config.getConfigurationSection(MAPNAME + ".beacons").getKeys(false)) {
            Location loc = parseLoc(this.config.getConfigurationSection(MAPNAME + ".beacons." + key));
            beacons.add(GMapC.buildBeacon(loc));
        }
        for (String key : this.config.getConfigurationSection(MAPNAME + ".spawnpoints").getKeys(false)) {
            Location loc = parseLoc(this.config.getConfigurationSection(MAPNAME + ".spawnpoints." + key));
            spawnpoints.add(loc);
        }
        Beacon[] beaconsA = beacons.toArray(new Beacon[beacons.size()]);
        Location[] spawnPointsA = spawnpoints.toArray(new Location[spawnpoints.size()]);
        return new GMapC(world, name, beaconsA, CachedLocation.fromArray(spawnPointsA));
    }

    public World loadWorld(String name) {
        File mapFile = new File(main.getServer().getWorldContainer().getAbsolutePath() + name.replaceAll(".", ""));
        if (mapFile.exists()) {
            World world = Bukkit.getServer().createWorld(new WorldCreator(name));
            return world;
        } else {
            Logger.getLogger(WorldParser.class.getName()).log(Level.SEVERE, "{0}{1}", new Object[]{main.getServer().getWorldContainer().getAbsolutePath(), name});
            return null;
        }
    }

    @Override
    public void saveConfig() {
        try {
            for (GMap map : this.maps) {
                String mapName = map.getWorld().getName();
                this.config.set(mapName + ".name", map.getName());
                saveBeacons(mapName + ".beacons", map.getBeacons());
                saveLocs(mapName + ".spawnpoints", CachedLocation.toList(map.getSpawnPoints()));
            }
        } catch (Exception ex) {
            Logger.getLogger(WorldParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Location parseLoc(ConfigurationSection cs) {
        if (cs == null) {
            return null;
        }

        String worldName = cs.getString("world");
        if (worldName == null || worldName.length() == 0) {
            return null;
        }

        final World world = main.getServer().getWorld(worldName);
        if (world == null) {
            return null;
        }

        final double x = cs.getDouble("x");
        final double y = cs.getDouble("y");
        final double z = cs.getDouble("z");
        return new Location(world, x, y, z);
    }

    private void saveLocs(String dest, List<Location> locs) {
        for (int i = 0; i < locs.size(); i++) {
            saveLoc(dest + "." + i, locs.get(i));
        }
    }

    private void saveBeacons(String dest, List<Beacon> beacons) {
        for (int i = 0; i < beacons.size(); i++) {
            Location loc = beacons.get(i).getLocation();
            loc.add(0, -1, 0);
            saveLoc(dest + "." + i, loc);
        }
    }

    private void saveLoc(String dest, Location loc) {
        this.config.set(dest + ".world", loc.getWorld().getName());
        this.config.set(dest + ".x", loc.getX());
        this.config.set(dest + ".y", loc.getY());
        this.config.set(dest + ".z", loc.getZ());
    }
    
    public List<GMap> getMaps() {
        return maps;
    }
}
