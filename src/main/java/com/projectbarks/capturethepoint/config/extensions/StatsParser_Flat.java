package com.projectbarks.capturethepoint.config.extensions;

import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.config.AbstractConfig;
import com.projectbarks.capturethepoint.stats.PlayerStats;
import com.projectbarks.capturethepoint.stats.StatType;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.OfflinePlayer;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
@Deprecated
public class StatsParser_Flat extends AbstractConfig {

    private static final Logger LOG = Logger.getLogger(StatsParser_Flat.class.getName());
    private List<PlayerStats> statistics;

    public StatsParser_Flat(GlobalManager gm) {
        super(gm.getCaptureThePoint(), "statistics.yml");
        this.statistics = new ArrayList<>();
    }

    @Override
    public void loadConfig() {
        try {
            fileCheck();
            this.getConfig().load(this.getFile());
            for (String key : this.getConfig().getKeys(false)) {
                if (key != null) {
                    PlayerStats stats = parsePlayerStat(key);
                    if (stats != null) {
                        this.statistics.add(stats);
                    }
                }
            }
            save();
        } catch (Exception e) {
            LOG.log(Level.WARNING, "Config failed to load!");
        }
    }

    @Override
    public void saveConfig() {
        try {
            for (PlayerStats stat : this.statistics) {
                this.getConfig().set(stat.getOfflinePlayer().getName(), buildStatSave(stat));
            }
        } catch (Exception iOException) {
        }
    }

    public void updateStats() {
        this.save();
        this.statistics.clear();
        this.load();
    }

    public PlayerStats getStats(OfflinePlayer player) {
        for (PlayerStats stats : statistics) {
            if (stats.getOfflinePlayer().getName().equalsIgnoreCase(player.getName())) {
                return stats;
            }
        }
        statistics.add(new PlayerStats(player));
        return getStats(player);
    }

    private PlayerStats parsePlayerStat(String string) {
        PlayerStats playerStats = new PlayerStats(string);
        if (string != null) {
            String[] stats = this.getConfig().getString(string).split("\\|");
            if (stats.length > 0) {
                playerStats.setAmount(StatType.Kills, parseIntAntiNull(stats[0]));
            }
            if (stats.length > 1) {
                playerStats.setAmount(StatType.Deaths, parseIntAntiNull(stats[1]));
            }
            if (stats.length > 2) {
                playerStats.setAmount(StatType.GamesPlayed, parseIntAntiNull(stats[2]));
            }
            if (stats.length > 3) {
                playerStats.setAmount(StatType.GamesWon, parseIntAntiNull(stats[3]));
            }
            if (stats.length > 4) {
                playerStats.setAmount(StatType.Points, parseIntAntiNull(stats[4]));
            }

            return playerStats;
        } else {
            return null;
        }

    }

    private Integer parseIntAntiNull(String string) {
        Integer number;
        try {
            number = Integer.parseInt(string);
        } catch (NumberFormatException e) {
            number = 0;
        }
        return number;
    }

    private String buildStatSave(PlayerStats stat) {
        String value = "";
        value += stat.getStat(StatType.Kills);
        value += "|" + stat.getStat(StatType.Deaths);
        value += "|" + stat.getStat(StatType.GamesPlayed);
        value += "|" + stat.getStat(StatType.GamesWon);
        value += "|" + stat.getStat(StatType.Points);
        return value;
    }
}
