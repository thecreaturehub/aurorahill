package com.projectbarks.capturethepoint.config.extensions;

import com.hcherndon.jsqlapi.JCaller;
import com.hcherndon.jsqlapi.JSQL;
import com.hcherndon.jsqlapi.MySQLInfo;
import com.projectbarks.capturethepoint.CaptureThePoint;
import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.GlobalManagerC;
import com.projectbarks.capturethepoint.config.Config;
import com.projectbarks.capturethepoint.stats.PlayerStats;
import com.projectbarks.capturethepoint.stats.StatType;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.logging.Logger;
import lombok.Getter;
import org.json.JSONException;
import org.json.JSONObject;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class JSQLBackend {

    CaptureThePoint plugin;
    private JSQL jsql;
    private Logger $;
    private String t;
    @Getter
    private boolean isEnabled = false;

    public JSQLBackend(GlobalManager gm) {
        this.plugin = gm.getCaptureThePoint();
        t = "stats";
        $ = Logger.getLogger(getClass().getSimpleName());
        jsql = JSQL.Creator.createInstance();
    }

    public void enable() {
        Config config = GlobalManagerC.getGlobalManager().getConfig();
        MySQLInfo info = new MySQLInfo(config.getMysqlHost(), config.getMysqlPort(), config.getMysqlUsername(), config.getMysqlPassword(), config.getMysqlDatabase());
        try (Connection connection = DriverManager.getConnection(info.toNoDatabaseURL(), info.getUsername(), info.getPassword())) {
            isEnabled = true;
        } catch (Exception e) {
            CaptureThePoint.LOG.warning("Could not connect to a database, JSQL is not enabled! Stats will not log!");
            isEnabled = false;
            return;
        }
        jsql.start(info);
        jsql.createDatabase();
        jsql.createTable(t, new JCaller() {
            @Override
            public void onTaskCompletion(String key, Object object) {
                $.info(String.format("Table %s has been created if not already created!", key));
            }
        });
    }

    public void savePlayer(PlayerStats playerStats) {
        if (!isEnabled) {
            return;
        }
        JSONObject object = new JSONObject();
        try {
            object.put("player", playerStats);
            for (StatType stat : StatType.values()) {
                object.put(stat.getName(), playerStats.getStat(stat));
            }
            jsql.setObject(t, playerStats.getOfflinePlayer().getName(), object, new JCaller<JSONObject>() {
                @Override
                public void onTaskCompletion(String key, JSONObject object) {
                    $.info(String.format("Saved " + key + " to the database!"));
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getStats(final String playerName, final JCaller<PlayerStats> whatToDoWhenTheStatsAreRetrieved) {
        if (!isEnabled) {
            return;
        }
        jsql.getObject(t, playerName, new JCaller<JSONObject>() {
            @Override
            public void onTaskCompletion(String key, final JSONObject object) {
                if (object == null) {
                    plugin.getServer().getScheduler().runTask(plugin, new Runnable() {
                        @Override
                        public void run() {
                            whatToDoWhenTheStatsAreRetrieved.onTaskCompletion(playerName, null);
                        }
                    });
                    return;
                }
                plugin.getServer().getScheduler().runTask(plugin, new Runnable() {
                    @Override
                    public void run() {
                        whatToDoWhenTheStatsAreRetrieved.onTaskCompletion(playerName, parseStats(object));
                    }
                });
            }
        });
    }

    public void getAllStats(final JCaller<PlayerStats[]> whatToDoWhenTheStatsAreRetrieved) {
        if (!isEnabled) {
            return;
        }
        jsql.getAllObjects(t, new JCaller<JSONObject[]>() {
            @Override
            public void onTaskCompletion(String key, JSONObject[] object) {
                final PlayerStats[] ret = new PlayerStats[object.length];
                int index = 0;
                for (JSONObject jsonObject : object) {
                    ret[index++] = parseStats(jsonObject);
                }
                plugin.getServer().getScheduler().runTask(plugin, new Runnable() {
                    @Override
                    public void run() {
                        whatToDoWhenTheStatsAreRetrieved.onTaskCompletion("statsTable", ret);
                    }
                });
            }
        });
    }

    private PlayerStats parseStats(JSONObject object) {
        try {
            PlayerStats playerStats = new PlayerStats(object.getString("player"));
            for (StatType stat : StatType.values()) {
                playerStats.setAmount(stat, object.getInt(stat.getName()));
            }
            return playerStats;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void shutdown() {
        if (!isEnabled) {
            return;
        }
        jsql.stop();
    }
}
