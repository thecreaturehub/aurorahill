package com.projectbarks.capturethepoint.config;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public abstract class AbstractConfig {
    
    //Fields//
    //-----Fields Final-----//
    protected final YamlConfiguration config;
    //-----Fields Default-----//
    protected File file;

    /**
     *
     * @param plugin
     * @param filename
     */
    public AbstractConfig(Plugin plugin, String filename) {
        this(new File(plugin.getDataFolder(), filename + ".yml"));
    }

    /**
     *
     * @param file
     */
    public AbstractConfig(File file) {
        this.config = new YamlConfiguration();
        this.file = file;
    }

    public void load() {
        try {
            fileCheck();
            this.config.load(file);
            this.loadConfig();
        } catch (Exception ex) {
            Logger.getLogger(AbstractConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void save() {
        this.saveConfig();
        try {
            this.getConfig().save(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     */
    abstract protected void loadConfig();

    /**
     *
     */
    abstract protected void saveConfig();

    //-----Protected Mutators-----//
    /**
     * Internal method, creates the file if it doesn't exist
     *
     * @throws Exception
     */
    protected void fileCheck() throws Exception {
        if (!file.exists()) {
            file.getParentFile().mkdirs();
            file.createNewFile();
        }
    }

    //-----Protected Final Mutators-----//
    /**
     * Returns the YALM configuration.
     *
     * @return the config
     */
    protected YamlConfiguration getConfig() {
        return config;
    }

    /**
     * Gets the file designated to the config. This can be defaulted to null if
     * not created properly.
     *
     * @return the file
     */
    protected File getFile() {
        return file;
    }

    /**
     * Sets the file designated to the config.
     *
     * @param file the file to set
     */
    protected void setFile(File file) {
        this.file = file;
    }
}
