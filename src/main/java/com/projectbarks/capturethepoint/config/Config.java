package com.projectbarks.capturethepoint.config;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;
import com.projectbarks.capturethepoint.CaptureThePoint;
import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.api.timed.EAction;
import com.projectbarks.capturethepoint.api.timed.TimedActionC;
import com.projectbarks.capturethepoint.kits.backend.DatabaseMode;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class Config extends AbstractConfig {

    //-----Fields-----//
    private List<TimedActionC> schedule;
    private Integer winnerUnicode;
    private Material kitSelector;
    private Location lobby;
    private Double percentOfRPDisperse;
    private boolean quickPotions, removePotionBottle, modifyChat, deathMessages;
    private String kitSelectorName;
    private String chatFormat, mysqlHost, mysqlUsername, mysqlPassword, mysqlDatabase, kitDatabaseMode;
    private Integer minPlayers, maxVoteWorlds, winner, looser, mysqlPort, minBeaconChangeInterval, maxBeaconChangeInterval;
    
    //Constructors//
    //-----Constructors Public-----//
    public Config(GlobalManager gm) {
        super(gm.getCaptureThePoint(), "config");
        this.maxVoteWorlds = 5;
        this.kitSelector = Material.STICK;
        this.kitSelectorName = "Kit Selector";
        this.schedule = new ArrayList<>();
        this.minPlayers = 10;
        this.winner = 10;
        this.looser = 1;
        this.quickPotions = true;
        this.removePotionBottle = true;
        this.chatFormat = "&8[&6(money)&8]&7(name)&8:&r (message)";
        this.modifyChat = true;
        this.mysqlHost = "http://example.net/example";
        this.mysqlUsername = "username";
        this.mysqlPassword = "password";
        this.mysqlDatabase = "default";
        this.winnerUnicode = 10022;
        this.minBeaconChangeInterval = 20;
        this.maxBeaconChangeInterval = 60;
        this.kitDatabaseMode = DatabaseMode.Sqlite.getName();
        this.mysqlPort = 3_306;
        this.percentOfRPDisperse = .05;
        this.deathMessages = false;
    }

    //Functions//
    //-----Public Mutators-----//
    @Override
    public void loadConfig() {
        CaptureThePoint.LOG.log(Level.INFO, "Loading Config...");
        try {
            this.minPlayers = config.getInt("Minimum Players", getMinPlayers());
            this.maxVoteWorlds = config.getInt("Max Worlds for Vote", getMaxVoteWorlds());
            this.kitSelector = Material.matchMaterial(config.getString("Kit.Item", kitSelector.name()));
            this.kitSelectorName = config.getString("Kit.Display Name", kitSelectorName);
            this.schedule.addAll(parseSchedule(config.getConfigurationSection("schedule")));
            this.setLobby(parseLoc(config.getConfigurationSection("Lobby")));
            this.winner = config.getInt("Winner Tokens", getWinner());
            this.looser = config.getInt("Looser Tokens", getLooser());
            this.removePotionBottle = config.getBoolean("Enhancements.potions.take", this.removePotionBottle);
            this.quickPotions = config.getBoolean("Enhancements.potions.enable", this.quickPotions);
            this.chatFormat = config.getString("Chat.Format", this.chatFormat);
            this.modifyChat = config.getBoolean("Chat.Enabled", this.modifyChat);
            this.mysqlHost = config.getString("MySQL.mysqlHost", this.mysqlHost);
            this.mysqlPort = config.getInt("MySQL.mysqlPort", this.mysqlPort);
            this.mysqlUsername = config.getString("MySQL.Username", this.mysqlUsername);
            this.mysqlPassword = config.getString("MySQL.Password", this.mysqlPassword);
            this.mysqlDatabase = config.getString("MySQL.mysqlDatabase", this.mysqlDatabase);
            this.percentOfRPDisperse = config.getDouble("Percent of Rank Points Desperse", this.percentOfRPDisperse);
            this.kitDatabaseMode = config.getString("Kit Database Mode", this.kitDatabaseMode);
            this.winnerUnicode = config.getInt("Winner Unicode Symbol", this.winnerUnicode);
            this.deathMessages = config.getBoolean("Enable Death Messages", this.deathMessages);
            this.maxBeaconChangeInterval = config.getInt("Beacon Change Interval.Max", this.maxBeaconChangeInterval);
            this.minBeaconChangeInterval = config.getInt("Beacon Change Interval.Min", this.minBeaconChangeInterval);
            save();
            CaptureThePoint.LOG.log(Level.INFO, "Loaded Config!");
        } catch (Exception e) {
            CaptureThePoint.LOG.log(Level.SEVERE, "Config loading failed!");
            e.printStackTrace();
        }
    }

    @Override
    public void saveConfig() {
        this.getConfig().set("Minimum Players", getMinPlayers());
        this.getConfig().set("Max Worlds for Vote", getMaxVoteWorlds());
        this.getConfig().set("Winner Tokens", getWinner());
        this.getConfig().set("Looser Tokens", getLooser());
        this.getConfig().set("Winner Unicode Symbol", this.winnerUnicode);
        this.getConfig().set("Percent of Rank Points disperse", this.percentOfRPDisperse);
        this.getConfig().set("Kit Database Mode", this.kitDatabaseMode);
        this.getConfig().set("MySQL.mysqlHost", this.mysqlHost);
        this.getConfig().set("MySQL.mysqlPort", this.mysqlPort);
        this.getConfig().set("MySQL.mysqlDatabase", this.mysqlDatabase);
        this.getConfig().set("MySQL.Username", this.mysqlUsername);
        this.getConfig().set("MySQL.Password", this.mysqlPassword);
        this.getConfig().set("Chat.Enabled", this.modifyChat);
        this.getConfig().set("Chat.Format", this.chatFormat);
        this.getConfig().set("Kit.Item", this.kitSelector.name());
        this.getConfig().set("Kit.Display Name", this.kitSelectorName);
        this.getConfig().set("Enhancements.potions.enable", this.quickPotions);
        this.getConfig().set("Enhancements.potions.take", this.removePotionBottle);
        this.getConfig().set("Enable Death Messages", this.deathMessages);
        saveLoc("Lobby", lobby);
        this.getConfig().set("Beacon Change Interval.Max", this.maxBeaconChangeInterval);
        this.getConfig().set("Beacon Change Interval.Min", this.minBeaconChangeInterval);
        saveSchedule(Lists.reverse(getSchedule()));
    }

    public void saveLoc(String dest, Location loc) {
        Location newLoc;
        if (loc == null) {
            newLoc = Bukkit.getServer().getWorlds().get(0).getSpawnLocation();
        } else {
            newLoc = loc;
        }
        this.getConfig().set(dest + ".world", newLoc.getWorld().getName());
        this.getConfig().set(dest + ".x", newLoc.getX());
        this.getConfig().set(dest + ".y", newLoc.getY());
        this.getConfig().set(dest + ".z", newLoc.getZ());
    }

    //-----Public Acessors------//
    public Location parseLoc(ConfigurationSection cs) {
        if (cs == null) {
            return lobby;
        }
        String worldName = cs.getString("world");
        if (worldName == null || worldName.length() == 0) {
            return lobby;
        }

        World world = Bukkit.getServer().getWorld(worldName);
        if (world == null) {
            return lobby;
        }
        final double x = cs.getDouble("x");
        final double y = cs.getDouble("y");
        final double z = cs.getDouble("z");
        return new Location(world, x, y, z);
    }

    public List<TimedActionC> getSchedule() {
        return Collections.unmodifiableList(schedule);
    }

    public ItemStack getKitSelector() {
        ItemStack item = new ItemStack(kitSelector, 1);
        if (item.getType() != Material.AIR) {
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(kitSelectorName);
            item.setItemMeta(itemMeta);
        }
        return item;
    }

    //-----Private Acessors-----//
    private List<TimedActionC> parseSchedule(ConfigurationSection cs) {
        List<TimedActionC> actions = new ArrayList<>();
        if (cs == null) {
            return getDefault();
        }
        for (String i : cs.getKeys(false)) {
            int n;
            try {
                n = Integer.parseInt(i);
            } catch (NumberFormatException e) {
                Logger.getLogger(Config.class.getName()).log(Level.WARNING, "Failed to parse \"{0}\". Is it a number?", i);
                continue;
            }
            for (String sEAction : this.getConfig().getString("schedule." + i).split(",")) {
                EAction eAction = EAction.valueOf(sEAction.replaceAll(",", "").replaceAll(" ", "").trim());
                if (eAction == EAction.BEACON_CHANGE) {
                    continue;
                }
                actions.add(new TimedActionC(n, eAction));
            }
        }
        return actions;
    }

    //-----Private Mutators-----//
    private void saveSchedule(List<TimedActionC> ta) {
        SortedMap<Integer, String> actions = new TreeMap<>();
        for (TimedActionC action : ta) {
            if (actions.containsKey(action.getTime())) {
                String newAction = actions.get(action.getTime()) + ", " + action.getType().getAction();
                actions.put(action.getTime(), newAction);
            } else {
                actions.put(action.getTime(), action.getType().getAction());
            }
        }
        for (Map.Entry<Integer, String> set : actions.entrySet()) {
            this.getConfig().set("schedule." + set.getKey(), set.getValue());
        }
    }

    //-----Private Acessors-----//
    private List<TimedActionC> getDefault() {
        List<TimedActionC> actions = new ArrayList<>();
        actions.add(new TimedActionC(60, EAction.LOBBY_MSG));//actions.add(new LobbyMsg(60));
        actions.add(new TimedActionC(120, EAction.LOBBY_MSG));//actions.add(new LobbyMsg(120));
        actions.add(new TimedActionC(180, EAction.LOBBY_MSG));//actions.add(new LobbyMsg(180));
        actions.add(new TimedActionC(210, EAction.LOBBY_MSG));//actions.add(new LobbyMsg(210));
        actions.add(new TimedActionC(220, EAction.LOBBY_MSG));//actions.add(new LobbyMsg(220));
        actions.add(new TimedActionC(230, EAction.LOBBY_MSG_TIME));//actions.add(new LobbyMsg(230));
        actions.add(new TimedActionC(235, EAction.LOBBY_MSG_TIME));//actions.add(new LobbyMsg(235));
        actions.add(new TimedActionC(236, EAction.LOBBY_MSG_TIME));//actions.add(new LobbyMsg(236));
        actions.add(new TimedActionC(237, EAction.LOBBY_MSG_TIME));//actions.add(new LobbyMsg(237));
        actions.add(new TimedActionC(238, EAction.LOBBY_MSG_TIME));//actions.add(new LobbyMsg(238));
        actions.add(new TimedActionC(239, EAction.LOBBY_MSG_TIME));//actions.add(new LobbyMsg(239));
        actions.add(new TimedActionC(240, EAction.START_GAME));//actions.add(new StartGame(240));
        actions.add(new TimedActionC(300, EAction.BROADCAST_GAME_TIME));//actions.add(new GameTimeMsg(300));
       // actions.add(new TimedActionC(330, EAction.BEACON_CHANGE));//actions.add(new BeaconChange(330));
        actions.add(new TimedActionC(360, EAction.BROADCAST_GAME_TIME));//actions.add(new GameTimeMsg(360));
        //actions.add(new TimedActionC(390, EAction.BEACON_CHANGE));//NO WAITactions.add(new BeaconChange(390));
        actions.add(new TimedActionC(420, EAction.BROADCAST_GAME_TIME));//actions.add(new GameTimeMsg(420));
        actions.add(new TimedActionC(450, EAction.BROADCAST_GAME_TIME));//actions.add(new GameTimeMsg(450));
        actions.add(new TimedActionC(460, EAction.BROADCAST_GAME_TIME));//actions.add(new GameTimeMsg(460));
        actions.add(new TimedActionC(470, EAction.BROADCAST_GAME_TIME));//actions.add(new GameTimeMsg(470));
        actions.add(new TimedActionC(475, EAction.BROADCAST_GAME_TIME));//actions.add(new GameTimeMsg(475));
        actions.add(new TimedActionC(476, EAction.BROADCAST_GAME_TIME));//actions.add(new GameTimeMsg(476));
        actions.add(new TimedActionC(477, EAction.BROADCAST_GAME_TIME));//actions.add(new GameTimeMsg(477));
        actions.add(new TimedActionC(478, EAction.BROADCAST_GAME_TIME));//actions.add(new GameTimeMsg(478));
        actions.add(new TimedActionC(479, EAction.BROADCAST_GAME_TIME));//actions.add(new GameTimeMsg(479));
        actions.add(new TimedActionC(480, EAction.END_GAME));//actions.add(new EndGame(480));
        actions.add(new TimedActionC(490, EAction.RESTART_GAME));
        return actions;
    }

    public String getWinnerUnicode() {
        return fromUnicode(this.winnerUnicode) + "";
    }

    public void setLobby(Location lobby) {
        this.lobby = ((lobby != null) ? lobby : Bukkit.getServer().getWorlds().get(0).getSpawnLocation());
    }
    
    private char fromUnicode(Integer codePoint) {
        return Character.toChars(codePoint)[0];
    }
    
    public Location getLobby() {
        return lobby;
    }
    
    public Double getPercentOfRPDisperse() {
        return percentOfRPDisperse;
    }
    
    public boolean isQuickPotions() {
        return quickPotions;
    }
    
    public boolean isRemovePotionBottle() {
        return removePotionBottle;
    }
    
    public boolean isModifyChat() {
        return modifyChat;
    }
    
    public boolean isDeathMessages() {
        return deathMessages;
    }
    
    public String getChatFormat() {
        return chatFormat;
    }
    
    public String getMysqlHost() {
        return mysqlHost;
    }
    
    public String getMysqlUsername() {
        return mysqlUsername;
    }
    
    public String getMysqlPassword() {
        return mysqlPassword;
    }
    
    public String getMysqlDatabase() {
        return mysqlDatabase;
    }
    
    public String getKitDatabaseMode() {
        return kitDatabaseMode;
    }
    
    public Integer getMinPlayers() {
        return minPlayers;
    }
    
    public Integer getMaxVoteWorlds() {
        return maxVoteWorlds;
    }
    
    public Integer getWinner() {
        return winner;
    }
    
    public Integer getLooser() {
        return looser;
    }
    
    public Integer getMysqlPort() {
        return mysqlPort;
    }
    
    public Integer getMinBeaconChangeInterval() {
        return minBeaconChangeInterval;
    }
    
    public Integer getMaxBeaconChangeInterval() {
        return maxBeaconChangeInterval;
    }
}
