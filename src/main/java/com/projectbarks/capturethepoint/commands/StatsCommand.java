package com.projectbarks.capturethepoint.commands;

import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.messages.MessageManager;
import com.projectbarks.capturethepoint.messages.Msg;
import com.projectbarks.capturethepoint.stats.PlayerStats;
import com.projectbarks.capturethepoint.stats.StatType;
import java.util.Locale;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class StatsCommand implements CommandExecutor {

    private final GlobalManager globalManager;
    private final MessageManager mm;

    public StatsCommand(GlobalManager globalManager) {
        this.globalManager = globalManager;
        this.mm = globalManager.getMsgManager();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(cmd.getName().equalsIgnoreCase("statistics") || cmd.getName().equalsIgnoreCase("stats") || cmd.getName().equalsIgnoreCase("s"))) {
            return false;
        }

        OfflinePlayer pStats;
        if (args.length > 0) {
            pStats = matchPlayer(args[0]);
        } else {
            pStats = ((Player) sender);
        }

        if (pStats != null) {
            this.msgPlayerStats(sender, pStats);
        } else {
            mm.msg(sender, Msg.STATS_MISSING_PLAYER);
        }
        return true;
    }

    private OfflinePlayer matchPlayer(String player) {
        for (OfflinePlayer offlinePlayer : Bukkit.getOfflinePlayers()) {
            if (offlinePlayer.getName().equalsIgnoreCase(player)) {
                return offlinePlayer;
            }
        }

        for (OfflinePlayer offlinePlayer : Bukkit.getOfflinePlayers()) {
            if (offlinePlayer.getName().toLowerCase(Locale.getDefault()).startsWith(
                    player.toLowerCase(Locale.getDefault()))) {
                return offlinePlayer;
            }
        }

        return null;
    }

    private void msgPlayerStats(CommandSender to, OfflinePlayer about) {
        PlayerStats stats = this.globalManager.getStatsManager().getStats(about);
        mm.msg(to, Msg.STATS, about.getName());
        mm.msg(to, Msg.STATS_RP, stats.getStat(StatType.CachedRank), stats.getStat(StatType.RankPoints));
        mm.msg(to, Msg.STATS_GAMES, stats.getStat(StatType.GamesPlayed), stats.getStat(StatType.GamesWon));
        mm.msg(to, Msg.STATS_OPPONENTS, stats.getStat(StatType.Opponents));
        mm.msg(to, Msg.STATS_POINTS, stats.getStat(StatType.Points));
        double balance = globalManager.getKitManager().getEconomy().getBalance(about.getName());
        mm.msg(to, Msg.STATS_TOKENS, balance, globalManager.getKitManager().getCurrencyName(balance));
        mm.msg(to, Msg.STATS_KILLS_DEATH, stats.getStat(StatType.Kills), stats.getStat(StatType.Deaths));
        mm.msg(to, Msg.STATS_RAGE_QUITS, stats.getStat(StatType.RageQuits));
    }
}
