package com.projectbarks.capturethepoint.commands;

import com.projectbarks.capturethepoint.CaptureThePoint;
import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.api.timed.EAction;
import com.projectbarks.capturethepoint.api.timed.TimedAction;
import com.projectbarks.capturethepoint.messages.MessageManager;
import com.projectbarks.capturethepoint.messages.Msg;
import com.projectbarks.capturethepoint.misc.GameUtils;
import java.util.Locale;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class TimerCommands implements CommandExecutor {

    private GlobalManager gm;
    private MessageManager mm;

    public TimerCommands(GlobalManager gm) {
        this.gm = gm;
        this.mm = gm.getMsgManager();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(cmd.getName().equalsIgnoreCase("timer") || cmd.getName().equalsIgnoreCase("game"))) {
            return false;
        }
        
        if (!sender.hasPermission("capturethepoint.settime")) {
            mm.msg(sender, Msg.PERMISSION);
        }

        if (args.length <= 0) {
            mm.msg(sender, Msg.USAGE, "/" + cmd.getName() + " [pause|resume|start|end]");
            return true;
        }

        String cName = args[0].toLowerCase(Locale.getDefault());
        switch (cName) {
            case "pause":
                CaptureThePoint.getTimerClass().pause(true);
                mm.msg(sender, Msg.TIMER_COMMANDS_PAUSED);
                break;
            case "resume":
                mm.msg(sender, Msg.TIMER_COMMANDS_RESUMED);
                CaptureThePoint.getTimerClass().pause(false);
                break;
            case "start": {
                TimedAction firstAction = GameUtils.findFirstAction(EAction.START_GAME,
                                                                CaptureThePoint.getTimerClass().getSuperActions());
                CaptureThePoint.getTimerClass().setTime(firstAction.getTime() - 5);
                mm.broadcast(Msg.TIMER_COMMANDS_STARTED);
                break;
            }
            case "end": {
                TimedAction firstAction = GameUtils.findFirstAction(EAction.END_GAME,
                                                                CaptureThePoint.getTimerClass().getSuperActions());
                CaptureThePoint.getTimerClass().setTime(firstAction.getTime() - 5);
                mm.broadcast(Msg.TIMER_COMMANDS_ENDED);
                break;
            }
            default:
                mm.msg(sender, Msg.USAGE, "/" + cmd.getName() + " [pause|resume|start|end]");
                break;
        }

        return true;
    }
}
