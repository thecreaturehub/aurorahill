package com.projectbarks.capturethepoint.commands;

import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.messages.MessageManager;
import com.projectbarks.capturethepoint.messages.Msg;
import java.util.Locale;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class SingleCommands implements CommandExecutor {

    private final GlobalManager gm;
    private final MessageManager mm;

    public SingleCommands(GlobalManager gm) {
        this.gm = gm;
        this.mm = gm.getMsgManager();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        String cName = cmd.getName().toLowerCase(Locale.getDefault());
        if (sender instanceof Player) {
            Player player = ((Player) sender);
            switch (cName) {
                case "setlobby":
                    if (player.hasPermission("capturethepoint.sc.setlobby")) {
                        gm.getConfig().setLobby(player.getLocation());
                        gm.getConfig().save();
                        mm.msg(sender, Msg.LOBBY_SET);
                    } else {
                        mm.msg(sender, Msg.PERMISSION);
                    }
                    break;
                case "lobby":
                    if (!(gm.getTimer().inGame())) {
                        player.teleport(gm.getConfig().getLobby());
                        mm.msg(sender, Msg.TELEPORTING);
                    } else {
                        mm.msg(sender, Msg.TELEPORTING_IN_GAME);
                    }
                    break;
            }

            return true;
        } else {
            mm.msg(sender, Msg.IN_GAME_COMMAND);
        }
        return false;
    }
}
