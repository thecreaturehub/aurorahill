package com.projectbarks.capturethepoint.commands;

import com.projectbarks.capturethepoint.api.CarbonVotable;
import com.projectbarks.capturethepoint.maps.GMap;
import java.util.List;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public interface VoteManager {

    /**
     * Adds new, unload chunks, and resets votes for a cluster of maps.
     */
    public void reloadVoteMaps();

    /**
     * Returns one map out of all the maps for vote. This may return a map
     * chosen between two at random if it faces maps with the same amount of
     * votes.
     *
     * @return
     */
    public CarbonVotable<GMap, String> getHighestMap();

    /**
     * Get all the Maps current being up for vote.
     *
     * @return
     */
    public List<CarbonVotable<GMap, String>> getVotes();
}
