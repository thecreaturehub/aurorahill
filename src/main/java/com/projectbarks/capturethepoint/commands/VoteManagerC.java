package com.projectbarks.capturethepoint.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.TreeSet;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.google.common.collect.Sets;
import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.api.CarbonVotable;
import com.projectbarks.capturethepoint.config.extensions.WorldParser;
import com.projectbarks.capturethepoint.maps.GMap;
import com.projectbarks.capturethepoint.maps.GMapC;
import com.projectbarks.capturethepoint.messages.MessageManager;
import com.projectbarks.capturethepoint.messages.Msg;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class VoteManagerC implements CommandExecutor, VoteManager {

    private GlobalManager gm;
    private WorldParser wp;
    private MessageManager mm;
    private List<CarbonVotable<GMap, String>> votes;

    public VoteManagerC(GlobalManager gm) {
        this.mm = gm.getMsgManager();
        this.votes = new ArrayList<>();
        this.wp = gm.getWorldParser();
        this.gm = gm;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        String cName = cmd.getName().toLowerCase(Locale.getDefault());
        if (!(cName.equals("v") || cName.equals("vote"))) {
            return false;
        }

        if (!(sender instanceof Player)) {
            mm.msg(sender, Msg.IN_GAME_COMMAND);
            return true;
        }
        if (!gm.getTimer().inLobby()) {
            mm.msg(sender, Msg.VOTE_IN_GAME);
            return true;
        }
        if (args.length < 1) {
            mm.msg(sender, Msg.USAGE, "/" + cName + " [id]");
            return true;
        }

        Integer n;
        try {
            n = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            mm.msg(sender, Msg.VOTE_BAD_ID);
            return true;
        }
        if (votes.size() < n) {
            mm.msg(sender, Msg.VOTE_ID_LARGE, votes.size() + 1);
        } else if (0 >= n) {
            mm.msg(sender, Msg.VOTE_ID_SMALL, 0);
        } else {
            CarbonVotable<GMap, String> votable = getVotes().get(n - 1);
            if (!hasVoted(sender.getName())) {
                votable.addToVotes(sender.getName());
                mm.msg(sender, Msg.VOTE_UPDATE, votable.getTotalVotes());
            } else {
                mm.msg(sender, Msg.VOTE_ALREADY);
            }
        }
        return true;
    }

    @Override
    public void reloadVoteMaps() {
        for (CarbonVotable<GMap, String> votable : votes) {
            votable.getVoteAble().unloadChunks();
        }

        votes.clear();
        if (wp.getMaps().size() <= 0) {
            votes.add(new CarbonVotable<GMap, String>(new GMapC(null, "No Worlds Loaded", null, null)));
        } else {
            Collections.shuffle(wp.getMaps());
            for (GMap map : wp.getMaps().subList(0, wp.getMaps().size() > gm.getConfig().getMaxVoteWorlds() ? gm.getConfig().getMaxVoteWorlds() : wp.getMaps().size())) {
                map.loadGameChunks();
                votes.add(new CarbonVotable<GMap, String>(map));
            }
        }
    }

    private boolean hasVoted(String player) {
        for (CarbonVotable<GMap, String> votable : votes) {
            if (votable.hasVoted(player)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public CarbonVotable<GMap, String> getHighestMap() {
        TreeSet<CarbonVotable<GMap, String>> treeSet = new TreeSet<>(Sets.newHashSet(votes));
        return treeSet.last();
    }

    @Override
    public List<CarbonVotable<GMap, String>> getVotes() {
        return votes;
    }
}