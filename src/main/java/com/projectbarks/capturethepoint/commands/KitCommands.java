package com.projectbarks.capturethepoint.commands;

import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.kits.backend.Kit;
import com.projectbarks.capturethepoint.kits.backend.KitAPIImpl;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class KitCommands implements CommandExecutor {

    private KitAPIImpl api;

    public KitCommands(GlobalManager gm) {
        this.api = (KitAPIImpl) gm.getKitAPI();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sendMessage(sender, "Only a player may do this!");
            return true;
        }

        if (!sender.hasPermission("capturethepoint.kitcreate")) {
            sendMessage(sender, "You do not have permission!");
            return true;
        }

        if (args.length == 0) {
            listSubCommands(sender);
        } else {
            if (args[0].equalsIgnoreCase("save")) {
                if (args.length == 1) {
                    sendMessage(sender, "&4You must specify a kit!");
                    listSubCommands(sender);
                } else if (args.length == 2) {
                    sendMessage(sender, "&4You must specify an id!");
                    listSubCommands(sender);
                } else if (args.length == 3) {
                    sendMessage(sender, "&4You must specify a cost!");
                    listSubCommands(sender);
                } else if (args.length > 3) {
                    Player p = (Player) sender;
                    int icon, cost;
                    try {
                        icon = Integer.valueOf(args[2]);
                    } catch (NumberFormatException nfe) {
                        sendMessage(sender, "&4Icon needs to be a numerical item ID.");
                        return false;
                    }
                    try {
                        cost = Integer.valueOf(args[3]);
                    } catch (NumberFormatException nfe) {
                        sendMessage(sender, "&4Cost needs to be an integer.");
                        return false;
                    }

                    if (!api.addKit(args[1], icon, cost, p.getInventory())) {
                        sendMessage(sender, "&4Kit could not be added");
                        return true;
                    }

                    sendMessage(sender, "&2The kit has been added.");
                }
            } else if (args[0].equalsIgnoreCase("delete")) {
                if (args.length >= 2) {
                    if (api.getKit(args[1]) == null) {
                        sendMessage(sender, "&4No kit by that name was found!");
                        return true;
                    }
                    api.removeKit(args[1]);
                    sendMessage(sender, "&2The kit has been removed.");
                } else {
                    sendMessage(sender, "&4You must specify a kit!");
                }
            } else if (args[0].equalsIgnoreCase("edit")) {
                if (args.length >= 2) {
                    if (api.getKit(args[1]) == null) {
                        sendMessage(sender, "&4No kit by that name was found!");
                        return true;
                    }
                    Player p = (Player) sender;
                    Kit kit = api.getKit(args[1]);
                    p.getInventory().setArmorContents(new ItemStack[]{kit.getBoots(), kit.getLeggings(), kit.getChestplate(), kit.getHelmet()});
                    ItemStack[] list = new ItemStack[36];
                    int i = 0;
                    for (ItemStack s : kit.getInventory()) {
                        list[i++] = s;
                    }
                    p.getInventory().setContents(list);
                    sendMessage(sender, String.format("&2You are editing kit &a%s&2, it has icon &a%d&2, and cost &a%d&2."
                                                      + "\nWhen you are done, do &a/kit save", kit.getName(), kit.getID(), kit.getCost()));
                } else {
                    sendMessage(sender, "&4You must specify an id!");
                }
            } else {
                listSubCommands(sender);
            }
        }
        return true;
    }

    private void listSubCommands(CommandSender sender) {
        sendMessage(sender, "/kit save {name} {iconID} {cost}- Saves your inventory into a kit\n/kit delete {name} - Deletes a kit\n/kit edit {name}");
    }

    public void sendMessage(CommandSender sender, String message) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
    }
}
