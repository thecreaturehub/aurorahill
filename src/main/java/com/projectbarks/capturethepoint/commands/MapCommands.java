package com.projectbarks.capturethepoint.commands;

import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.maps.Beacon;
import com.projectbarks.capturethepoint.maps.GMap;
import com.projectbarks.capturethepoint.maps.GMapC;
import com.projectbarks.capturethepoint.maps.CachedLocation;
import com.projectbarks.capturethepoint.messages.MessageManager;
import com.projectbarks.capturethepoint.messages.Msg;
import java.util.*;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class MapCommands implements CommandExecutor {

    private Map<String, GMap> unFinishedMaps;
    private final GlobalManager gm;
    private final MessageManager mm;

    public MapCommands(GlobalManager gm) {
        this.gm = gm;
        this.mm = gm.getMsgManager();
        this.unFinishedMaps = new HashMap<>();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("map")) {
            if (args.length >= 1) {
                String newCmd = args[0];
                String[] newArgs = Arrays.copyOfRange(args, 1, args.length);
                this.onCommandMap(sender, newCmd, label, newArgs);
            } else {
                mm.msg(sender, Msg.USAGE, "/map [create|edit|beacon|spawnpoint|publish|list|load]");
            }

            return true;
        }
        return false;
    }

    public void onCommandMap(CommandSender sender, String cmd, String label, String[] args) {
        String cName = cmd.toLowerCase(Locale.getDefault());
        if (sender instanceof Player) {
            Player player = ((Player) sender);
            String nPlayer = player.getName();
            if (!(player.hasPermission("capturethepoint.map." + cmd))) {
                mm.msg(sender, Msg.PERMISSION);
            }

            if (cName.equals("create")) {
                if (unFinishedMaps.containsKey(nPlayer)) {
                    mm.msg(sender, Msg.MAP_CURRENT);
                } else {
                    if (args.length < 1) {
                        mm.msg(sender, Msg.USAGE, "/map create [name]");
                        return;
                    }
                    String name = gm.getCaptureThePoint().buildArgs(args);
                    for (GMap map : gm.getWorldParser().getMaps()) {
                        if (map.getWorld().getName().equals(player.getWorld().getName())) {
                            mm.msg(sender, Msg.MAP_SIMILARITY, "world");
                            return;
                        } else if (map.getName().equals(name)) {
                            mm.msg(sender, Msg.MAP_SIMILARITY, "name");
                            return;
                        }
                    }

                    unFinishedMaps.put(nPlayer, new GMapC(player.getWorld(), name, null, null));
                    mm.msg(sender, Msg.MAP_OPEN);
                }
            } else if (cName.equals("edit")) {
                if (unFinishedMaps.containsKey(nPlayer)) {
                    mm.msg(sender, Msg.MAP_CURRENT);
                    return;
                }
                GMap project = findProject(player.getWorld());
                if (project == null) {
                    mm.msg(sender, Msg.MAP_WORLD_NON);
                    return;
                }
                unFinishedMaps.put(nPlayer, project);
                mm.msg(sender, Msg.MAP_OPEN);
            } else if (cName.equals("beacon")) {
                if (!unFinishedMaps.containsKey(nPlayer)) {
                    mm.msg(sender, Msg.MAP_MISSING);
                    return;
                }
                if (args.length < 2) {
                    mm.msg(sender, Msg.USAGE, "/map beacon <add|remove|list> [id]");
                }

                String cArg = args[0].toLowerCase(Locale.getDefault());
                GMap map = unFinishedMaps.get(nPlayer);
                switch (cArg) {
                    case "add":
                        map.addBeacon(GMapC.buildBeacon(player.getLocation()));
                        mm.msg(sender, Msg.MAP_MODIFY_BEACON, map.getBeacons().size());
                        break;
                    case "list":
                        mm.msg(sender, Msg.MAP_MODIFY_BEACON, map.getBeacons().size());
                        Integer count = 0;
                        for (Beacon beacon : map.getBeacons()) {
                            Location loc = beacon.getLocation();
                            player.sendMessage(mm.getName() + " - [" + count + "] x:" + loc.getBlockX() + " y: " + loc.getBlockY() + " z: " + loc.getBlockZ());
                            count++;
                        }
                        break;
                    case "remove":
                        if (args.length < 2) {
                            mm.msg(sender, Msg.USAGE, "/map beacon remove [id]");
                            return;
                        }
                        List<Beacon> locs = new ArrayList<>();
                        int id;
                        try {
                            id = Integer.parseInt(args[1]);
                        } catch (Exception e) {
                            id = -1;
                        }
                        if (!(id < 0 || id >= map.getSpawnPoints().size())) {
                            mm.msg(sender, Msg.MAP_INVAILID_ID);
                            return;
                        }
                        if (map.getBeacons() != null) {
                            locs.addAll(map.getBeacons());
                        }
                        locs.remove(id);
                        map.setBeacons(locs.toArray(new Beacon[locs.size()]));
                        mm.msg(sender, Msg.MAP_MODIFY_BEACON, locs.size());
                        break;
                    default:
                        mm.msg(sender, Msg.USAGE, "/map beacon <add|remove|list> [id]");
                        break;
                }

            } else if (cName.equals("spawnpoint")) {
                if (!unFinishedMaps.containsKey(nPlayer)) {
                    mm.msg(sender, Msg.MAP_MISSING);
                    return;
                }
                if (args.length < 1) {
                    mm.msg(sender, Msg.USAGE, "/map spawnpoint <add|remove|list> [id]");
                    return;
                }
                String cArg = args[0].toLowerCase(Locale.getDefault());
                GMap map = unFinishedMaps.get(nPlayer);
                switch (cArg) {
                    case "add":
                        map.addSpawnpoint(player.getLocation());
                        mm.msg(sender, Msg.MAP_MODIFY_SPAWNPOINT, map.getSpawnPoints().size());
                        break;
                    case "list":
                        mm.msg(sender, Msg.MAP_MODIFY_SPAWNPOINT, map.getSpawnPoints().size());
                        Integer count = 0;
                        for (CachedLocation loc : map.getSpawnPoints()) {
                            player.sendMessage(mm.getName() + " - [" + count + "] x:" + loc.getX()+ " y: " + loc.getY() + " z: " + loc.getZ());
                            count++;
                        }
                        break;
                    case "remove":
                        if (args.length < 2) {
                            mm.msg(sender, Msg.USAGE, "/map spawnpoint remove [id]");
                            return;
                        }
                        List<CachedLocation> locs = new ArrayList<>();
                        int id;
                        try {
                            id = Integer.parseInt(args[1]);
                        } catch (Exception e) {
                            id = -1;
                        }
                        if (id < 0 || id >= map.getSpawnPoints().size()) {
                            mm.msg(sender, Msg.MAP_INVAILID_ID);
                            return;
                        }
                        if (map.getSpawnPoints() != null) {
                            locs.addAll(map.getSpawnPoints());
                        }
                        locs.remove(id);
                        map.setSpawnPoints(locs.toArray(new CachedLocation[locs.size()]));
                        mm.msg(sender, Msg.MAP_MODIFY_SPAWNPOINT, locs.size());
                        break;
                    default:
                        mm.msg(sender, Msg.USAGE, "/map spawnpoint <add|remove|list> [id]");
                        break;
                }
            } else if (cName.equals("publish")) {
                if (!unFinishedMaps.containsKey(nPlayer)) {
                    mm.msg(sender, Msg.MAP_MISSING);
                    return;
                }
                GMap map = unFinishedMaps.get(nPlayer);
                if (map.getSpawnPoints() == null || !(map.getSpawnPoints().size() >= 1)) {
                    mm.msg(sender, Msg.MAP_FEW_SPAWNPOINTS);
                    return;
                }
                if (map.getSpawnPoints() == null || !(map.getSpawnPoints().size() >= 1)) {
                    mm.msg(sender, Msg.MAP_FEW_SPAWNPOINTS);
                    return;
                }
                gm.getWorldParser().getMaps().add(map);
                unFinishedMaps.remove(nPlayer);
                mm.msg(sender, Msg.MAP_PUBLISHED);
            } else if (cName.equalsIgnoreCase("list")) {
                for (GMap map : gm.getWorldParser().getMaps()) {
                    sender.sendMessage(mm.getName() + " " + "Map: " + map.getName());
                }

            } else if (cName.equalsIgnoreCase("load")) {
                if (args.length <= 0) {
                    if (getWorlds().contains(args[0].toLowerCase(Locale.getDefault()))) {
                        //TODO: Complete me bitch.
                    }
                } else {
                    mm.msg(sender, Msg.USAGE, "/map load <World>");
                }

            } else {
                mm.msg(sender, Msg.USAGE, "/map [create|edit|beacon|spawnpoint|publish|list|load]");
            }
        } else {
            mm.msg(sender, Msg.IN_GAME_COMMAND);
        }
    }

    private GMap findProject(World world) {
        for (GMap g : gm.getWorldParser().getMaps()) {
            if (g.getWorld().getName().equals(world.getName())) {
                return g;
            }
        }
        return null;
    }

    private List<String> getWorlds() {
        List<String> worlds = new ArrayList<>();
        for (World world : gm.getCaptureThePoint().getServer().getWorlds()) {
            worlds.add(world.getName().toLowerCase(Locale.getDefault()));
        }
        return worlds;
    }
}
