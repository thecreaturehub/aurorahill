package com.projectbarks.capturethepoint;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.bukkit.Location;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.java.JavaPlugin;

import com.projectbarks.capturethepoint.commands.KitCommands;
import com.projectbarks.capturethepoint.commands.MapCommands;
import com.projectbarks.capturethepoint.commands.SingleCommands;
import com.projectbarks.capturethepoint.commands.StatsCommand;
import com.projectbarks.capturethepoint.commands.TimerCommands;
import com.projectbarks.capturethepoint.commands.VoteManagerC;
import com.projectbarks.capturethepoint.config.Config;
import com.projectbarks.capturethepoint.config.extensions.JSQLBackend;
import com.projectbarks.capturethepoint.config.extensions.WorldParser;
import com.projectbarks.capturethepoint.game.GameManager;
import com.projectbarks.capturethepoint.game.TimerC;
import com.projectbarks.capturethepoint.game.bar.BarAPI;
import com.projectbarks.capturethepoint.game.scoreboards.ScoreboardManager;
import com.projectbarks.capturethepoint.kits.EconomyManager;
import com.projectbarks.capturethepoint.kits.KitGUI;
import com.projectbarks.capturethepoint.kits.KitManagerC;
import com.projectbarks.capturethepoint.kits.backend.KitAPIImpl;
import com.projectbarks.capturethepoint.listeners.OnBlock;
import com.projectbarks.capturethepoint.listeners.OnCreatureSpawn;
import com.projectbarks.capturethepoint.listeners.OnEntityShootBow;
import com.projectbarks.capturethepoint.listeners.OnPing;
import com.projectbarks.capturethepoint.listeners.OnPlayerChat;
import com.projectbarks.capturethepoint.listeners.OnPlayerConnect;
import com.projectbarks.capturethepoint.listeners.OnPlayerDeath;
import com.projectbarks.capturethepoint.listeners.OnPlayerFoodChange;
import com.projectbarks.capturethepoint.listeners.OnPlayerMove;
import com.projectbarks.capturethepoint.listeners.OnPotionDrink;
import com.projectbarks.capturethepoint.listeners.OnTeleport;
import com.projectbarks.capturethepoint.listeners.OnWinnerChange;
import com.projectbarks.capturethepoint.messages.MessageManager;
import com.projectbarks.capturethepoint.stats.StatsManager;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class CaptureThePoint extends JavaPlugin {
    
    public static final Logger LOG = Logger.getLogger(CaptureThePoint.class.getName());
    private GlobalManager gm;
    
    @Override
    public void onLoad() {
        this.gm = new GlobalManagerC(this);
        GlobalManagerC.setGameManager((GlobalManagerC) this.gm);
        this.gm.enableClass(BarAPI.class);
        this.gm.enableClass(JSQLBackend.class);
        this.gm.enableClass(Config.class);
        this.gm.enableClass(KitAPIImpl.class);
        this.gm.enableClass(KitManagerC.class);
        this.gm.enableClass(StatsManager.class);
        this.gm.enableClass(WorldParser.class);
        this.gm.enableClass(MessageManager.class);
        this.gm.enableClass(VoteManagerC.class);
        this.gm.enableClass(GameManager.class);
        this.gm.enableClass(ScoreboardManager.class);
        this.gm.enableClass(TimerC.class);
        this.gm.enableClass(MapCommands.class);
        this.gm.enableClass(KitGUI.class);
        
        
    }
    
    @Override
    public void onEnable() {
        ((EconomyManager) gm.getKitManager()).init();
        gm.getConfig().load();
        ((KitAPIImpl)gm.getKitAPI()).enable();
        gm.getStatsManager().loadAllStats();
        gm.getMsgManager().load();
        gm.getWorldParser().load();
        gm.getVoteManager().reloadVoteMaps();
        ScoreboardManager.reset();
        getTimerClass().runTaskTimer(this, 0, 20L);
        this.buildPermsCmds();
        this.registerListeners();
        Location lobby = gm.getConfig().getLobby();
        lobby.getWorld().setSpawnLocation(lobby.getBlockX(), lobby.getBlockY(), lobby.getBlockZ());
    }

    /**
     * IDk
     *
     */
    @Override
    public void onDisable() {
        gm.getWorldParser().save();
        getTimerClass().pause(true);
        ((KitAPIImpl)gm.getKitAPI()).disable();
        if (getTimerClass().inGame()) {
            GameManager.endGame(gm.getTimer().getGame(), true);
        }
        // Start hcherndon - shutdown JSQL
        gm.getJsqlBackend().shutdown();
        // End hcherndon
        getTimerClass().cancel();
        GameManager.restart();
    }

    /**
     *
     * @param args
     * @return
     */
    public String buildArgs(String[] args) {
        StringBuilder nameBuff = new StringBuilder();
        for (String n : args) {
            nameBuff.append(" ").append(n);
        }
        return nameBuff.toString().trim();
    }
    
    private void buildPermsCmds() {
        //Map Manager Setup
        Map<String, Boolean> children = new HashMap<>();
        String[] maps = {"create", "edit", "beacon", "spawnpoint", "publish", "list"};
        for (String cmd : maps) {
            children.put("capturethepoint.map." + cmd, true);
        }
        children.put("capturethepoint.sc.setlobby", true);
        children.put("capturethepoint.modifyblock", true);
        children.put("capturethepoint.settime", true);
        children.put("capturethepoint.kitcreate", true);
        getCommand("map").setExecutor(gm.getMapManager());
        Permission permissions = new Permission("capturethepoint.*", PermissionDefault.OP, children);
        getServer().getPluginManager().addPermission(permissions);

        //Vote setup
        getCommand("vote").setExecutor((VoteManagerC) gm.getVoteManager());
        getCommand("timer").setExecutor(new TimerCommands(gm));
        getCommand("stats").setExecutor(new StatsCommand(gm));
        SingleCommands sc = new SingleCommands(gm);
        getCommand("setlobby").setExecutor(sc);
        getCommand("lobby").setExecutor(sc);
        getCommand("kit").setExecutor(new KitCommands(gm));
    }
    
    private void registerListeners() {
        gm.getListenerInvGUI().enable();
        new OnTeleport();
        new OnPlayerConnect(gm);
        new OnPlayerDeath(gm);
        new OnWinnerChange();
        new OnBlock();
        new OnPlayerFoodChange();
        new OnPotionDrink(gm.getConfig());
        new OnPlayerChat(gm);
        new OnPing();
        new OnCreatureSpawn();
        new OnEntityShootBow(gm);
        new OnPlayerMove(gm);
    }
    
    public static TimerC getTimerClass() {
        return (TimerC) GlobalManagerC.getGlobalManager().getTimer();
    }
}
