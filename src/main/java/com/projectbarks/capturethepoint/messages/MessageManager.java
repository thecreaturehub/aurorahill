package com.projectbarks.capturethepoint.messages;

import com.projectbarks.capturethepoint.CaptureThePoint;
import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.config.AbstractConfig;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class MessageManager extends AbstractConfig {

    private String name;

    public MessageManager(GlobalManager gm) {
        super(gm.getCaptureThePoint(), "messages");
    }

    @Override
    public void loadConfig() {
        CaptureThePoint.LOG.log(Level.INFO, "Loading {0} messages", Msg.values().length);
        try {
            name = this.getConfig().getString("name", "&8[&6CTP&8]&7");

            for (Msg key : Msg.values()) {
                String cmsg = this.getConfig().getString("Messages." + key.getName(), key.getMsg());
                key.setMsg(cmsg);
            }
            save();
            CaptureThePoint.LOG.log(Level.INFO, "Message Loading Completed");
        } catch (Exception ex) {
            CaptureThePoint.LOG.log(Level.WARNING, "Message parsing failed");
            CaptureThePoint.LOG.log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void saveConfig() {
        this.getConfig().set("name", name);

        for (Msg msg : Msg.values()) {
            this.getConfig().set("Messages." + msg.getName(), msg.getMsg());
        }

        try {
            this.getConfig().save(this.getFile());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public String getName() {
        return ChatColor.translateAlternateColorCodes('&', name);
    }

    public String getMsgNoTitle(Msg format) {
        return ChatColor.translateAlternateColorCodes('&', format.getMsg());
    }

    public String getMsgNoTitle(Msg format, Object... args) {
        return ChatColor.translateAlternateColorCodes('&', String.format(format.getMsg(), args));
    }

    public String getMsg(Msg format) {
        return this.getName() + " " + this.getMsgNoTitle(format);
    }

    public String getMsg(Msg format, Object... args) {
        return this.getName() + " " + this.getMsgNoTitle(format, args);
    }

    public void msg(CommandSender sender, Msg message) {
        sender.sendMessage(this.getMsg(message));
    }

    public void msg(CommandSender sender, Msg message, Object... args) {
        sender.sendMessage(this.getMsg(message, args));
    }

    public void broadcast(Msg message) {
        Bukkit.getServer().broadcastMessage(this.getMsg(message));
    }

    public void broadcast(Msg message, Object... args) {
        Bukkit.getServer().broadcastMessage(this.getMsg(message, args));
    }
}