package com.projectbarks.capturethepoint.messages;

import java.util.Locale;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public enum Msg {

    USAGE("&4Usage: %s"),
    IN_GAME_COMMAND("This command can only be run in-game!"),
    MAP_CURRENT("You are currently editing a map."),
    MAP_OPEN("You have opened a new map project in this world"),
    MAP_MISSING("You do not have any map projects opened"),
    MAP_WORLD_NON("There are no maps in this world"),
    MAP_PUBLISHED("Your map has been published"),
    MAP_FEW_BEACONS("There are not enough beacons in this map"),
    MAP_FEW_SPAWNPOINTS("There are not enough spawnpoints in this map"),
    MAP_MODIFY_BEACON("There are now &8[&6%s&8] beacons in this map"),
    MAP_MODIFY_SPAWNPOINT("There are now &8[&6%s&8] spawnpoints in this map"),
    MAP_INVAILID_ID("&cInvailid map id!"),
    MAP_SIMILARITY("&cThere already is a map with the same &8[&6%s&8]"),
    VOTE_IN_GAME("&c7You cannot vote when in a game!"),
    VOTE_BAD_ID("&cInvalid id!"),
    VOTE_ID_LARGE("&cMap id is too large! ID must be smaller then &8[&6%s&8]"),
    VOTE_ID_SMALL("&cMap id is too small! ID must be larger then &8[&6%s&8]"),
    VOTE_UPDATE("&7Your map now has &8[&6%s&8]&7 votes!"),
    VOTE_ALREADY("&cYou have already voted!"),
    LOBBY_TIME("&8[&e%s&8]&c %s until lobby ends!"),
    LOBBY_PLAYER_COUNT("&2Players Waiting&8: [&6%s&8/&6%s&8]&2. Game require&8: [&6%s&8]&2 to play."),
    LOBBY_VOTE("&2Vote for a map using &8[&a/vote #&8]&2."),
    LOBBY_MAP("&7ID &a%s&8 > |&e %s&7 Votes&8 |&2 %s"),
    LOBBY_SET("The lobby has been set to your location"),
    GAME_FEW_PLAYERS("&cToo few players to start the game, countdown reset."),
    GAME_STARTED("The Game has Started!"),
    GAME_TIME("&8[&e%s&8]&c %s until game ends!"),
    GAME_BEACON_CHANGE("&2The Current Beacon is at&8: [&6%s&8]"),
    GAME_NO_WINNER("&8[&enobody&8]&2 won with a score of&8: [&60&8]&2"),
    GAME_WINNER("&8[&e%s&8]&2 has won with a score of&8: [&6%s&8]&2"),
    GAME_WINNERS("&8[&e%s&8]&2''s has won with a score of&8: [&6%s&8]&2"),
    GAME_WINNER_CHANGED("&8[&e%s&8]&2 has taken the lead!"),
    GAME_ENDED("&2The Game has ended!"),
    GAME_POINTS("Points&8: [&6%s&8]"),
    TIMER_COMMANDS_PAUSED("Game timer paused"),
    TIMER_COMMANDS_RESUMED("Game timer resumed"),
    TIMER_COMMANDS_STARTED("Game force started"),
    TIMER_COMMANDS_ENDED("Game force ended"),
    TOKENS_CHANGED("You now have&8: [&e%s&8]&7 %s."),
    KIT_CHANGE("&7Your kit has been changed to:&e %s"),
    KIT_FEW_MONEY("&cInsufficient funds for&8: [&6%s&8]"),
    KIT_BUY("You have bought&8: [&6%s&8]&7 for&8: [&6%s&8]"),
    KICK_GAME("Game in Progress!"),
    KICK_SERVER_REBOOTING("Server rebooting for new game."),
    WELCOME("Welcome! Enjoy the game!"),
    RECONNECT("%s, has rejoined the action"),
    CONNECT("%s, has joined the action!"),
    DISCONNECT("%s, has left the action"),
    DEATH("%s has fallen!"),
    TELEPORTING("Teleporting..."),
    TELEPORTING_IN_GAME("&cYou cannot teleport during a game"),
    STATS_POINTS("Total Points&8: [&6%s&8]"),
    STATS_GAMES("Games Played&8: [&6%s&8]&7, winning&8: [&6%s&8]"),
    STATS_KILLS_DEATH("Kill/Death Ratio&8: [&6%s&8/&6%s&8]"),
    STATS_TOKENS("%s&8: [&6%s&8]"),
    STATS_OPPONENTS("Total Opponents&8: [&6%s&8]"),
    STATS_RAGE_QUITS("Total Rage Quits&8: [&6%s&8]"),
    STATS_RP("Rank&8: &o#%s &r&7with&8: %s&oRP"),
    STATS("&2Latest Stats on&8: [&6%s&8]"),
    STATS_MISSING_PLAYER("Player could not be found"),
    STATS_DATABASE_OFFLINE("&cThe stats are currently offline! Warn staff."),
    PERMISSION("&cYou do not have permission to use this"),
    SCOREBOARD_LOBBY("&8&l[&6LOBBY&8&l]"),
    SCOREBOARD_LOBBY_TITLE1("&8&lPlayers"),
    SCOREBOARD_LOBBY_MAX("&6Max&8:&7 %s"),
    SCOREBOARD_LOBBY_MIN("&6Min&8:&7 %s"),
    SCOREBOARD_LOBBY_ONLINE("&6Online&8:&7 "),
    SCOREBOARD_LOBBY_TITLE2("&8&lMaps"),
    SCOREBOARD_LOBBY_MAPS("&6%s"),
    SUFFIX_WINNER("&8[&6%s&8]&r");
    
    private String msg;

    Msg(String message) {
        this.msg = message;
    }

    public String getName() {
        return this.toString().toUpperCase(Locale.getDefault());
    }
    
    public String getMsg() {
        return msg;
    }
    
    public void setMsg(String msg) {
        this.msg = msg;
    }
}
