package com.projectbarks.capturethepoint.kits;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.kits.backend.Kit;
import com.projectbarks.capturethepoint.kits.backend.KitAPI;
import com.projectbarks.capturethepoint.misc.GameUtils;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class KitManagerC extends EconomyManager implements KitManager {

    private Map<Player, String> playersKits;
    private KitAPI api;

    public KitManagerC(GlobalManager gm) {
        super();
        this.playersKits = new HashMap<>();
        this.api = gm.getKitAPI();
    }

    @Override
    public List<Kit> getKits() {
        List<Kit> kits = new ArrayList<>();
        for (String s : getApi().getKitsList()) {
            if (s != null) {
                kits.add(getApi().getKit(s));
            }
        }
        return kits;
    }

    @Override
    public void applyKit(Player p, Kit k) {
        Kit kit = kitCheck(k);
        PlayerInventory inv = p.getInventory();
        GameUtils.resetPlayer(p, GameUtils.PlayerInfo.values());
        int i = 0;
        for (ItemStack stack : kit.getInventory()) {
            inv.setItem(i, stack);
            i++;
        }
        inv.setBoots(kit.getBoots());
        inv.setLeggings(kit.getLeggings());
        inv.setChestplate(kit.getChestplate());
        inv.setHelmet(kit.getHelmet());
        p.updateInventory();
    }

    @Override
    public void buyKit(Player p, Kit k) {
        Kit kit = kitCheck(k);
        super.getEconomy().withdrawPlayer(p.getName(), k.getCost());
        applyKit(p, kit);
    }

    private Kit getDefaultKit() {
        for (Kit kit : getKits()) {
            if (kit.getCost() == 0) {
                return kit;
            }
        }
        List<ItemStack> inventory = new ArrayList<>();
        ItemStack air = new ItemStack(Material.AIR);
        return new Kit("Default", 0, 0, air, air, air, air, inventory);
    }

    @Override
    public Kit getKit(Material mat) {
        for (Kit k : getKits()) {
            if (mat.getId() == k.getID()) {
                return k;
            }
        }
        return null;
    }

    @Override
    public Kit getKit(String name) {
        for (Kit k : getKits()) {
            // Hopefull match
            if (k.getName().equalsIgnoreCase(name)) {
                return k;
            } else {
                String fName = ChatColor.translateAlternateColorCodes('&', k.getName());
                fName = ChatColor.stripColor(fName);
                String newName = ChatColor.stripColor(name);
                if (fName.equalsIgnoreCase(newName)) {
                    return k;
                }
            }
        }
        return null;
    }

    private Kit kitCheck(Kit k) {
        if (k == null) {
            return getDefaultKit();
        } else {
            return k;
        }
    }

    @Override
    public Kit getPlayerKit(Player player) {
        if (playersKits.containsKey(player)) {
            return api.getKit(playersKits.get(player));
        }
        return getDefaultKit();
    }

    @Override
    public boolean canAffordKit(Player p, Kit k) {
        return this.getEconomy().has(p.getName(), k.getCost());
    }

    @Override
    public Map<Player, String> getPlayersKits() {
        return playersKits;
    }

    public KitAPI getApi() {
        return api;
    }
}
