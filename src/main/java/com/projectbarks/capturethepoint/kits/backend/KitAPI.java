package com.projectbarks.capturethepoint.kits.backend;

import java.util.List;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public interface KitAPI {

    /**
     * Returns the kit you search for
     *
     * @param kitname the kit to get
     * @return the kit if found
     */
    public Kit getKit(String kitname);

    /**
     * Adds a kit
     *
     * @param name the kit name
     * @param iconID
     * @param cost the cost of the kit
     * @param inv the ItemStacks of the inventory
     * @return
     */
    public boolean addKit(String name, Integer iconID, Integer cost, PlayerInventory inv);

     public boolean updateKit(String name, Integer iconID, Integer cost, PlayerInventory inv);

    /**
     * Deletes a kit
     *
     * @param kitName the name of the kit to delete
     */
    public void removeKit(String kitName);

    /**
     * Returns the list of kit names
     *
     * @return the list of kit names
     */
    public List<String> getKitsList();
}
