package com.projectbarks.capturethepoint.kits.backend;

import java.util.List;

import org.bukkit.inventory.ItemStack;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class Kit {

    private String name;
    private Integer iD;
    private Integer cost;
    private ItemStack helmet, chestplate, leggings, boots;
    private List<ItemStack> inventory;

    public Kit(String name, Integer id, Integer cost, ItemStack helmet, ItemStack chestplate, ItemStack leggings, ItemStack boots, List<ItemStack> inventory) {
        this.name = name;
        this.iD = id;
        this.cost = cost;
        this.helmet = helmet;
        this.chestplate = chestplate;
        this.leggings = leggings;
        this.boots = boots;
        this.inventory = inventory;
    }

    public String getName() {
        return name;
    }

    public int getCost() {
        return cost;
    }

    public int getID() {
        return iD;
    }

    public ItemStack getHelmet() {
        return helmet;
    }

    public ItemStack getChestplate() {
        return chestplate;
    }

    public ItemStack getLeggings() {
        return leggings;
    }

    public ItemStack getBoots() {
        return boots;
    }

    public List<ItemStack> getInventory() {
        return inventory;
    }
}
