package com.projectbarks.capturethepoint.kits.backend;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import com.projectbarks.capturethepoint.CaptureThePoint;
import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.GlobalManagerC;
import com.projectbarks.capturethepoint.config.Config;
import com.projectbarks.capturethepoint.misc.InventoryUtils;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class KitAPIImpl implements KitAPI {

    public Connection c;
    public DatabaseMode dbMode;
    public List<String> kitNames;
    public Map<String, Kit> cache;
    private Config config;
    private Logger log = CaptureThePoint.LOG;

    public KitAPIImpl(GlobalManager gm) {
        this.config = gm.getConfig();
        kitNames = new ArrayList<>();
        cache = new HashMap<>();
    }

    public void disable() {
        try {
            if (c == null) {
                return;
            }
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void enable() {
        switch (DatabaseMode.match(config.getKitDatabaseMode())) {
            case Mysql:
                initMysql();
                dbMode = DatabaseMode.Mysql;
                log.info("Connected to the chosen database!");
                break;
            case Sqlite:
                initSqlite();
                dbMode = DatabaseMode.Sqlite;
                log.info("Connected to the chosen database!");
                break;
            default:
                log.log(Level.WARNING, "Improper connection mode: {0}", config.getKitDatabaseMode());
                break;
        }
    }

    private void initSqlite() {
        Statement s;
        try {
            String absolutePath = GlobalManagerC.getGlobalManager().getCaptureThePoint().getDataFolder().getAbsolutePath();
            c = DriverManager.getConnection("jdbc:sqlite:" + absolutePath + "/data.db");
            s = c.createStatement();

            String createTable_Kits = "CREATE TABLE IF NOT EXISTS KITS "
                                      + "(NAME TEXT NOT NULL,"
                                      + "ICON INT NOT NULL,"
                                      + "COST INT NOT NULL,"
                                      + "ARMOR TEXT NOT NULL,"
                                      + "CONTENTS TEXT NOT NULL);";

            s.executeUpdate(createTable_Kits);

            String select = "SELECT * FROM KITS";
            ResultSet rs = s.executeQuery(select);

            while (rs.next()) {
                if (rs.getString("NAME") == null) {
                    continue;
                }
                kitNames.add(rs.getString("NAME"));
            }

            s.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initMysql() {
        Properties props = new Properties();

        props.put("user", config.getMysqlUsername());
        props.put("password", config.getMysqlPassword());

        try {
            Statement s;

            c = DriverManager.getConnection("jdbc:mysql://" + config.getMysqlHost() + ":" + config.getMysqlPort() + "/" + config.getMysqlDatabase(), props);
            s = c.createStatement();

            String selectDB = "USE " + config.getMysqlDatabase();

            s.executeUpdate(selectDB);

            String createTable_Kits = "CREATE TABLE IF NOT EXISTS " + "KITS "
                                      + "(NAME varchar(16),"
                                      + "ICON INT,"
                                      + "COST INT,"
                                      + "COSTSTOKENS BOOLEAN,"
                                      + "ARMOR TEXT,"
                                      + "CONTENTS TEXT);";

            s.executeUpdate(createTable_Kits);

            String select = "SELECT * FROM KITS";
            ResultSet rs = s.executeQuery(select);

            while (rs.next()) {
                if (rs.getString("NAME") == null) {
                    continue;
                }
                kitNames.add(rs.getString("NAME"));
            }

            s.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public DatabaseMode getDatabaseMode() {
        return dbMode;
    }

    public void sendMessage(CommandSender sender, String message) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
    }

    /**
     * Returns the kit you search for
     *
     * @param kitname the kit to get
     * @return the kit if found
     */
    @Override
    public Kit getKit(String kitname) {

        if (dbMode == null || c == null) {
            return null;
        }
        
        if (cache.containsKey(kitname.toLowerCase())) {
            return cache.get(kitname.toLowerCase());
        }

        Statement s;

        try {
            s = c.createStatement();
            String select = "";
            if (dbMode == DatabaseMode.Sqlite) {
                select = "SELECT * FROM KITS WHERE NAME = '" + kitname + "'";
            } else if (dbMode == DatabaseMode.Mysql) {
                select = "SELECT * FROM KITS WHERE NAME = '" + kitname + "'";
            }

            ResultSet rs = s.executeQuery(select);

            if (rs == null || !rs.next()) {
                return null;
            }

            Map<Integer, ItemStack> armorMap = new HashMap<>();
            List<ItemStack> contentsList = new ArrayList<>();

            int id = rs.getInt("ICON");
            int cost = rs.getInt("COST");
            String armor = rs.getString("ARMOR");
            String contentString = rs.getString("CONTENTS");
            s.close();

            if (armor != null) {

                int counter = 0;
                for (ItemStack item : InventoryUtils.itemStackArrayFromBase64(armor)) {
                    armorMap.put(counter, item);
                    counter++;
                }
            }

            if (contentString != null) {
                contentsList.addAll(Arrays.asList(InventoryUtils.itemStackArrayFromBase64(contentString)));
            }
            Kit kit = new Kit(kitname, id, cost, armorMap.get(3), armorMap.get(2), armorMap.get(1), armorMap.get(0), contentsList);
            this.cache.put(kitname, kit);
            return kit;
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Adds a kit
     *
     * @param name the kit name
     * @param iconID
     * @param cost the cost of the kit
     * @param inv the ItemStacks of the inventory
     * @return
     */
    @Override
    public boolean addKit(String name, Integer iconID, Integer cost, PlayerInventory inv) {
        if (dbMode == null || c == null) {
            return false;
        }

        String newName = name.toLowerCase();
        if (kitNames.contains(name)) {
            return false;
        }


        try {
            String[] contents = InventoryUtils.playerInventoryToBase64(inv);
            Statement s = c.createStatement();

            String insert = "";
            if (dbMode == DatabaseMode.Sqlite) {
                insert = "INSERT INTO KITS(NAME,ICON,COST,ARMOR,CONTENTS) VALUES('" + newName + "', " + iconID + ", "
                         + cost + ", '" + contents[1] + "', '" + contents[0] + "')";
            } else if (dbMode == DatabaseMode.Mysql) {
                insert = "INSERT INTO KITS(NAME,ICON,COST,ARMOR,CONTENTS) VALUES('" + newName + "', " + iconID + ", "
                         + cost + ", '" + contents[1] + "', '" + contents[0] + "')";
            }
            s.executeUpdate(insert);

            s.close();
            kitNames.add(newName);
            return true;

        } catch (IllegalStateException | SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean updateKit(String name, Integer iconID, Integer cost, PlayerInventory inv) {
        if (dbMode == null || c == null) {
            return false;
        }

        if (!kitNames.contains(name)) {
            return addKit(name, iconID, cost, inv);
        }

        try {
            String[] contents = InventoryUtils.playerInventoryToBase64(inv);
            Statement s = c.createStatement();

            String update = "";
            if (dbMode == DatabaseMode.Sqlite) {
                update = "UPDATE KITS SET ICON=" + iconID + ",COST=" + cost + ",ARMOR='" + contents[1]
                         + "',CONTENTS='" + contents[0] + "' WHERE NAME='" + name + "'";
            } else if (dbMode == DatabaseMode.Mysql) {
                update = "UPDATE KITS SET ICON=" + iconID + ",COST=" + cost + ",ARMOR='" + contents[1]
                         + "',CONTENTS='" + contents[0] + "' WHERE NAME='" + name + "'";
            }
            s.executeUpdate(update);

            s.close();
            return true;

        } catch (IllegalStateException | SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Deletes a kit
     *
     * @param kitName the name of the kit to delete
     */
    @Override
    public void removeKit(String kitName) {
        if (dbMode == null || c == null) {
            return;
        }
        try {
            Statement s = c.createStatement();
            String delete = "";
            if (dbMode == DatabaseMode.Sqlite) {
                delete = "DELETE FROM KITS WHERE NAME ='" + kitName + "'";
            } else if (dbMode == DatabaseMode.Mysql) {
                delete = "DELETE FROM KITS WHERE NAME ='" + kitName + "'";
            }
            s.executeUpdate(delete);
            s.close();
            kitNames.remove(kitName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Returns the list of kit names
     *
     * @return the list of kit names
     */
    @Override
    public List<String> getKitsList() {
        return kitNames;
    }
}
