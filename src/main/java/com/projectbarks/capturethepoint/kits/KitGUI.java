package com.projectbarks.capturethepoint.kits;

import com.projectbarks.capturethepoint.EventCore;
import com.projectbarks.capturethepoint.GlobalManager;
import com.projectbarks.capturethepoint.config.Config;
import com.projectbarks.capturethepoint.kits.backend.Kit;
import com.projectbarks.capturethepoint.listeners.CTPEventHandler;
import com.projectbarks.capturethepoint.messages.Msg;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventException;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class KitGUI implements CTPEventHandler {

    private GlobalManager gm;
    private Config config;
    private KitManager km;
    private ItemStack[] items;

    public KitGUI(GlobalManager gm) {
        this.gm = gm;
        this.config = gm.getConfig();
        this.km = gm.getKitManager();
    }

    public void enable() {
        EventCore.register(PlayerInteractEvent.class, this);
        EventCore.register(InventoryClickEvent.class, this);
        EventCore.register(PlayerDropItemEvent.class, this);
    }

    public void onPlayerInteract(PlayerInteractEvent event) {
        Material item;
        if (event.getItem() == null) {
            item = Material.AIR;
        } else {
            item = event.getItem().getType();
        }
        if (item == config.getKitSelector().getType()) {
            if (gm.getTimer().inLobby()) {
                event.getPlayer().openInventory(getInv(event.getPlayer()));
                event.setCancelled(true);
            }
        } else if (!gm.getTimer().inGame() && !event.getPlayer().hasPermission("capturethepoint.modifyblock")) {
            event.setCancelled(true);
        }
    }

    public void onPlayerClick(InventoryClickEvent event) {
        if (event.getView().getTitle().equals(
                getInv((Player) event.getView().getPlayer()).getTitle())) {
            if (event.getRawSlot() < event.getView().getTopInventory().getSize()) {
                try {
                    ItemStack currentItem = event.getCurrentItem();
                    if (currentItem != null && currentItem.hasItemMeta()) {
                        Kit kit = km.getKit(currentItem.getItemMeta().getDisplayName());
                        if (kit != null && event.getWhoClicked() instanceof Player) {
                            Player player = ((Player) event.getWhoClicked());
                            if (km.canAffordKit(player, kit)) {
                                km.applyKit(player, kit);
                                player.getInventory().addItem(gm.getConfig().getKitSelector());
                                km.getPlayersKits().put(player, kit.getName());
                                gm.getMsgManager().msg(player, Msg.KIT_CHANGE, kit.getName());
                            } else {
                                gm.getMsgManager().msg(player, Msg.KIT_FEW_MONEY, kit.getName());
                            }
                            event.setCancelled(true);
                            player.updateInventory();
                        }
                    }
                } catch (Exception e) {
                    event.setCancelled(true);
                    e.printStackTrace();
                }
            }
        }
    }

    /*@EventHandler(priority = EventPriority.HIGHEST)
     public void onPlayerItemMove(InventoryMoveItemEvent event) {
     if (event.getDestination().getTitle().equals(getInv(event.).getTitle())
     || event.getSource().getTitle().equals(getInv().getTitle())) {
     event.setCancelled(true);
     }
     }*/
    private Inventory getInv(Player player) {
        Integer row = calRow(km.getKits().size());
        String name = ChatColor.translateAlternateColorCodes('&', "&0Kits[&6"
                                                                  + gm.getKitManager().getEconomy().getBalance(player.getName())
                                                                  + "$&8]");
        Inventory inventory = Bukkit.getServer().createInventory(null, row * 9, name);
        if (items == null) {
            for (Kit kit : km.getKits()) {
                ItemStack item = new ItemStack(Material.getMaterial(kit.getID()), 1);
                ItemMeta meta = item.getItemMeta();
                String kitName = kit.getName();
                kitName = ChatColor.translateAlternateColorCodes('&', kitName);
                if (kitName.length() == kit.getName().length()) {
                    kitName = ChatColor.WHITE + kitName;
                }
                meta.setDisplayName(kitName);
                List<String> strings = new ArrayList<>();
                strings.add(ChatColor.GREEN + "Tokens: " + ChatColor.DARK_GRAY + kit.getCost());
                meta.setLore(strings);
                item.setItemMeta(meta);
                inventory.addItem(item);
            }
            this.items = inventory.getContents();
        }
        inventory.setContents(items.clone());
        return inventory;
    }

    public void preventDropEvent(PlayerDropItemEvent event) {
        if (!event.getPlayer().isOp()) {
            event.setCancelled(true);
            event.getPlayer().updateInventory();
        }
    }

    private Integer calRow(Integer items) {
        int val = items / 9;
        return val <= 0 ? 1 : val;
    }

    @Override
    public void execute(Listener ll, Event event) throws EventException {
        if (event instanceof PlayerInteractEvent) {
            this.onPlayerInteract((PlayerInteractEvent) event);
        } else if (event instanceof InventoryClickEvent) {
            this.onPlayerClick((InventoryClickEvent) event);
        } else if (event instanceof PlayerDropItemEvent) {
            this.preventDropEvent((PlayerDropItemEvent) event);
        }
    }
}
