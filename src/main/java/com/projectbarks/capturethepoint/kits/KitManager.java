package com.projectbarks.capturethepoint.kits;

import com.projectbarks.capturethepoint.kits.backend.Kit;
import java.util.List;
import java.util.Map;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public interface KitManager {

    /**
     * Get a list of kits obtained from  Core Dependency
     *
     * @return
     */
    public List<Kit> getKits();

    /**
     * Apply a kit to player.
     *
     * @param p
     * @param k
     */
    public void applyKit(Player p, Kit k);

    /**
     * Purchase and apply a kit.
     *
     * @param p
     * @param k
     */
    public void buyKit(Player p, Kit k);

    /**
     * Match a kit by its Material icon.
     *
     * @param mat
     * @return
     */
    public Kit getKit(Material mat);

    /**
     * Match a kit by its name.
     *
     * @param name
     * @return
     */
    public Kit getKit(String name);

    /**
     * Get a kit assigned to players.
     *
     * @return
     */
    public Map<Player, String> getPlayersKits();

    /**
     * Match a kit assigned to a player. A a substitute empty kit will return
     * instead of null.
     *
     * @param player
     * @return
     */
    public Kit getPlayerKit(Player player);

    /**
     * Checks if a player can afford or cannot afford a kit.
     *
     * @param p
     * @param k
     * @return
     */
    public boolean canAffordKit(Player p, Kit k);

    /**
     * Gets the tied in vault economy.
     *
     * @return
     */
    public Economy getEconomy();
    
    public String getCurrencyName(double amount);
}
