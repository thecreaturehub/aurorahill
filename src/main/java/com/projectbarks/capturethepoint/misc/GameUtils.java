package com.projectbarks.capturethepoint.misc;

import com.projectbarks.capturethepoint.api.timed.EAction;
import com.projectbarks.capturethepoint.api.timed.TimedAction;
import com.projectbarks.capturethepoint.api.timed.TimedActionC;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import lombok.NonNull;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class GameUtils {

    public enum PlayerInfo {

        HEALTH,
        INVENTORY,
        POTIONEFFECTS,
        XP,
        FLIGHT,
        HUNGER;
    }

    public static List<Chunk> getChunkBox(Chunk chunk) {
        List<Chunk> chunks = getChunksTouching(chunk);
        chunks.addAll(getChunksCorners(chunk));
        chunks.add(chunk);
        return chunks;
    }

    public static List<Chunk> getChunksCorners(Chunk chunk) {
        List<Chunk> chunks = new ArrayList<>();
        World world = chunk.getWorld();
        chunks.add(world.getChunkAt(chunk.getX() + 16, chunk.getZ() + 16));
        chunks.add(world.getChunkAt(chunk.getX() - 16, chunk.getZ() - 16));
        chunks.add(world.getChunkAt(chunk.getX() + 16, chunk.getZ() - 16));
        chunks.add(world.getChunkAt(chunk.getX() - 16, chunk.getZ() + 16));
        return chunks;
    }

    public static List<Chunk> getChunksTouching(Chunk chunk) {
        List<Chunk> chunks = new ArrayList<>();
        World world = chunk.getWorld();
        chunks.add(world.getChunkAt(chunk.getX() + 16, chunk.getZ()));
        chunks.add(world.getChunkAt(chunk.getX(), chunk.getZ() + 16));
        chunks.add(world.getChunkAt(chunk.getX() - 16, chunk.getZ()));
        chunks.add(world.getChunkAt(chunk.getX(), chunk.getZ() - 16));
        return chunks;
    }

    public static String[] getTime(Integer time) {
        Integer hours = time / 3_600;
        Integer minutes = time / 60;
        String[] returnS = new String[2];
        if (hours > 0) {
            returnS[0] = ((int) Math.floor(hours)) + "";
            returnS[1] = "hour(s)";
            return returnS;
        } else if (minutes > 0) {
            returnS[0] = ((int) Math.floor(minutes)) + "";
            returnS[1] = "minute(s)";
            return returnS;
        } else {
            returnS[0] = ((int) Math.floor(time)) + "";
            returnS[1] = "second(s)";
            return returnS;
        }
    }

    public static TimedAction findFirstAction(EAction target, Map<Integer, Set<TimedActionC>> actions) {
        for (Set<TimedActionC> actionList : actions.values()) {
            for (TimedAction timedAction : actionList) {
                if (timedAction.getType() == target) {
                    return timedAction;
                }
            }
        }
        return null;
    }

    public static void resetPlayer(@NonNull Player player, PlayerInfo... info) {
        for (PlayerInfo i : info) {
            switch (i) {
                case HEALTH:
                    player.setHealth(20);
                    break;
                case INVENTORY:
                    player.getInventory().clear();
                    player.getInventory().setArmorContents(null);
                    break;
                case POTIONEFFECTS:
                    for (PotionEffect effect : player.getActivePotionEffects()) {
                        player.removePotionEffect(effect.getType());
                    }
                    break;
                case XP:
                    player.setExp(0);
                    player.setLevel(0);
                    break;
                case FLIGHT:
                    player.setFlying(false);
                    break;
                case HUNGER:
                    player.setFoodLevel(18);
                    player.setSaturation(6);
                    break;
                default:
                    break;
            }
        }
    }

    private GameUtils() {
    }
}
