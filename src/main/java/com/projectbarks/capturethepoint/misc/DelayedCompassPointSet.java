package com.projectbarks.capturethepoint.misc;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

/*
 * Copyright 2013 Brandon Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class DelayedCompassPointSet extends BukkitRunnable {

    private final Player player;
    private final Location loc;

    public DelayedCompassPointSet(final Player player, final Location loc) {
        this.player = player;
        this.loc = loc;
    }

    @Override
    public void run() {
        if (!player.isOnline()) {
            return;
        }
        player.setCompassTarget(loc);
    }

    public void scheduleForFire(Plugin plugin) {
        this.runTaskLater(plugin, 5L);
    }
}
