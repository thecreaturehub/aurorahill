package com.hcherndon.jsqlapi;

public class MySQLInfo {

    private String ip;
    private int port;
    private String username;
    private String password;
    private String database;

    public MySQLInfo(String ip, int port, String username, String password, String database) {
        this.ip = ip;
        this.port = port;
        this.username = username;
        this.password = password;
        this.database = database;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getDatabase() {
        return database;
    }

    public String toURL() {
        return "jdbc:mysql://" + ip + ":" + port + "/" + database;
    }

    public String toNoDatabaseURL() {
        return "jdbc:mysql://" + ip + ":" + port;
    }
}
