package com.hcherndon.jsqlapi.internal.tasks;

import com.hcherndon.jsqlapi.MySQLInfo;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DatabaseCreator implements Runnable {

    private MySQLInfo info;

    public DatabaseCreator(MySQLInfo info) {
        this.info = info;
    }

    @Override
    public void run() {
        try (Connection connection = DriverManager.getConnection(info.toNoDatabaseURL(), info.getUsername(), info.getPassword())) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("CREATE SCHEMA IF NOT EXISTS `" + info.getDatabase() + "`;")) {
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
