package com.hcherndon.jsqlapi.internal.tasks;

import com.hcherndon.jsqlapi.JCaller;
import com.hcherndon.jsqlapi.MySQLInfo;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * JSQLAPI by hcherndon is under an All Rights Reserved, created year 2013. No
 * one is allowed permission with out explicit permission from hcherndon.
 */
public class TableClear implements Runnable {

    private final String table;
    private final MySQLInfo info;
    private final JCaller caller;

    public TableClear(String table, MySQLInfo info, JCaller caller) {
        this.table = table;
        this.info = info;
        this.caller = caller;
    }

    @Override
    public void run() {
        try (Connection connection = DriverManager.getConnection(info.toURL(), info.getUsername(), info.getPassword())) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM `" + table + "`;")) {
                preparedStatement.executeUpdate();
                if (caller != null) {
                    caller.onTaskCompletion(table, null);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
