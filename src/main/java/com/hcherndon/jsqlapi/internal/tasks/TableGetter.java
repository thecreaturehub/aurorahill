package com.hcherndon.jsqlapi.internal.tasks;

import com.hcherndon.jsqlapi.JCaller;
import com.hcherndon.jsqlapi.MySQLInfo;
import java.sql.*;
import org.json.JSONObject;

public class TableGetter implements Runnable {

    private final String table;
    private final JCaller<JSONObject[]> caller;
    private final MySQLInfo info;

    public TableGetter(String table, JCaller<JSONObject[]> caller, MySQLInfo info) {
        this.table = table;
        this.caller = caller;
        this.info = info;
    }

    @Override
    public void run() {
        try (Connection connection = DriverManager.getConnection(info.toURL(), info.getUsername(), info.getPassword())) {
            int count = 0;
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT COUNT(*) FROM `" + table + "`;")) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        count = resultSet.getInt("COUNT(*)");
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (count == 0) {
                caller.onTaskCompletion(table, new JSONObject[0]);
            } else {
                try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM `" + table + "`;")) {
                    try (ResultSet resultSet = preparedStatement.executeQuery()) {
                        JSONObject[] objects = new JSONObject[count];
                        int i = 0;
                        while (resultSet.next()) {
                            objects[i] = new JSONObject(resultSet.getString("value"));
                            i++;
                        }
                        caller.onTaskCompletion(table, objects);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
