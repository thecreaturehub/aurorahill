package com.hcherndon.jsqlapi.internal.tasks;

import com.hcherndon.jsqlapi.JCaller;
import com.hcherndon.jsqlapi.MySQLInfo;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TableKeyDeleter implements Runnable {

    private final String table;
    private final String key;
    private final MySQLInfo info;
    private final JCaller caller;

    public TableKeyDeleter(String table, String key, MySQLInfo info, JCaller caller) {
        this.table = table;
        this.key = key;
        this.info = info;
        this.caller = caller;
    }

    @Override
    public void run() {
        try (Connection connection = DriverManager.getConnection(info.toURL(), info.getUsername(), info.getPassword())) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM `" + table + "` WHERE `key`=?;")) {
                preparedStatement.setString(1, key);
                preparedStatement.executeUpdate();
                if (caller != null) {
                    caller.onTaskCompletion(table, null);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
