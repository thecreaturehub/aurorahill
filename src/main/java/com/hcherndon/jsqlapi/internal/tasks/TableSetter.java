package com.hcherndon.jsqlapi.internal.tasks;

import com.hcherndon.jsqlapi.JCaller;
import com.hcherndon.jsqlapi.MySQLInfo;
import java.sql.*;
import org.json.JSONObject;

public class TableSetter implements Runnable {

    private final String table;
    private final String key;
    private final JSONObject value;
    private final MySQLInfo info;
    private final JCaller caller;

    public TableSetter(String table, String key, JSONObject value, MySQLInfo info, JCaller<JSONObject> caller) {
        this.table = table;
        this.key = key;
        this.value = value;
        this.info = info;
        this.caller = caller;
    }

    @Override
    public void run() {
        try (Connection connection = DriverManager.getConnection(info.toURL(), info.getUsername(), info.getPassword())) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM `" + table + "` WHERE `key`=?;")) {
                preparedStatement.setString(1, key);
                try (ResultSet rs = preparedStatement.executeQuery()) {
                    if (!rs.next()) {
                        try (PreparedStatement ps = connection.prepareStatement("INSERT INTO `" + table + "` VALUES(?, ?);")) {
                            ps.setString(1, key);
                            ps.setString(2, value.toString());
                            ps.executeUpdate();
                            if (caller != null) {
                                caller.onTaskCompletion(table, value);
                            }
                        }
                    } else {
                        try (PreparedStatement ps = connection.prepareStatement("UPDATE `" + table + "` SET `value`=? WHERE `key`=?;")) {
                            ps.setString(1, value.toString());
                            ps.setString(2, key);
                            ps.executeUpdate();
                            if (caller != null) {
                                caller.onTaskCompletion(table, value);
                            }
                        }
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
