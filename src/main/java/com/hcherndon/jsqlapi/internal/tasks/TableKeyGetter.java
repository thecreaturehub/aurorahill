package com.hcherndon.jsqlapi.internal.tasks;

import com.hcherndon.jsqlapi.JCaller;
import com.hcherndon.jsqlapi.MySQLInfo;
import java.sql.*;
import org.json.JSONException;
import org.json.JSONObject;

public class TableKeyGetter implements Runnable {

    private final String table;
    private final String key;
    private final JCaller<JSONObject> caller;
    private final MySQLInfo info;

    public TableKeyGetter(String table, String key, JCaller<JSONObject> caller, MySQLInfo info) {
        this.table = table;
        this.key = key;
        this.caller = caller;
        this.info = info;
    }

    @Override
    public void run() {
        try (Connection connection = DriverManager.getConnection(info.toURL(), info.getUsername(), info.getPassword())) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM `" + table + "` WHERE `key`=?;")) {
                preparedStatement.setString(1, key);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (!resultSet.next()) {
                        caller.onTaskCompletion(key, null);
                    } else {
                        JSONObject object = new JSONObject(resultSet.getString("value"));
                        caller.onTaskCompletion(key, object);
                    }
                }
            }
        } catch (SQLException | JSONException e) {
            e.printStackTrace();
        }
    }
}
