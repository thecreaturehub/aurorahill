package com.hcherndon.jsqlapi.internal.tasks;

import com.hcherndon.jsqlapi.JCaller;
import com.hcherndon.jsqlapi.MySQLInfo;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * JSQLAPI by hcherndon is under an All Rights Reserved, created year 2013. No
 * one is allowed permission with out explicit permission from hcherndon.
 */
public class TableCreator implements Runnable {

    private final String tableName;
    private final JCaller caller;
    private final MySQLInfo info;

    public TableCreator(String tableName, JCaller caller, MySQLInfo info) {
        this.tableName = tableName;
        this.caller = caller;
        this.info = info;
    }

    @Override
    public void run() {
        try (Connection connection = DriverManager.getConnection(info.toURL(), info.getUsername(), info.getPassword())) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("CREATE TABLE IF NOT EXISTS `" + tableName + "` (`key` VARCHAR(64), `value` LONGTEXT, PRIMARY KEY(`key`));")) {
                preparedStatement.executeUpdate();
                if (caller != null) {
                    caller.onTaskCompletion(tableName, null);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
