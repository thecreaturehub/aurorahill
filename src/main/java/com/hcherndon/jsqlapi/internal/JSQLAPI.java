package com.hcherndon.jsqlapi.internal;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONObject;

import com.hcherndon.jsqlapi.JCaller;
import com.hcherndon.jsqlapi.JSQL;
import com.hcherndon.jsqlapi.MySQLInfo;
import com.hcherndon.jsqlapi.internal.tasks.DatabaseCreator;
import com.hcherndon.jsqlapi.internal.tasks.TableClear;
import com.hcherndon.jsqlapi.internal.tasks.TableCreator;
import com.hcherndon.jsqlapi.internal.tasks.TableDeleter;
import com.hcherndon.jsqlapi.internal.tasks.TableGetter;
import com.hcherndon.jsqlapi.internal.tasks.TableKeyDeleter;
import com.hcherndon.jsqlapi.internal.tasks.TableKeyGetter;
import com.hcherndon.jsqlapi.internal.tasks.TableSetter;
import com.projectbarks.capturethepoint.CaptureThePoint;


public class JSQLAPI implements JSQL {

    public static JSQLAPI init() {
        return new JSQLAPI();
    }
    private ExecutorService executorService;
    private boolean isRunning = false;
    private MySQLInfo info;
    private Logger log = CaptureThePoint.LOG;

    @Override
    public void start(MySQLInfo info) {
        if (isRunning) {
            throw new IllegalStateException("This instance is already running!");
        }
        this.info = info;
        executorService = Executors.newCachedThreadPool();
        isRunning = true;
    }

    @Override
    public void stop() {
        checkNull();
        executorService.shutdown();
        try {
            executorService.awaitTermination(2L, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            log.log(Level.WARNING, "Exception while attempting to shut down JSQLAPI Thread Cache!");
            e.printStackTrace();
        }
        log.log(Level.INFO, "All threads shut down!");
    }

    @Override
    public void createDatabase() {
        checkNull();
        new DatabaseCreator(info).run();
    }

    @Override
    public void createTable(String tableName, JCaller onCreated) {
        checkNull(tableName);
        executorService.execute(new TableCreator(tableName, onCreated, info));
    }

    @Override
    public void setObject(String table, String key, JSONObject value, JCaller<JSONObject> onSet) {
        checkNull(table, key);
        executorService.execute(new TableSetter(table, key, value, info, onSet));
    }

    @Override
    public void getObject(String table, String key, JCaller<JSONObject> whenFound) {
        checkNull(table, key, whenFound);
        executorService.execute(new TableKeyGetter(table, key, whenFound, info));
    }

    @Override
    public void getAllObjects(String table, JCaller<JSONObject[]> whenFound) {
        checkNull(table, whenFound);
        executorService.execute(new TableGetter(table, whenFound, info));
    }

    @Override
    public void deleteRow(String table, String key, JCaller onDelete) {
        checkNull(table, key);
        executorService.execute(new TableKeyDeleter(table, key, info, onDelete));
    }

    @Override
    public void clearTable(String table, JCaller onClear) {
        checkNull(table);
        executorService.execute(new TableClear(table, info, onClear));
    }

    @Override
    public void deleteTable(String table, JCaller onDelete) {
        checkNull(table);
        executorService.execute(new TableDeleter(table, info, onDelete));
    }

    private void checkNull(Object... objects) {
        if (!isRunning) {
            throw new IllegalStateException("The threads are not running! Call start()!");
        }
        for (Object o : objects) {
            if (o == null) {
                throw new NullPointerException(o.getClass().getSimpleName() + " is null!");
            }
        }
    }
}
