package com.hcherndon.jsqlapi;

public interface JCaller<T> {

    public void onTaskCompletion(String key, T object);
}
