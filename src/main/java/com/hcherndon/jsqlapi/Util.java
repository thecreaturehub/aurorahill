package com.hcherndon.jsqlapi;

import java.lang.reflect.Field;
import java.util.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Util {

    public static JSONObject objectToJson(Object object) {
        JSONObject jsonObject = new JSONObject();
        for (Field field : object.getClass().getDeclaredFields()) {
            try {
                field.setAccessible(true);
                Object fObject = field.get(object);
                if (fObject.getClass().isArray()) {
                    if (isJSONAble((Object[]) fObject)) {
                        jsonObject.put(field.getName(), fObject);
                        continue;
                    }
                }
                if (isJSONAble(fObject)) {
                    jsonObject.put(field.getName(), fObject);
                }
            } catch (JSONException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return jsonObject;
    }

    public static List<Object> convertJSONArrayToList(JSONArray array) {
        List<Object> ret = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            try {
                ret.add(array.get(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return ret;
    }

    public static Map<String, Object> convertJSONObjectToMap(JSONObject object) {
        Map<String, Object> ret = new HashMap<>();
        @SuppressWarnings("unchecked")
        Iterator<String> keys = ((Iterator<String>) object.keys());
        while (keys.hasNext()) {
            String key = keys.next();
            try {
                Object value = object.get(key);
                if (value instanceof JSONObject) {
                    ret.put(key, convertJSONObjectToMap((JSONObject) value));
                } else if (value instanceof JSONArray) {
                    ret.put(key, convertJSONArrayToList((JSONArray) value));
                } else {
                    ret.put(key, value);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return ret;
    }

    public static Object[] convertJSONArrayToArray(JSONArray array) {
        Object[] ret = new Object[array.length()];
        try {
            for (int i = 0; i < array.length(); i++) {
                ret[i] = array.get(i);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ret;
    }

    public static boolean isJSONAble(Object bean) {
        return bean instanceof Integer || bean instanceof Byte || bean instanceof String
               || bean instanceof Character || bean instanceof Double || bean instanceof Float
               || bean instanceof Long || bean instanceof Collection || bean instanceof JSONArray
               || bean instanceof JSONObject || bean instanceof Boolean;
    }

    public static boolean isJSONAble(Object[] bean) {
        return bean instanceof Integer[] || bean instanceof Byte[] || bean instanceof String[]
               || bean instanceof Character[] || bean instanceof Double[] || bean instanceof Float[]
               || bean instanceof Long[] || bean instanceof Collection[] || bean instanceof JSONArray[]
               || bean instanceof JSONObject[] || bean instanceof Boolean[];
    }

    private Util() {
    }
}
