package com.hcherndon.jsqlapi;

import com.hcherndon.jsqlapi.internal.JSQLAPI;
import org.json.JSONObject;

public interface JSQL {

    public static class Creator {

        /**
         * Creates an instance of JSQL with the provided
         * {@link com.hcherndon.jsqlapi.MySQLInfo} info;
         *
         * @return A new {@link com.hcherndon.jsqlapi.JSQL} instance.
         */
        public static JSQL createInstance() {
            return JSQLAPI.init();
        }

        private Creator() {
        }
    }

    /**
     * Must be called first to start the Thread pool.
     *
     * @param info the information to login to your MySQL box.
     */
    public void start(MySQLInfo info);

    /**
     * Called to stop all threads.
     */
    public void stop();

    /**
     * Creates the database if it does not already exist. If it exists, it does
     * nothing. *It is a smart idea to use this.*
     */
    public void createDatabase();

    /**
     * Creates a data table if it doesn't already exist.
     *
     * @param tableName the name of the table to create.
     * @param onCreated this {@link com.hcherndon.jsqlapi.JCaller} will be
     * called when the table has been created. The onCreated can be set to null
     * if you do not need to be alerted that it has finished. *NOTE* The
     * callable will be async from your main thread!
     * @throws NullPointerException if the tableName is null
     */
    public void createTable(String tableName, JCaller onCreated);

    /**
     * Sets the value of a key, it will insert if it does not already exist in
     * the database.
     *
     * @param table the table to set the data in.
     * @param key the key of the data.
     * @param value the data to set to the key.
     * @param onSet called when task is complete. Can be null. *NOTE* The
     * callable will be async from your main thread!
     * @throws NullPointerException if the tableName key or value is null
     */
    public void setObject(String table, String key, JSONObject value, JCaller<JSONObject> onSet);

    /**
     * Grabs an object from the database then calls the whenFound with the data.
     *
     * @param table the table to select data from.
     * @param key the key of the data.
     * @param whenFound the caller statement that will be called when the data
     * is found. *NOTE* The callable will be async from your main thread!
     * @throws NullPointerException if the tableName key or whenFound is null
     */
    public void getObject(String table, String key, JCaller<JSONObject> whenFound);

    /**
     * Grabs all data from the table and returns in an Array.
     *
     * @param table the table to select data from.
     * @param whenFound the caller statement that will be called when the data
     * is recieved. *NOTE* The callable will be async from your main thread!
     * @throws NullPointerException if the table or whenFound is null!
     */
    public void getAllObjects(String table, JCaller<JSONObject[]> whenFound);

    /**
     * Deletes the row of the key from the table. Does nothing if the key is not
     * found.
     *
     * @param table the table to delete from.
     * @param key the key of the data.
     * @param onDelete called when task is complete. Can be null. *NOTE* The
     * callable will be async from your main thread!
     * @throws NullPointerException if the table of key is null!
     */
    public void deleteRow(String table, String key, JCaller onDelete);

    /**
     * Deletes all data from a table.
     *
     * @param table the table to delete from.
     * @param onClear called when task is complete. Can be null. *NOTE* The
     * callable will be async from your main thread!
     * @throws NullPointerException if the table is null!
     */
    public void clearTable(String table, JCaller onClear);

    /**
     * Deletes the table and everything inside it.
     *
     * @param table the table to delete.
     * @param onDelete called when task is complete. Can be null. *NOTE* The
     * callable will be async from your main thread!
     * @throws NullPointerException if the table is null!
     */
    public void deleteTable(String table, JCaller onDelete);
}
